<?php

exit( 'Migrator should only be run by a developer.' );

ini_set( "display_errors", true );
ini_set('error_reporting', E_ALL);

$source_db = 'jhbqjogx_wp615';
$destination_db = 'jhbqjogx_wp001';
$migrator = mysqli_connect( 'localhost', 'jhbqjogx_migrator', 'C!txue6wx84D', $source_db );

$source_table_prefix = "9j4_";
$destination_table_prefix =  "9j4_";
$bookly_prefix = "ab_";

$tables_to_migrate = array(
	'staff' =>	array(),
	'staff_schedule_items' => array(), // depends on staff
	'schedule_item_breaks' => array(), // depends on staff and staff_schedule_items
	'holidays' => array(), // depends on staff
	'customers' => array(
			array( 'name', 'full_name' )
	),
	'categories' => array(),
	'services' => array( // depends on categories
		array( 'capacity', 'capacity_min' ),
		array( 'capacity', 'capacity_max' )
	),
	'staff_services' => array( // depends on staff and services
		array( 'capacity', 'capacity_min' ),
		array( 'capacity', 'capacity_max' )
	),
	'appointments' => array(), // depends on staff and services*** UID needs some extra processing!
 	'coupons' => array(),
	'customer_appointments' => array(), // depends on customers, appointments
	// // 'notifications' => array(), // don't migrate, syntax changed a lot
	// // 'sent_notifications' => array(), // don't migrate, table empty and ref_id is unclear
);

echo "<table><tbody><tr><td>";
$source_tables = getTables( $source_db, $source_table_prefix.$bookly_prefix );
echo "<h4>Source tables</h4><pre>";print_r( $source_tables );echo "</pre>";
echo "</td><td>";
$destination_tables = getTables( $destination_db, $destination_table_prefix.$bookly_prefix );
echo "<h4>Destination tables</h4><pre>";print_r( $destination_tables );echo "</pre>";
echo "</td><td>";
$tables = array_values( array_intersect( $source_tables, $destination_tables ) );
echo "<h4>Tables present in both</h4><pre>";print_r( $tables );echo "</pre>";
echo "</td></tr></tbody></table>";

foreach( array_reverse( $tables_to_migrate ) as $name => $devnull ) { // truncate in backwards order
	// so foreign key constraints are not violated
	echo "<h5>Truncating <i>$name</i></h5>";
	truncate( $name );
}

foreach( $tables_to_migrate as $name => $crossref ) {
	echo "<h3>Processing table <i>$name</i></h4>";
	echo "<table><tbody><tr><td>";
	$source_columns = getColumns( $source_db, $source_table_prefix.$bookly_prefix.$name );
	echo "<h4>Source columns</h4><pre>";print_r( $source_columns );echo "</pre>";
	echo "</td><td>";
	$destination_columns = getColumns( $destination_db, $destination_table_prefix.$bookly_prefix.$name );
	echo "<h4>Destination columns</h4><pre>";print_r( $destination_columns );echo "</pre>";
	echo "</td><td>";
	$columns = array_values( array_intersect( $source_columns, $destination_columns ) );
	echo "<h4>Columns present in both</h4><pre>";print_r( $columns );echo "</pre>";
	echo "</td></tr></tbody></table>";
	$dcolumns = $scolumns = $columns;
	if ( $crossref ) {
		foreach ( $crossref as $cr ) {
			$scolumns[] = $cr[ 0 ];
			$dcolumns[] = $cr[ 1 ];
		}
	}
	migrate( $name, $scolumns, $dcolumns );
}

// populate coupon_services - every old coupon will be valid for every service
truncate( 'coupon_services' );
$cs = "`$destination_db`.`{$destination_table_prefix}{$bookly_prefix}coupon_services`";
$c = "`$destination_db`.`{$destination_table_prefix}{$bookly_prefix}coupons`";
$s = "`$destination_db`.`{$destination_table_prefix}{$bookly_prefix}services`";
$sql = "INSERT INTO $cs ( coupon_id, service_id ) "
		."SELECT `t1`.`id`, `t2`.`id` FROM $c as t1 "
		."CROSS JOIN $s as t2";
runQuery( $sql );

// truncate oppcs helpers extra table for UID, appointment status, etc.
truncate( 'oppcs_ab_appointments_addons', '' );
$d = "`$destination_db`.`{$destination_table_prefix}oppcs_ab_appointments_addons`";
$s = "`$source_db`.`" . $source_table_prefix . $bookly_prefix . "appointments`";
$sql = "INSERT INTO $d SELECT `id`, `status`, `UID` FROM $s";
runQuery( $sql );
		
function truncate( $table, $prefix_override = false){
	global $destination_db,$destination_table_prefix,$bookly_prefix;
	$prefix = false === $prefix_override ? $bookly_prefix : $prefix_override;
	$dtable = $destination_table_prefix.$prefix.$table;
	// truncate destination - can't use truncate because of foreign key checks and don't have SUPER
	// privileges to disable foreign key checks temporarily...
	runQuery( "DELETE FROM `$destination_db`.`$dtable`" );
	runQuery( "ALTER TABLE `$destination_db`.`$dtable` AUTO_INCREMENT = 1" );
}
function migrate( $table, $scolumns, $dcolumns ){ // assumes the table is pre-truncated
	global $migrator,$source_db,$source_table_prefix,$destination_db,$destination_table_prefix,$bookly_prefix;
	$dtable = $destination_table_prefix.$bookly_prefix.$table;
	$stable = $source_table_prefix.$bookly_prefix.$table;
	
	$sql = "INSERT INTO `$destination_db`.`$dtable` ( ".implode( ",", $dcolumns )." ) "
			."SELECT ".implode( ",", $scolumns )." FROM `$source_db`.`$stable`";
	runQuery( $sql );
}
function getTables( $db, $prefix ) {
	global $migrator;
	$sql = "SHOW TABLES IN `$db` LIKE \"$prefix%\"";
	return getFirstColumn( runQuery( $sql ) );
}
function getColumns( $db, $table ) {
	global $migrator;
	$sql = "SHOW COLUMNS FROM `$db`.`$table`";
	return getFirstColumn( runQuery( $sql ) );	
}
function getFirstColumn( $result ) {
	$res = array();
	while ( $row = $result->fetch_array() ) {
		$res[] = $row[ 0 ];
	}
	return $res;
}
function maybeShowMysqliError( $sql ) {
	global $migrator;
	if ( $migrator->error ) {
		echo "<p style=\"border: 1px solid red;\">".$migrator->error." on query:<br />$sql</p>";
	}
}
function runQuery( $sql ){
	global $migrator;
	$result = $migrator->query( $sql );
	maybeShowMysqliError( $sql );
	return $result;
}