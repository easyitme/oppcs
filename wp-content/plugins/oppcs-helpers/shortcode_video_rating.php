<?php
use Bookly\Lib;

add_shortcode( 'oppcs_video_rating', 'oppcs_video_rating' );
function oppcs_video_rating( $atts, $content = '' ){
	$output = false;
	if ( array_key_exists( 'token', $_REQUEST) && $_REQUEST[ 'token' ]
			&& strlen( $_REQUEST[ 'token' ] ) >= 32 ) {
		$token = substr( $_REQUEST[ 'token' ], -32 );
		$participants = get_participants_for_appointment( $token );
		if ( $participants ) {
			if ( in_array( get_current_user_id(), $participants ) ) {
				$output = do_shortcode( $content );
			}
		}
	}
	if ( false === $output ) {
		$output = oppcs_get_translated_error_paragraph( 'TNF' );
	}
	return $output;
}
// participant_please_rate_video
add_action( 'plugins_loaded', 'send_video_rating_notification' );
function send_video_rating_notification(){
	global $wpdb, $oppcs_customer_appointments_addons_table;
	$cas = Bookly\Lib\Entities\CustomerAppointment::query( 'ca' )
		->select( 'ca.*' )
		->leftJoin( 'Appointment', 'a', 'a.id = ca.appointment_id' )
		->tableJoin( $oppcs_customer_appointments_addons_table, 'addon', 'addon.id=ca.id' )
		->where( 'status', Bookly\Lib\Entities\CustomerAppointment::STATUS_APPROVED )
		->whereNot( 'payment_id', null )
		->whereRaw( 'addon.rate_notification_sent IS NULL', array( ) )
		->whereLt( 'a.end_date', date( 'Y-m-d H:i:s' ) )
		->find();
	foreach( $cas as $ca ) {
		bookly_oppcs_notifier::prepare_data( $ca );
		$result = bookly_oppcs_notifier::send_email_with_merge( 'participant_please_rate_video', 'participants' );
		$wpdb->update( $oppcs_customer_appointments_addons_table,
			array( 'rate_notification_sent' => 1 ), array( 'id' => $ca->get( 'id' ) ) );
		break;
	}
//  	echo"<p style=\"border: 1px solid blue; padding: 10px; margin: 10px;\">" . count( $cas ) . "</p>";die();
}

