<?php
add_shortcode( 'my_video_feed', 'my_video_feed' );
function check_chrome_version($version)
{
	if ( 0 == $version ) {
		return sprintf(__(
"We have detected that you are using %s but couldn't determine the version.
If you have problems with the video chat, please make sure to upgrade your %s to
the latest version.",
	"OPPCS" ), "Google Chrome", "Chrome");
	}

	if ( 36 == $version ) {
		return __(
"We have detected that you are using Google Chrome version 36.
This particular version is known to have a bug which causes it to show a black screen
instead of your chat partner's video feed, even though video chat has been supported
since version 32. Please upgrade to the latest version of Chrome, to at least version 45."
					, "OPPCS" );
	}

	if ( 32 > $version ) {
		return sprintf(
				__(			
"We have detected that you are using Google Chrome version %d.
This version is too old and does not support this video chat technology.
Please upgrade to the latest version of Chrome, to at least version 45."
				, "OPPCS")
				, $version);
	}

	if ( $version < 45 ) {
		return sprintf(
				__(
"We have detected that you are using Google Chrome version %d.
This version is known to have a bug which causes it to show a black screen
instead of your chat partner's video feed. Please upgrade to the latest
version of Chrome, to at least version 45."
				, "OPPCS")
				, $version );
	}
	return null;
}
function check_firefox_version($version)
{
	if ( 0 == $version ) {
		return sprintf(__(
"We have detected that you are using %s but couldn't determine the version.
If you have problems with the video chat, please make sure to upgrade your %s to
the latest version.",
	"OPPCS" ), "Mozilla Firefox", "Firefox");
	}

	if ( 22 > $version) {
		return sprintf(__(
"We have detected that you are using Mozilla Firefox version %d.
This version is too old and does not support video chat.
Please upgrade to the lates version of Firefox.",
			"OPPCS" ),
			$version);
	}
	return null;
}
function detect_browser($force_user_agent_parsing = false)
{
	$browser_cap = get_browser(null, true);
	if(!$browser_cap || $force_user_agent_parsing)
	{
		$ua = $_SERVER['HTTP_USER_AGENT'];
		$matches = array();
		if(!preg_match("/Edge\/12/", $ua) && // Edge thinks it's Chrome 42
				preg_match("/Chrome\/(\d+)\./", $ua, $matches)) {
			$browser_cap['browser'] = "Chrome";
			$browser_cap['version'] = $matches[1];
		}
		if(preg_match("/Firefox\/(\d+)\./", $ua, $matches)) {
			$browser_cap['browser'] = "Firefox";
			$browser_cap['version'] = $matches[1];
		}
	}
	return $browser_cap;
}
function get_browser_X(){
	$browser_cap = detect_browser();
	$version = intval($browser_cap['version']);
	$warning_message = null;
	switch($browser_cap['browser'])
	{
		case 'Chrome':
			$warning_message = check_chrome_version($version);
			break;
		case 'Firefox':
			$warning_message = check_firefox_version($version);
			break;

		default:
			$warning_message = __(
"We have detected that you are using a browser other than Google Chrome or Firefox.
Unfortunately, as of late 2016, the only Chrome (version at least 45) and Firefox
(version at least 49) are implementing this video chat technology reliably.
We expect that in a few months all other major browsers will follow along,
but right now our recommendation is to switch to Chrome 45.0 or Firefox 49.0 or newer.",
				"OPPCS"
			);
	}

	if ($warning_message !== null)
		return array(
			"warning" => "<div class=\"browserwarning\">" . $warning_message . "</div>"
		);

	return null;
}
function my_video_feed( $atts ){
	wp_enqueue_style("oppcs-helpers");
	$browser_warning = get_browser_X();

	$output = "<div class=\"browserwarning\">" . __(
"Ha bármiféle problémát tapasztal a videóval vagy a hanggal, akkor próbálja meg frissíteni
az oldalt a böngészőben a CTRL+SHIFT+R vagy CTRL+F5 gombok lenyomásával, vagy esetleg
indítsa újra a böngészőjét.", "OPPCS") . "</div>";

	if($browser_warning) {
		if ( array_key_exists("critical", $browser_warning)){
			return $browser_warning['critical'];
		}
		$output = $browser_warning['warning'];
	}

	$a = shortcode_atts(array('provider' => 0), $atts);
	$SOON_MINUTES = get_option('video_available_before_appointment_start_minutes'); // this is how many minutes before the start of an appointment can participants log in
	$AFTER_MINUTES = get_option('video_available_after_appointment_end_minutes'); // this is how many minutes before the start of an appointment can participants log in
	
	global $wpdb, $user_ID,
		$oppcs_provider_client_link_table_name, $oppcs_appointments_table,
		$oppcs_appointments_addons_table, $oppcs_appointments_guests_table,
		$oppcs_customer_appointments_table, $oppcs_customers_table,
		$oppcs_staff_table; // Peter Test Doctor (15) 55 // Peter Test Client (8) 56 // Simon Viktoria (6) 41 // Paul Vitti (1) 6
	$match_table_alias		= $a["provider"] ? "staff" 		: "customer";
	$partner_table_alias	= $a["provider"] ? "customer" : "staff";
	$wp_time_offset = 60 * get_option('gmt_offset'); // minutes
	$sql = "
		SELECT DISTINCT 
				MD5( CONCAT( 'salt9465732155 ',`staff`.`wp_user_id`, '-',`customer`.`wp_user_id`)) AS `name`,
				MD5( CONCAT( 'salt4827773664 ', `customer`.`wp_user_id`, `start_date` ) ) AS `video_room_id`,
				`start_date` AS `start`,
				`$partner_table_alias`.`wp_user_id` as `partner_id`, `$partner_table_alias`.`id` as `partner_bookly_id`,
				`$partner_table_alias`.`full_name` as `partner_bookly_name`,
				 `addon`.`UID`,
				IF (
					DATE_ADD( NOW(), INTERVAL $wp_time_offset MINUTE) BETWEEN `start_date` AND
					DATE_ADD(`end_date`, INTERVAL $AFTER_MINUTES MINUTE), 1, 0) AS `now`,
				IF (
					DATE_ADD( NOW(), INTERVAL $wp_time_offset MINUTE) BETWEEN
					DATE_SUB(`start_date`, INTERVAL $SOON_MINUTES MINUTE)
					AND `start_date`, 1, 0) AS `soon`
			FROM `$oppcs_appointments_table` AS `appointment`
				JOIN $oppcs_staff_table AS `staff` ON `staff`.`id` = `appointment`.`staff_id`
				JOIN $oppcs_customer_appointments_table AS `customer_appointment`
					ON `customer_appointment`.`appointment_id` = `appointment`.`id`
				JOIN $oppcs_customers_table AS `customer` ON `customer`.`id` = `customer_appointment`.`customer_id`
				JOIN $oppcs_appointments_addons_table AS `addon` ON `appointment`.`id` = `addon`.`id`
				LEFT JOIN $oppcs_appointments_guests_table AS `guests` ON `guests`.`appointment_id` = `appointment`.`id`
				WHERE ( `$match_table_alias`.`wp_user_id` = $user_ID OR
						`guests`.`user_id` = $user_ID )
					AND DATE_ADD( `appointment`.`end_date`, INTERVAL $AFTER_MINUTES MINUTE)
								> DATE_ADD( NOW(), INTERVAL $wp_time_offset MINUTE)
					AND `customer_appointment`.`status` = 'approved'
					AND `customer_appointment`.`payment_id` IS NOT NULL
		ORDER BY `now` DESC, `soon` DESC, `start_date` ASC
	";
// $output .= '<p style="border: 1px solid blue;">'.$sql.'</p>';
	$res = $wpdb->get_results($sql);
// $output .= "<pre>".print_r( $res, true ). "</pre>";
	if (!is_array($res) || !count($res)){
		$output .= "<div class=\"oppcs_no_upcoming_appointments oppcs_no_video_msg\">"
			."<p>".__("You don't have any upcoming appointments.", "OPPCS")."</p></div>";
	}
	elseif ($res[0]->now || $res[0]->soon) {
		$partner_name = $res[0]->partner_bookly_name;
		$partner_translated_name = $a['provider'] ? $partner_name
			: _o($partner_name, "bookly", "staff_".$res[0]->partner_bookly_id);
		// group together if multiple start at the same time for a client
		if( !$a[ 'provider' ] ) {
			if ( count( $res ) > 1 ) { // more than 1
				for( $i = 1; $i < count( $res ) && $res[ $i ]->start == $res[ 0 ]->start; $i++ ) {
					$ptn = _o( $res[ $i ]->partner_bookly_name, "bookly", "staff_".$res[ $i ]->partner_bookly_id );
					$partner_translated_name .= ", " . $ptn;
				}
			}
		}
		$uid = trim(chunk_split($res[0]->UID, 4, "-"),"-");
		$room_title = sprintf(__('Video chat room with %s, id: %s', "OPPCS"), $partner_translated_name, $uid);

		$shortcode_pattern = get_option('video_shortcode_pattern');
		$shortcode = str_replace(
			array("%ID%", "%TITLE%", "%UID%"),
			array( $res[0]->video_room_id, $room_title, $res[0]->video_room_id ),
			$shortcode_pattern);

		$output .= do_shortcode($shortcode);
		//$output .= "<div>".__("If you experience problems with the video or audio, please refresh this page in your browser, with Ctrl+R or Ctrl+F5.","OPPCS")."</div>";
	}
	else {
		$starttime = new DateTime( $res[ 0 ]->start );
		$to = ( array_key_exists( 'timeoffset', $_SESSION ) ? -$_SESSION[ 'timeoffset' ]
				: ( -get_user_meta( get_current_user_id(), 'timeoffset' ) || 0 ) )
				- $wp_time_offset;
		$tos = ( $to >= 0 ? '+' : '' ). $to;
		$starttime->modify( "$tos minutes" );
		$start_time_to_print = $starttime->format( 'Y-m-d H:i' );
		$output .= "<div class=\"oppcs_video_your_next_appointment oppcs_no_video_msg\"><p>"
			.sprintf(__(
"Your next appointment is at %s. You will be able to log in to the video 
channel %d minutes before that. Please refresh or revisit this page at that time."
					, "OPPCS"),
		$start_time_to_print, $SOON_MINUTES)."<p><div>";
	}
	//$output .= "<pre>".print_r($res, true)."</pre>";
	//$output .= print_r($a, true);
	return $output;
}
