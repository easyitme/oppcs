<?php

DEFINE("oppcs_smart_hungarian_grammar_vowels", "aáeéiíoóöőuúüűy");//AÁEÉIÍOÓÖŐÜŰY");
DEFINE("oppcs_smart_hungarian_grammar_consonants", "bcdfghjklmnpqrstvwx"); //BCDFGHJKLMNPQRSTVWX");
DEFINE("oppcs_smart_hungarian_grammar_consonant_standalone_pronunciation_starts_with_vowel", "FLMNRS");

add_filter( 'bookly_render', 'oppcs_smart_hungarian_grammar', 90, 1 );

/*
 * Registers string in WPML, marking that it needs translation, if:
 * the string in the context is not yet in the POT (regardless of having been translated yet)
 * the string in the context is not registered yet in WPML
 */
function oppcs_register_string_in_wpml($string, $domain, $context){
	$string = oppcs_poeditify_string($string);
	if (oppcs_is_string_in_POT($string, $domain, $context)){ return true; } 	// already in POT
	global $wpdb;
	$table = $wpdb->prefix . "icl_strings";
	$sql = $wpdb->prepare("SELECT COUNT(*) FROM $table WHERE "
			."`context`=%s AND `value`=%s AND `gettext_context`=%s", $domain, $string, $context);
	if(!$wpdb->get_var($sql)){ // not registered yet in WPML
		$record = array(
			"context"	=>	$domain,
			"value"		=>	$string,
			"gettext_context"	=> $context,
			"language"	=>	"en",
			"name"		=>	"",
			"type"			=>	"LINE",
			"status"		=>	0,
			"domain_name_context_md5"	=> md5($domain . $context . $string)
		);
		$sql = $wpdb->prepare("INSERT INTO $table (".join(",", array_keys($record)).")".
				"VALUES( %s, %s, %s, %s, %s, %s, %d, %s )", array_values($record));
		return $wpdb->query($sql);
	}
	return true;
}
function oppcs_is_string_in_POT($string, $domain, $context = null){
	$string = oppcs_poeditify_string($string);
	require_once( ABSPATH . WPINC . '/pomo/po.php' );
	static $translations = false;
	if (false === $translations){
		$translations = new  PO();
		$translations->import_from_file( dirname( dirname( __FILE__ ) )
				. '/appointment-booking/languages/bookly-hu_HU.po' );
	}

	$entry = new Translation_Entry(array('singular' => $string, 'context' => $context));
	$key = $entry->key();
	return array_key_exists($key, $translations->entries);
}
function _o($string, $domain, $context){
// 	if( "category_" == $context ){debug_print_backtrace();exit();}	
	$string = oppcs_poeditify_string($string);
	oppcs_register_string_in_wpml($string, $domain, $context);
	return _x($string, $context, $domain);
}
function _bo($option_name){
	return _o( get_option($option_name), "bookly", $option_name);
}
function oppcs_ajax_error($msg){ // ajax error
	$error_msg = _o( $msg, 'bookly', 'error' );
	return array(
		"success" => false,
		"error"	=> $error_msg,
		"html" => "<div class=\"ajax_error_message\">$error_msg</div>"
	);
}
function oppcs_poeditify_string($string){
	if(!preg_match("/(\r|\n)/", $string)){ return $string; } // single line, nothing to do
	// multiline - convert line endings to unix
	$msg_unix_line_endings = preg_replace("/\r\n?/", "\n", $string);
	// make sure string finishes with a \n
	if("\n" !== substr($msg_unix_line_endings,strlen($msg_unix_line_endings) - 1, 1)){
		$add = "\n";		
	}else{
		$add = "";
	}
	$msg_unix_line_ending_extra_nl_at_end = $msg_unix_line_endings . $add;
	return $msg_unix_line_ending_extra_nl_at_end;
}
function oppcs_smart_hungarian_definite_article($html, $text){ // {{a}} -> "a" or "az"
	$w = oppcs_smart_hungarian_grammar_vowels . oppcs_smart_hungarian_grammar_consonants;
	$matches = array();
	$num_a = preg_match_all( "/\{\{(a|az)\}\}\s+([$w]+)/i" , $text, $matches );
// 	echo "<pre>num_a=$num_a".print_r($matches, true)."</pre>";
	if ( 0 === $num_a || false === $num_a){ return $html; } // nothing to replace
	$pattern = array();
	$replace = array();
	for ($i = 0; $i < $num_a; $i++ ){
		$repl = substr( $matches[1][$i], 0, 1 ); // this way we keep capitalization
// 		echo "<pre>repl=$repl</pre>";
		if ( preg_match( "/^[". oppcs_smart_hungarian_grammar_vowels ."].*/i", $matches[2][$i]) // if next word starts with vowel
		 || preg_match ("/^[" . oppcs_smart_hungarian_grammar_consonant_standalone_pronunciation_starts_with_vowel . "]($|[\p{Lu}])/"
		 				, $matches[2][$i]) // or it's all caps, so maybe 'MTA'
			){
			$repl .= "Z" === substr( $matches[1][$i], 1, 1) ? "Z" : "z"; // ; if it was noted as {{AZ}} we keep it all caps
		}
		$replace[] = $repl;
		
		$pattern[] = "/\{\{". $matches[1][$i] ."\}\}/";
	}
	$html = preg_replace( $pattern, $replace, $html, 1 );
// 	echo "<pre>pattern=".print_r( $pattern, true)."\nreplace=".print_r($replace, true)."</pre>";
	return $html;
}
function oppcs_smart_hungarian_grammar($html){
	if ( "hu_HU" === get_locale() ){
		$text = wp_strip_all_tags($html);
		$html = oppcs_smart_hungarian_definite_article($html, $text);
		//... any more functions here
	}
	return $html;
}
// if ("hu_HU" === get_locale()){
// 	add_filter("the_content", 'oppcs_smart_hungarian_grammar');
// 	add_filter("widget_text", 'oppcs_smart_hungarian_grammar');
// }
// echo get_locale();
// echo oppcs_smart_hungarian_grammar("<p><b>{{A}} Yvette felment {{a}} MTA-ban {{a}} első emeletre!!! {{AZ}} TERINGETTÉT!</b></p>");exit();


