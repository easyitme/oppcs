<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
    echo $progress_tracker;
?>

<div class="bookly-box">
	<p><?php
	global $oppcs_wpml_package;
	$txt = 	"We have received your appointment request and have notified the provider. "
				."We have also sent you a confirmation email. As soon as the provider accepts "
				."or rejects the request we will notify you via email.";
	echo apply_filters( 'wpml_translate_string', $txt, 'notification-request-received', $oppcs_wpml_package );
	?></p>
</div>

<div class="bookly-gateway-buttons pay-coupon bookly-box bookly-nav-steps">
    <button class="bookly-back-step bookly-js-back-step bookly-btn ladda-button" data-style="zoom-in" data-spinner-size="40">
        <span class="ladda-label"><?php echo \Bookly\Lib\Utils\Common::getTranslatedOption( 'bookly_l10n_button_back' ) ?></span>
    </button>
<?php if ( 'approved' == bookly_oppcs_appointment_data::$ca_status ): ?>
    <button class="bookly-next-step bookly-js-next-step bookly-js-coupon-payment bookly-btn ladda-button" data-style="zoom-in" data-spinner-size="40">
        <span class="ladda-label"><?php echo \Bookly\Lib\Utils\Common::getTranslatedOption( 'bookly_l10n_step_payment_button_next' ) ?></span>
    </button>
<?php endif; ?>
</div>
