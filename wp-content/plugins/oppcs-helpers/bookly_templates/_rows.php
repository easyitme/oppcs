<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
use Bookly\Lib\Utils\DateTime;
use Bookly\Lib\Utils\Price;
use Bookly\Lib\Entities;
?>
<?php foreach ( $appointments as $app ) : ?>
    <?php if ( ! isset( $compound_token[ $app['compound_token'] ] ) ) :
        if ( $app['compound_token'] !== null ) {
            $compound_token[ $app['compound_token'] ] = true;
        }
        $extras_total_price = 0;
        foreach ( $app['extras'] as $extra ) {
            $extras_total_price += $extra['price'];
        }
    ?>
    <tr class="bookly-appointment-status-<?php
    if ( 'booked' == $app[ 'appointment_status' ] ) {
    	echo strtolower( __( 'Booked', 'bookly' ) );
    } else {
    	echo strtolower( Entities\CustomerAppointment::statusToString( $app['appointment_status'] ) );
    }
    if ( strtotime( $app[ 'start_date' ] ) < strtotime( 'now' ) ) {
    	echo ' bookly-appointment-expired';
    }
    ?>">
        <?php foreach ( $columns as $column ) :
            switch ( $column ) :
                case 'service' : ?>
                    <td>
                        <?php echo $app['service'] ?>
                        <?php if ( ! empty ( $app['extras'] ) ): ?>
                            <ul class="bookly-extras">
                                <?php foreach ( $app['extras'] as $extra ) : ?>
                                    <li><?php echo $extra['title'] ?></li>
                                <?php endforeach ?>
                            </ul>
                        <?php endif ?>
                    </td><?php
                    break;
                case 'date' : ?>
                    <td><?php echo DateTime::formatDate( $app['start_date'] ) ?></td><?php
                    break;
                case 'time' : ?>
                    <td><?php echo DateTime::formatTime( $app['start_date'] ) ?></td><?php
                    break;
                case 'price' : ?>
                    <td style="text-align:right!important;"><?php echo Price::format( ( $app['price'] + $extras_total_price ) * $app['number_of_persons'] ) ?></td><?php
                    break;
                case 'status' : ?>
                    <td class="bookly-column-status"><i class="fa fa-2x" title="<?php
                    	if ( 'booked' == $app[ 'appointment_status' ] ) {
    						echo __( 'Booked', 'bookly' );
    					} else {
    						echo Entities\CustomerAppointment::statusToString( $app['appointment_status'] );
    					}
    				?>"></i></td><?php
                    break;
                case 'cancel' :
                    $this->render( '_custom_fields', compact( 'custom_fields', 'app' ) ); ?>
                    <td class="has-button">
                    <?php if ( $app['start_date'] > current_time( 'mysql' ) ) : ?>
                        <?php if ( $allow_cancel < strtotime( $app['start_date'] ) ) : ?>
                            <?php if (
                                ( $app['appointment_status'] != Entities\CustomerAppointment::STATUS_CANCELLED )
                                && ( $app['appointment_status'] != Entities\CustomerAppointment::STATUS_REJECTED )
                            ) : ?>
                                <a class="bookly-btn" style="background-color: <?php echo get_option('oppcs_destructive_button_color'); ?>" href="<?php echo esc_attr( $url_cancel . '&token=' . $app['token'] ) ?>">
                                    <span><?php _e( 'Cancel', 'bookly' ) ?></span>
                                </a>
                            <?php endif ?>
                        <?php else : ?>
                            <?php _e( 'Not allowed', 'bookly' ) ?>
                        <?php endif ?>
                    <?php else : ?>
                        <?php _e( 'Expired', 'bookly' ) ?>
                    <?php endif ?>
                    </td><?php
                    break;
				case 'reject_or_accept': ?>
					<td class="bookly-column-reject-or-accept has-button">
						<?php if ( 'staff' == $role
								&& $app['appointment_status'] == Entities\CustomerAppointment::STATUS_PENDING ): 
								global $oppcs_wpml_package; ?>
                                <a class="bookly-btn" style="background-color: <?php echo get_option('oppcs_destructive_button_color'); ?>" href="<?php
                                	echo bookly_oppcs_notifier::get_status_change_url( $app['token'], 'reject' ); ?>">
                                    <span><?php echo apply_filters( 'wpml_translate_string', 'Reject', 'button-reject', $oppcs_wpml_package ); ?></span>
                                </a>
                                <a class="bookly-btn" style="background-color: <?php echo $color ?>" href="<?php
                                	echo bookly_oppcs_notifier::get_status_change_url( $app['token'], 'approve' );?>">
                                    <span><?php echo apply_filters( 'wpml_translate_string', 'Accept', 'button-accept', $oppcs_wpml_package ) ?></span>
                                </a>
						<?php endif;?>
					</td><?php 
					break;
				case 'finalize': ?>
					<td class="bookly-column-finalize has-button">
						<?php if ( strtotime( $app['start_date'] ) > strtotime( 'now' )
								&& $app['appointment_status'] == Entities\CustomerAppointment::STATUS_APPROVED ): 
								global $oppcs_wpml_package;?>
                               <a class="bookly-btn" style="background-color: <?php echo $color; ?>" href="<?php
                                	echo get_home_url( null, get_option( 'oppcs_new_booking_page' ) ) . "?token=" . $app[ 'token' ]; ?>">
                                    <span><?php echo apply_filters( 'wpml_translate_string', 'Finalize', 'button-finalize', $oppcs_wpml_package ); ?></span>
                                </a>
						<?php endif; ?>
					</td><?php 
					break;
                default : ?>
                    <td><?php echo $app[ $column ] ?></td>
            <?php endswitch ?>
        <?php endforeach ?>
    <?php endif ?>
    <?php if ( $with_cancel == false ) :
        $this->render( '_custom_fields', compact( 'custom_fields', 'app' ) );
    endif ?>
    </tr>
<?php endforeach ?>