<?php
use Bookly\Backend\Modules\Staff;
add_shortcode('oppcs_bookly_profile', 'oppcs_bookly_profile');


function oppcs_bookly_profile($atts)
{
    $controller = Bookly\Backend\Modules\Staff\Controller::getInstance();

    $ajax_url = admin_url('admin-ajax.php');

    if ( ! session_id() ) {
        @session_start();
    }

    if ( isset( $_POST['action']) && $_POST['action'] == 'ab_update_staff')
        $controller->updateStaff();

    // From WordPress/wp-include/script-loader.php
    wp_enqueue_script( 'common', "/wp-admin/js/common.js", array('jquery', 'hoverIntent', 'utils'), false, 1 );
    wp_enqueue_style( 'common', "/wp-admin/css/common.css" );
    wp_enqueue_style( 'dashicons', "/wp-includes/css/dashicons.css" );
    wp_localize_script( 'common', 'commonL10n', array(
        'warnDelete'   => __( "You are about to permanently delete these items.\n  'Cancel' to stop, 'OK' to delete." ),
        'dismiss'      => __( 'Dismiss this notice.' ),
        'collapseMenu' => __( 'Collapse Main menu' ),
        'expandMenu'   => __( 'Expand Main menu' ),
    ));

    print <<<EOF
    <script>
        window.ajaxurl = "$ajax_url";
    </script>
EOF;
    $controller->index();
}
