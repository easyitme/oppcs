<?php
/*
 * Ultimate membership esc_url-s redirect to links
 * but forgets to unescape them before processing
 * This is now done from here 
 * */
DEFINE("NEWSEPARATOR", "------------");
function fix_ultimate_membership_redirect_first_step( $redirect_to ){
	$new_redirect = preg_replace( "/(&#038;)/",NEWSEPARATOR, $redirect_to);
	return $new_redirect;	
}
add_filter( 'wp_redirect', 'fix_ultimate_membership_redirect_first_step', 10 );

function fix_ultimate_membership_redirect_second_step(){ // rewriting GET params...
	if(array_key_exists('redirect_to', $_GET)){
		$_REQUEST['redirect_to'] = preg_replace( "/".NEWSEPARATOR."/","&",
			$_REQUEST['redirect_to']);
	}
}
add_action("init", 'fix_ultimate_membership_redirect_second_step', 1 );