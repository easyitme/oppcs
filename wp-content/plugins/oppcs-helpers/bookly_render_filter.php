<?php

use Bookly\Lib;

@session_start();

error_reporting(E_ALL);
ini_set('display_errors', 'on');
add_action( 'plugins_loaded', 'purge_old_approved_unpaid_appointments' );

class bookly_oppcs_appointment_data {
	static $form_id = false;
	static $token = false;
	static $requested_by_token = false;
	static $ca = null; // bookly customer appointment object
	static $ca_data = false;
	static $appointment_data = false;
	static $ca_status;
	static $payment_id;
	static $read = false;
	static $status = 'new';
	static $participants = array(); // wp userid of current meeting participants
	static $current_role = ''; // role of current user: provider/client/guest/none
	
	static function init( $maybe_form_id = false ) {
		self::$form_id = $maybe_form_id ? : self::read_form_id_from_request();
		self::read_token_from_request() ? : self::read_token_from_session();
	}
	static function read_form_id_from_request() {
		if ( !self::$form_id ) {
			if ( array_key_exists( 'form_id', $_REQUEST ) ) {
				self::$form_id = $_REQUEST[ 'form_id' ];
	//  			echo "<pre>form_id:".self::$form_id."</pre>";
			} else { self::$form_id = false; }
		}
		return self::$form_id;
	}
	static function read_token_from_request() {
		if ( ! self::$token ) {
			if ( array_key_exists( 'token', $_REQUEST ) || array_key_exists( 'oppcsdeconflictedtoken', $_REQUEST ) ) {
				if ( array_key_exists( 'oppcsdeconflictedtoken', $_REQUEST ) ) {
					self::$token = $_REQUEST[ 'oppcsdeconflictedtoken' ];
				} else {
					self::$token = $_REQUEST[ 'token' ];
				}
	// 			echo "<pre>token:".self::$token."</pre>";
				self::$requested_by_token = true;
				set_var_in_session( self::$form_id, 'token', self::$token );
			} else { self::$token = false; }
		}
		return self::$token;
	}
	static function read_token_from_session() {
		if ( ! self::$token ) {
			self::$token = get_var_from_session( self::$form_id, 'token' );
		}
		return self::$token;
	}
	static function read_appointment_from_dbase(){
		if ( self::$read || !self::$form_id || !self::$token ) {
			return;
		}
		self::get_appointment_data();
		self::$ca_status = self::$ca_data[ 'status' ];
// 		echo "<pre>appointment_status:";var_dump( self::$ca_status ); echo "</pre>";
		self::$payment_id = self::$ca_data[ 'payment_id' ];
// 		echo "<pre>payment_id:";var_dump( self::$payment_id ); echo "</pre>";
		if ( null != self::$ca_data[ 'payment_id' ] ) { self::$status = 'finished'; }
		else { self::$status = 'cancelled'; } // this triggers the payment step in a weird way
		self::$read = true;
		return self::$status;
	}
	static function get_appointment_data( ) {
		global $wpdb, $oppcs_appointments_table, $oppcs_customer_appointments_table;
		$sql = "	SELECT * FROM $oppcs_customer_appointments_table
						WHERE `token`=\"".self::$token."\"";
		self::$ca_data = $wpdb->get_row( $sql, ARRAY_A );
		if ( self::$ca_data ) {
			$sql = "	SELECT * FROM $oppcs_appointments_table
							WHERE `id` = ".self::$ca_data[ 'appointment_id' ];
			self::$appointment_data = $wpdb->get_row( $sql, ARRAY_A );
			self::get_appointment_participants();
		}
		self::$ca = new Bookly\Lib\Entities\CustomerAppointment();
		self::$ca->loadBy( array( 'id' => self::$ca_data[ 'id' ] ) );
	}
	static function get_appointment_participants() {
		global $wpdb, $oppcs_customers_table, $oppcs_staff_table,
		$oppcs_appointments_table, $oppcs_appointments_addons_table,
		$oppcs_customer_appointments_table, $oppcs_customer_appointments_addons_table;
		self::$participants = array();
		self::$current_role = '';
		$sql = "	SELECT `wp_user_id` FROM  $oppcs_customers_table 
						WHERE `id` = ".self::$ca_data[ 'customer_id' ];
		$customer_id = $wpdb->get_var( $sql );
		if ( $customer_id ) {
			self::$participants[] = $customer_id;
			if ( $customer_id == get_current_user_id() ) {
				self::$current_role = 'client';
			}
		}
		$sql = "	SELECT `wp_user_id` FROM  $oppcs_staff_table
						WHERE `id` = ".self::$appointment_data[ 'staff_id' ];
		$staff_id = $wpdb->get_var( $sql );
		if ( $staff_id ) {
			self::$participants[] = $staff_id;
			if ( $staff_id == get_current_user_id() ) {
				self::$current_role = 'provider';
			}
		}
	}
	static function save_bookly_session_for_ca( ){
		global $wpdb, $oppcs_customer_appointments_addons_table;
// 		echo "<h5>about to save ca appointment session</h5>";
// 		echo "<pre>"; var_dump( $_SESSION ); echo "</pre>";
/*		echo "<pre>"; var_dump( array_key_exists( 'bookly', $_SESSION ),
				array_key_exists( 'forms', $_SESSION[ 'bookly' ] ),
				self::$form_id,
				array_key_exists( self::$form_id, $_SESSION[ 'bookly' ][ 'forms' ] ),
				self::$ca_data, self::$ca_data[ 'id' ] ); echo "</pre>";*/ 
		if ( 		array_key_exists( 'bookly', $_SESSION ) 
				&& array_key_exists( 'forms', $_SESSION[ 'bookly' ] )
				&& self::$form_id
				&&	array_key_exists( self::$form_id, $_SESSION[ 'bookly' ][ 'forms' ] )
				&& self::$ca_data && self::$ca_data[ 'id' ] ) {
			$sess = json_encode( $_SESSION[ 'bookly' ][ 'forms' ][ self::$form_id ],
						JSON_NUMERIC_CHECK & JSON_PRETTY_PRINT );
			$wpdb->show_errors();
			$rows = $wpdb->insert( $oppcs_customer_appointments_addons_table,
					array( 'id' => self::$ca_data[ 'id' ], 'session' => $sess ), array( '%d', '%s' ) );
			if ( false === $rows ) {
				$wpdb->replace( $oppcs_customer_appointments_addons_table,
						array( 'id' => self::$ca_data[ 'id' ], 'session' => $sess ), array( '%d', '%s' ) );
			}
		} else {
			return false;
		}
	}
	static function load_bookly_session_from_ca() {
		global $wpdb, $oppcs_customer_appointments_addons_table;		
		if ( self::$form_id && self::$ca_data && self::$ca_data[ 'id' ] ) {
			$sess = $wpdb->get_var( "SELECT `session` FROM $oppcs_customer_appointments_addons_table
					WHERE `id`=". self::$ca_data[ 'id' ] );
			if ( $sess ) {
				if ( !array_key_exists( 'bookly', $_SESSION ) ) { $_SESSION[ 'bookly' ] = array(); }
				if ( !array_key_exists( 'forms', $_SESSION[ 'bookly' ] ) ) { $_SESSION[ 'bookly' ][ 'forms' ] = array(); }
				$_SESSION[ 'bookly' ][ 'forms' ][ self::$form_id ] = json_decode( $sess, true );
			}
		}
	}
	static function save_session_as_new_appointment(){
		$userData = new Lib\UserBookingData( self::$form_id );
		$userData->load();
		$ca_list = $userData->save( null ); // no payment yet
		$ca_id = array_keys( $ca_list )[ 0 ]; // no cart, just one appointment
		$ca = $ca_list[ $ca_id ];
		self::$token = $ca->get( 'token' );
		set_var_in_session( self::$form_id, 'token', self::$token );
		self::$ca_data = array( 'id' => $ca_id );
		self::save_bookly_session_for_ca();
		self::read_appointment_from_dbase(); // populate data for email sending
	}
	static function add_payment_id_to_customer_appointment_in_db( $payment_id ) {
		// payment must already be saved because of the foreign key constraint
		global $wpdb, $oppcs_customer_appointments_table;
		if ( self::$read ) {
			$updated_rows = $wpdb->update( $oppcs_customer_appointments_table, 
				array( 'payment_id' => $payment_id ), array( 'id' => self::$ca_data[ 'id' ] ),
				array( '%d' ), array( '%d' ) );
			self::$ca->set( 'payment_id', $payment_id );
			self::$ca_data[ 'payment_id' ] = $payment_id;
			return (bool) $updated_rows;
		} else {
			trigger_error( 'Customer appointment must be loaded before update.', E_USER_ERROR );
		}
	}
}
class bookly_oppcs_notifier {
	static $ca;
	static $data;
	static $codes;
	static $appointment;
	static $customer;
	static $staff;
	static $oppcs_codes;

	private static function make_button( $url, $text ){
		global $oppcs_wpml_package;
		$color = $text[ 'destructive' ] ? get_option('oppcs_destructive_button_color') : get_option( 'bookly_app_color');
		if($color){$color = "background-color:$color;";}
		$style = "$color; width:auto; padding: 9px 15px; border-radius: 4px; color:white; "
			."cursor: pointer; height: auto; text-transform: uppercase; font: 18px Arial;"
			."text-decoration: none; margin: 5px; white-space: nowrap;";
		$button_text = apply_filters( 'wpml_translate_string', $text[ 'string' ], $text[ 'name' ], $oppcs_wpml_package );	
		$button = "<a href=\"$url\" style=\"$style\"><b>$button_text</b></a>";
		return $button;
	}
	private static function make_ribbon( $url, $text ){
		$style = "width:100%;border-top:1px solid silver; padding-top:20px;";
		$ribbon = "<table style=\"$style\"><tr>";
		if( is_array( $url ) && count( $url ) > 1 ){
			$ribbon .= "<td>" . self::make_button( $url[0], $text[0] ) . "</td>";
			$url2 = end( $url ); $text2 = end( $text );
		} else {
			$url2 = $url; $text2 = $text;
		}
		$ribbon .= "<td style=\"width:90%;\"></td>";
		$ribbon .= "<td>" . self::make_button( $url2, $text2 ) . "</td>";
		$ribbon .= "</tr></table>";
		return $ribbon;
	}
	private static function add_buttons_and_ribbons() {
		$to = self::$codes->get( 'appointment_token' );
		$query_args_array = array( 'token' => $to );
		$lang_array = array();
		if ( array_key_exists( 'lang', $_GET ) ) { // manual multilingual: WPML home url doesn't work
			$query_args_array[ 'lang' ] = $_GET[ 'lang' ];
			$lang_array[ 'lang' ] = $_GET[ 'lang' ];
		}
		self::$oppcs_codes =  array(
			'{pending_staff_appointments_url}' =>
					add_query_arg( $lang_array,
						get_home_url( null, get_option( 'oppcs_pending_staff_appointments_page' ) ) ),
			'{accept_reject_ribbon}' => self::make_ribbon( 
					array( self::get_status_change_url( $to, 'reject' ), self::get_status_change_url( $to, 'approve' ) ),
					array( array( 'string' => 'Reject', 'name' => 'button-reject', 'destructive' => true ),
							array( 'string' => 'Accept', 'name' => 'button-accept', 'destructive' => false ) ) ),
			'{make_new_appointment_ribbon}' => self::make_ribbon( 
					add_query_arg( $lang_array,
						get_home_url( null, get_option( 'oppcs_new_booking_page' ) ) ),
					array( 'string' => 'Make new appointment', 'name' => 'button-new-booking', 'destructive' => false )),
			'{finalize_appointment_ribbon}' => self::make_ribbon(
					add_query_arg( $query_args_array,
						get_home_url( null, get_option( 'oppcs_new_booking_page' ) ) ),
					array( 'string' => 'Finalize appointment', 'name' => 'button-finalize', 'destructive' => false )	),
			'{rate_video_ribbon}' => self::make_ribbon(
					add_query_arg( $query_args_array,
						get_home_url( null, get_option( 'oppcs_rate_video_page' ) ) ),
					array( 'string' => 'Rate video', 'name' => 'button-rate-video', 'destructive' => false )	),
		);
	}
	private static function apply_client_timezone_to_start_and_end(){
		$time_offset = self::$ca->get( 'time_zone_offset') + (int) get_option( 'gmt_offset' ) * 60;
// 		echo "<p>time_offset:$time_offset</p>";
		if ( $time_offset ) {
			foreach( array( 'appointment_start', 'appointment_end' ) as $key ) {
//  				echo "<p>Old time: " . self::$codes->get( $key ) . "  New time:".date( "Y-m-d H:i:s", strtotime( self::$codes->get( $key ) ) - $time_offset * 60 ) . "</p>";
				self::$codes->set( $key, date( "Y-m-d H:i:s", strtotime( self::$codes->get( $key ) ) - $time_offset * 60 ) );
			}
		}
	}
	private static function apply_codes( $body, $type = 'text' ) {
		$b2 = self::$codes->replace( $body, $type );
		$b3 = strtr( $b2, self::$oppcs_codes );
		return $b3;
	}
	static function get_status_change_url( $to, $action ) {
			$query_args_array = array(
						'action' => 'oppcs_appointment_status',
						'op' => $action,
						'token' => urlencode( Bookly\Lib\Utils\Common::xorEncrypt( $to, $action ) )
					);
			if ( array_key_exists( 'lang', $_GET ) ) { // manual multilingual: WPML home url doesn't work
				$query_args_array[ 'lang' ] = $_GET[ 'lang' ];
			}
			return add_query_arg( $query_args_array, admin_url( 'admin-ajax.php' ) );
	}
	static function prepare_data( $ca, $locale = null ) {
		self::$ca = $ca;
		$m = new ReflectionMethod( 'Bookly\Lib\NotificationSender::_prepareData' );
		$m->setAccessible( true );
		self::$data = $m->invoke( null, $ca, $locale ); // $codes, $appointment, $customer, $staff
		list ( self::$codes, self::$appointment, self::$customer, self::$staff ) = self::$data;
		self::add_buttons_and_ribbons();
	}
	static function send_email_with_merge( $type, $email ) {
		global $wpdb, $oppcs_notifications_table, $oppcs_wpml_notifications_package;
		$sql = "	SELECT * FROM $oppcs_notifications_table
						WHERE `type`=\"$type\"";
		$note = $wpdb->get_row( $sql, ARRAY_A );
		$translated_subject = apply_filters( 'wpml_translate_string',
			$note[ 'subject' ], $type.'_subject', $oppcs_wpml_notifications_package );
		$translated_body = apply_filters( 'wpml_translate_string',
				$note[ 'message' ], $type.'_message', $oppcs_wpml_notifications_package );
		$headers = Bookly\Lib\Utils\Common::getEmailHeaders( array() );
		$email_by_user_id = false;
		switch ( $email ) {
			case 'client' : case 'customer' :
				$email = self::$customer->get( 'full_name' ) . ' <' . self::$customer->get( 'email' ) . '>';
				self::apply_client_timezone_to_start_and_end();
				break;
			case 'provider' : case 'staff' :
				$email = self::$staff->get( 'full_name' ) . ' <' . self::$staff->get( 'email' ) . '>';
				break;
			case 'admin' :
				$email = get_option( 'admin_email' );
				break;
			case 'participants' :
				$email = get_participants_for_appointment( self::$ca->get( 'token' ) );
				$email_by_user_id = true;
				break;
			default:
				break;
		}
		if ( !is_array( $email ) ) {
			$email = array( $email );
		}
		$result = array();
		foreach ( $email as $e ) {
			if ( $email_by_user_id ) {
				$u = get_user_by( 'id', $e );
				$e = $u->display_name . ' <' . $u->user_email . '>';
				self::$oppcs_codes[ '{user_name}' ] = $u->display_name;				
			}
			$subject = self::apply_codes( $translated_subject, 'text' );
			$body = oppcs_smart_hungarian_grammar ( self::apply_codes( $translated_body, 'html' ) );
			$result[] = wp_mail( $e, $subject, $body, $headers );	
		}
		return $result;
	}
}
add_filter( 'bookly_template', 'bookly_template_filter', 10, 4 );
function bookly_template_filter( $template_full_path, $variables, $controller ) {
//  	echo "<pre>".$template_full_path."</pre>";
 	$template_relative = explode( 'appointment-booking/', $template_full_path )[ 1 ];
//  	echo "<pre>".$template_relative."</pre>";
// 	echo "<pre>";var_dump( $controller ); echo "</pre>";
// 	error_reporting(E_ALL);
// 	ini_set('display_errors', 'on');
// 	echo"<pre>session:";var_dump( $_SESSION );echo"</pre>";
	switch ( $template_relative ) {
		case 'frontend/modules/booking/templates/short_code.php' : // main booking form
			bookly_oppcs_appointment_data::init( $variables[ 0 ][ 'form_id' ] );
			if ( bookly_oppcs_appointment_data::$token ) {
				bookly_oppcs_appointment_data::read_appointment_from_dbase();
				bookly_oppcs_appointment_data::load_bookly_session_from_ca();
				$variables[ 0 ][ 'status' ][ 'booking' ] = bookly_oppcs_appointment_data::$status;
				if ( in_array( bookly_oppcs_appointment_data::$ca->get( 'status' ), array( 'cancelled', 'rejected' ) ) ) {
					$url = get_option( 'oppcs_appointment_status_page' );
					$url .= "?token=".bookly_oppcs_appointment_data::$token;
					wp_redirect( $url );
					exit();
				}
// 				echo "<pre>"; var_dump( $_SESSION[ 'bookly' ][ 'forms' ] ); echo "</pre>";
// 				echo "<pre>notifier data:";var_dump( bookly_oppcs_notifier::$data ); echo "</pre>";
			}
			// populate $form_id
// 			echo "<pre>"; var_dump( $variables ); echo "</pre>";
// 			$template_full_path = array( oppcs_path( array( 'bookly_templates', 'get_form_id.php' ) ), $template_full_path );
			break;
		case 'frontend/modules/booking/templates/_progress_tracker.php' :
			if ( 'bookly_render_payment' == $_REQUEST[ 'action' ] ) {
				bookly_oppcs_appointment_data::init();
				bookly_oppcs_appointment_data::read_appointment_from_dbase();
// 				$ref = new ReflectionClass( 'bookly_oppcs_appointment_data' );
// 				echo "<pre>"; var_dump( $ref->getStaticProperties() ); echo "</pre>";

// 				echo "<pre>status should be:";var_dump( bookly_oppcs_appointment_data::$status ); echo "</pre>";
				bookly_oppcs_appointment_data::read_appointment_from_dbase();
				if ( !bookly_oppcs_appointment_data::$token ) {
					bookly_oppcs_appointment_data::save_session_as_new_appointment();
					bookly_oppcs_notifier::prepare_data( bookly_oppcs_appointment_data::$ca );
//  					echo "<pre>notifier codes:";var_dump( bookly_oppcs_notifier::$codes ); echo "</pre>";
					bookly_oppcs_notifier::send_email_with_merge( 'staff_new_pending_appointment', 'provider' );
					bookly_oppcs_notifier::send_email_with_merge( 'client_new_pending_appointment', 'client' );
				}
			}
			break;
		case 'frontend/modules/booking/templates/7_payment.php' :
			bookly_oppcs_appointment_data::init();
			bookly_oppcs_appointment_data::read_appointment_from_dbase();
//  			echo "<pre>appointment data:";var_dump( bookly_oppcs_appointment_data::$ca_data ); echo "</pre>";
			switch ( bookly_oppcs_appointment_data::$ca_status ) {
				case 'pending' : // no approval yet!
					$template_full_path = oppcs_path( array( "bookly_templates", "6.5_accept.php" ) );
					break;
				case 'approved' : // approved - post payment template
				default:	
					break;
			}
			break;
		case 'frontend/modules/customer_profile/templates/short_code.php' : // appointment list
			$template_full_path = oppcs_path( array( "bookly_templates", "short_code.php" ) );
			break;
		case 'frontend/modules/customer_profile/templates/_rows.php':
			$template_full_path = oppcs_path( array( "bookly_templates", "_rows.php" ) );
			break;
	}
	return $template_full_path;
}

add_filter( 'bookly_render', 'bookly_render_filter', 10, 4 );
function bookly_render_filter( $output, $template_full_path, $variables, $controller ) {

	if ( -1 < strpos( $template_full_path, 'appointment-booking/' ) ) {
		$template_relative = explode( 'appointment-booking/', $template_full_path )[ 1 ];
	} elseif ( -1 < strpos( $template_full_path, 'oppcs-helpers/' ) ) {
		$template_relative = explode( 'oppcs-helpers/', $template_full_path )[ 1 ];
	}
	$dom = false;
	switch ( $template_relative ) {
		case 'frontend/modules/booking/templates/short_code.php':
			break;
		case 'frontend/modules/booking/templates/1_service.php' :
			break;
		case 'frontend/modules/booking/templates/_progress_tracker.php' :
			bookly_oppcs_appointment_data::init();
			bookly_oppcs_appointment_data::read_appointment_from_dbase();
			$dom = dom_from_string( $output );
			// delete original step numbers, allow css numbers to take over
			$elems = elements_from_xpath_string( $dom, ".//div[ contains( @class, 'bookly-progress-tracker' ) ]/div" );
			foreach ( $elems as $e ) {
				$e->firstChild->nodeValue = preg_replace( '/^\d+\.\s/', "", trim( $e->firstChild->nodeValue ) );
			}
			// insert approval step in progress tracker
			$payment = $elems->item( 3 );
			$new_step = $payment->cloneNode( true ); // deep clone
			$new_step->firstChild->nodeValue = \Bookly\Lib\Utils\Common::getTranslatedOption( 'bookly_l10n_step_accept' );
			if ( $variables[ 'step' ] > 6 ) {
				$new_step->setAttribute( 'class', 'active' );
			}
			$payment->parentNode->insertBefore( $new_step, $payment );
			if ( 'pending' == bookly_oppcs_appointment_data::$ca_status ) {
				$payment->setAttribute( 'class', '' ); // payment not active yet
			}
			$output = string_from_dom( $dom );
			break;
	}
// 	$output = "<h1>$template_relative</h1>$output"; // DEBUG
	return $output;
}
add_action( 'wp_enqueue_scripts', 'bookly_render_filter_enqueue_scripts' );
function bookly_render_filter_enqueue_scripts() {
	wp_register_style('bookly_render_filter',
			plugins_url( 'bookly_render_addon.css', __FILE__),
			array() );
	wp_enqueue_style("bookly_render_filter");
}

add_action( 'wp_enqueue_scripts', 'bookly_bookly_main_oppcs_overrides' );
function bookly_bookly_main_oppcs_overrides() {
	wp_register_style('bookly-main-oppcs-overrides',
			plugins_url( 'bookly-main-oppcs-overrides.css', __FILE__),
			array() );
	wp_enqueue_style("bookly-main-oppcs-overrides");
}

add_action( 'wp_ajax_oppcs_appointment_status', 'oppcs_appointment_status' );
add_action( 'wp_ajax_nopriv_oppcs_appointment_status', 'oppcs_appointment_status' );
function oppcs_appointment_status() {
	global $oppcs_error_messages;
	function report_error( $err, $op, $json, $o_token ) {
		global $oppcs_error_messages;
		if ( !array_key_exists( $err, $oppcs_error_messages) ) {
			$err = 'UNK';
		}
		$msg = $oppcs_error_messages[ $err ];
		if ( $json ) {
			wp_send_error( new WP_Error( $err, $msg ) );
			exit();
		} else {
			$url = get_option( 'oppcs_appointment_status_page' );
			$url .= "?op=$op&error=$err&token=".$_REQUEST[ 'token' ];
			wp_redirect( $url );
			exit();
		}
	}
	if ( array_key_exists( 'op', $_REQUEST ) && $_REQUEST[ 'op' ] ) {
		$op = $_REQUEST[ 'op' ];
 		$json = array_key_exists( 'json', $_REQUEST ) && $_REQUEST[ 'json' ];
 		$o_token = $_REQUEST[ 'token' ];
		$token = Lib\Utils\Common::xorDecrypt( $o_token, $op );
		$ca = new Lib\Entities\CustomerAppointment();
		if ( $ca->loadBy( array( 'token' => $token ) ) ) {
			$appointment = new Lib\Entities\Appointment();
			if ( $appointment->load( $ca->get( 'appointment_id' ) ) ) {
				// do the actual thing
				switch ( $op ) {
					case 'cancel':
						if ( $ca->get( 'status' ) == Lib\Entities\CustomerAppointment::STATUS_CANCELLED ) {
							report_error( 'AAC', $op, $json, $o_token );
						}
						if ( $ca->get( 'status' ) == Lib\Entities\CustomerAppointment::STATUS_REJECTED ) {
							report_error( 'AAR', $op, $json, $o_token );
						}
						// we have to check if we can still cancel
						$minimum_time_prior_cancel = (int) get_option( 'bookly_gen_min_time_prior_cancel', 0 );
						$allow_cancel_time = strtotime( $appointment->get( 'start_date' ) ) - $minimum_time_prior_cancel * HOUR_IN_SECONDS;
						if ( current_time( 'timestamp' ) > $allow_cancel_time ) {
							report_error( 'TTC', $op, $json, $o_token ); // too close to cancel
						}
						$ca->cancel(); // handles note sending too
						break;
					case 'approve':
						if ( $ca->get( 'status' ) == Lib\Entities\CustomerAppointment::STATUS_APPROVED ) {
							report_error( 'AAA', $op, $json, $o_token );
						}
						if ( $ca->get( 'status' ) == Lib\Entities\CustomerAppointment::STATUS_CANCELLED ) {
							report_error( 'AAC', $op, $json, $o_token );
						}
						if ( $ca->get( 'status' ) == Lib\Entities\CustomerAppointment::STATUS_REJECTED ) {
							report_error( 'AAR', $op, $json, $o_token );
						}
						$ca->set( 'status', Lib\Entities\CustomerAppointment::STATUS_APPROVED )->save();
						$appointment->handleGoogleCalendar();
						//Bookly\Lib\NotificationSender::send( $ca );
						set_approval_time_for_ca( $ca );
						bookly_oppcs_notifier::prepare_data( $ca );
						bookly_oppcs_notifier::send_email_with_merge( 'client_appointment_accepted', 'client' );
						break;
					case 'reject':
						if ( $ca->get( 'status' ) == Lib\Entities\CustomerAppointment::STATUS_APPROVED ) {
							report_error( 'AAA', $op, $json, $o_token );
						}
						if ( $ca->get( 'status' ) == Lib\Entities\CustomerAppointment::STATUS_CANCELLED ) {
							report_error( 'AAC', $op, $json, $o_token );
						}
						if ( $ca->get( 'status' ) == Lib\Entities\CustomerAppointment::STATUS_REJECTED ) {
							report_error( 'AAR', $op, $json, $o_token );
						}
						$ca->set( 'status', Lib\Entities\CustomerAppointment::STATUS_REJECTED )->save();
						$appointment->handleGoogleCalendar();
						// Waiting list.
						Bookly\Lib\Proxy\WaitingList::handleParticipantsChange( $appointment );
						bookly_oppcs_notifier::prepare_data( $ca );
						bookly_oppcs_notifier::send_email_with_merge( 'client_appointment_rejected', 'client' );
						break;
					default:
						report_error( 'UOP', $op, $json, $o_token );
						break;
				}
				
				if ( !$json && isset ( $_SERVER['HTTP_REFERER'] ) ) {
					if ( parse_url( $_SERVER['HTTP_REFERER'], PHP_URL_HOST ) == $_SERVER['HTTP_HOST'] ) {
						// Redirect back to previous page if user came from our site.
						$url = $_SERVER['HTTP_REFERER'];
						wp_redirect( $url );
						exit();
					}
				}
				if ( $json ) { wp_send_json_success(); }
				else {
					$url = get_home_url( null, get_option( 'oppcs_appointment_status_page' ) );
					$query_args_array = array(
							'op' => $op,
							'token' => $_REQUEST[ 'token' ]
					);
					if ( array_key_exists( 'lang', $_GET ) ) { // manual multilingual: WPML home url doesn't work
						$query_args_array[ 'lang' ] = $_GET[ 'lang' ];
					}
					wp_redirect( add_query_arg( $query_args_array, $url ) );
					exit();
				}
				
			} else { // no appointment
				report_error( 'ADE', $op, $json, $o_token );
			}
		} else {
			report_error( 'TNF', $op, $json, $o_token );
		}
	}
}

if ( wp_doing_ajax() ) { // disable time conflict checking since pre-saved appointments always conflict with themselves
	switch ( $_REQUEST[ 'action' ] ) {
		case 'bookly_save_appointment' :
		case 'bookly_save_pending_appointment' :
		case 'bookly_check_cart' :
			add_filter( 'bookly_get_failed_key_time_conflict_disabled', 'bookly_get_failed_key_time_conflict_disabled' );
			break;
		default:
			break;
	}
}
function bookly_get_failed_key_time_conflict_disabled( $_ ) {
	return true;
}

add_action( 'wp_ajax_bookly_save_appointment', 'bookly_save_appointment_override', 0 );
function bookly_save_appointment_override() {
	// bookly_save_appointment is called after payment. normally this is the time to save the
	// appointment, but we already pre-saved and pre-approved it, so now we just have to
	// handle the payment and post the payment into the customer_appointment record
	bookly_oppcs_appointment_data::init();
	bookly_oppcs_appointment_data::read_appointment_from_dbase();
// 	trigger_error( print_r( bookly_oppcs_appointment_data::$ca_data, true ), E_USER_NOTICE );
	$userData = new Bookly\Lib\UserBookingData( bookly_oppcs_appointment_data::$form_id );
	if ( $userData->load() ) {
		
		// not checking for failed keys as the appointment was already saved
		list( $total, $deposit ) = $userData->cart->getInfo();
		$is_payment_disabled  = Bookly\Lib\Config::paymentStepDisabled();
		$is_pay_locally_enabled = Bookly\Lib\Config::paymentTypeEnabled( Lib\Entities\Payment::TYPE_LOCAL );
		if ( $is_payment_disabled || $is_pay_locally_enabled || $deposit <= 0 ) {
			// Handle coupon.
			$coupon = $userData->getCoupon();
			if ( $coupon ) {
				$coupon->claim();
				$coupon->save();
			}
			// Handle payment.
			$payment_id = null;
			if ( ! $is_payment_disabled ) {
				$payment = new Bookly\Lib\Entities\Payment();
				$payment->set( 'status',  Bookly\Lib\Entities\Payment::STATUS_COMPLETED )
				->set( 'paid_type',   Bookly\Lib\Entities\Payment::PAY_IN_FULL )
				->set( 'created',     current_time( 'mysql' ) );
				if ( $coupon && $deposit <= 0 ) {
					// Create fake payment record for 100% discount coupons.
					$payment->set( 'type', Bookly\Lib\Entities\Payment::TYPE_COUPON )
					->set( 'total', 0 )
					->set( 'paid',  0 )
					->save();
					$payment_id = $payment->get( 'id' );
				} elseif ( $is_pay_locally_enabled && $deposit > 0 ) {
					// Create record for local payment.
					$payment->set( 'type', Bookly\Lib\Entities\Payment::TYPE_LOCAL )
					->set( 'total',  $total )
					->set( 'paid',   0 )
					->set( 'status', Bookly\Lib\Entities\Payment::STATUS_PENDING )
					->save();
					$payment_id = $payment->get( 'id' );
				}
				bookly_oppcs_appointment_data::add_payment_id_to_customer_appointment_in_db( $payment_id );
			}
			$ca_list = array( bookly_oppcs_appointment_data::$ca );
			Bookly\Lib\NotificationSender::sendFromCart( $ca_list );
			if ( ! $is_payment_disabled && $payment_id !== null ) {
				$payment->setDetails( $ca_list, $coupon )->save();
			}
			$response = array(
					'success' => true,
			);
		}else {
			$response = array(
				'success'    => false,
				'error_code' => 4,
				'error'      => __( 'Pay locally is not available.', 'bookly' ),
			);
		}
	} else {
		$response = array( 'success' => false, 'error_code' => 1, 'error' => __( 'Session error.', 'bookly' ) );
	}
	wp_send_json( $response );
}

if ( !is_admin() ) {
	add_shortcode( 'oppcs_appointment_status', 'oppcs_appointment_status_shortcode' );
	add_shortcode( 'oppcs-appointments-list', 'oppcs_appointments_list' );
	add_shortcode( 'oppcs-manage-appointment-guests', 'oppcs_manage_appointment_guests_shortcode' );
}

function oppcs_appointment_status_shortcode( $atts, $content = '' ) {
	$args = shortcode_atts( array(), $atts ); // put defaults in array
	$output = '';
	// op, err, token
	if ( array_key_exists( 'error', $_REQUEST ) && $_REQUEST[ 'error' ] ) { // there was an error
		if ( array_key_exists( 'op', $_REQUEST ) && $_REQUEST[ 'op' ] ) { // we were trying to do something
			switch ( $_REQUEST[ 'op' ] ) {
				case 'reject': $err0 = 'ARF'; break;
				case 'approve': $err0 = 'AAF'; break;
				case 'cancel': $err0 = 'ACF'; break;
				default:
					$err0 = '';
					$output .= oppcs_get_translated_error( 'OPF', $_REQUEST[ 'op' ] );
			}
			if ( $err0 ) { $output .= '<p>' . oppcs_get_translated_error( $err0 ) . '</p>'; }
		}
		$err = $_REQUEST[ 'error' ];
		$output .= '<p style="color: darkred; border: 1px solid red; background: pink;">' . oppcs_get_translated_error( $err ) . '</p>';
		return $output;
	}
	// no error, operation was successful
	if ( array_key_exists( 'op', $_REQUEST ) && $_REQUEST[ 'op' ] ) { // we were trying to do something
		switch ( $_REQUEST[ 'op' ] ) {
			case 'reject': $err0 = 'ARS'; break;
			case 'approve': $err0 = 'AAS'; break;
			case 'cancel': $err0 = 'ACS'; break;
			default: $err0 = '';
		}
		if ( $err0 ) { $output .= '<p>' . oppcs_get_translated_error( $err0 ) . '</p>'; }
	} else { // no operation, just status
		if ( array_key_exists( 'token', $_REQUEST) && $_REQUEST[ 'token' ] ) {
			$ca = new Bookly\Lib\Entities\CustomerAppointment();
			$ca->loadBy( array( 'token' => $_REQUEST[ 'token' ] ) );
			$err = array();
			switch ( $ca->get( 'status' ) ) {
				case 'rejected': $err[] = "ASR"; break;
				case 'cancelled': $err[] = "ASC"; break;
				case 'pending': $err[] = "ASP"; break;
				case 'approved':
					$err[] = "ASA";
					if ( null != $ca->get( 'payment_id' ) ) {
						$err[] = 'ACT';
					} else {
						$err[] = 'UNP';
					}
					break;
			}
			foreach ( $err as $e ) {
				$output .= '<p>' . oppcs_get_translated_error( $e ) . '</p>';
			}
		}
	}
	return $output;
}
function oppcs_appointments_list( $attributes, $content ) {
	global $sitepress;
	
	$args = $attributes;
	$output = '';
	// Disable caching.
	Bookly\Lib\Utils\Common::noCache();
	
	$customer = new Bookly\Lib\Entities\Customer();
	$staff = new Bookly\Lib\Entities\Staff();
	$customer->loadBy( array( 'wp_user_id' => get_current_user_id() ) );
	$controller = Bookly\Frontend\Modules\CustomerProfile\Controller::getInstance();
	if ( $customer->isLoaded() ) {
		$role = 'customer';
		if ( $_appointments = $customer->getUpcomingAppointments() ) {
			$λ = function( $p ) { return $this->_translateAppointments( $p ); };
			$appointments = call_as_method( $controller, $λ, $_appointments );
		} else {
			$appointments = array();
		}
// 		$appointments = $this->_translateAppointments( $_appointments );
		$expired      = $customer->getPastAppointments( 1, 1 );
		$more   = ! empty ( $expired['appointments'] );
	} elseif ( $staff->loadBy( array( 'wp_user_id' => get_current_user_id() ) ) ) {
		$staff->loadBy( array( 'wp_user_id' => get_current_user_id() ) );
		if ( $staff->isLoaded() ) {
			
			$role = 'staff';
			$filters = array();
			foreach( $args as $key => $filter ) {
// 				echo "<pre>key:$key, filter:$filter</pre>";
				if ( "_filter" == substr( $key, -7 ) ) {
					$field = substr( $key, 0, -7 );
					if ( in_array( $field, array( 'status', 'payment_id' ) ) ) { $field = 'ca.'.$field; } // status comes from CustomerAppointment
					$filters[ $field  ] = explode( ',', $filter );
					foreach( $filters[ $field ] as &$f ) {
						if ( 'null' == strtolower( $f ) ) {
							$f = null;
						}
					}
// 					echo "<p>field `$field`, count: ".count( $filters[ $field ] ) ."</p>";
					if ( 1 == count( $filters[ $field ] ) ) {
						$filters[ $field ] = $filters[ $field ][ 0 ]; }
				}
			}
// 			echo "<pre>filters:\n";var_dump( $filters ); echo "</pre>";
			$result = get_appointments_for_staff( $staff->get( 'id' ), true, 0, 25, $filters );
// 			echo "<pre>";print_r( $result ); echo "</pre>";
			$_appointments = $result[ 'appointments' ];
			$λ = function( $p ) { return $this->_translateAppointments( $p ); };
			$appointments = call_as_method( $controller, $λ, $_appointments );
			$more = false; // no past appointments for staff
		}
	} else {
		$appointments = array();
		$more   = false;
	}
	
	$any_to_finalize = false;
	foreach ( $appointments as &$a ) { // change approved status to booked if also paid
		if ( 'approved' == $a[ 'appointment_status' ] ) {
			$ca = new Bookly\Lib\Entities\CustomerAppointment();
			$ca->loadBy( array( 'id' => $a[ 'ca_id' ] ) );
			if ( null != $ca->get( 'payment_id' ) ) {
				$a[ 'appointment_status' ] = 'booked';
			} elseif ( 'customer' == $role && strtotime( $a['start_date'] ) > strtotime( 'now' ) ) {
				$any_to_finalize = true;
			}
		}
	}
	if ( !$any_to_finalize && array_key_exists( 'columns', $attributes ) ) {
		$attributes[ 'columns'] = str_replace( ',finalize', '', $attributes[ 'columns'] );
	}

//  	$output .= '<pre>' . print_r( $appointments, true ) . '</pre>';
	
	$allow_cancel = current_time( 'timestamp' );
	$minimum_time_prior_cancel = (int) get_option( 'bookly_gen_min_time_prior_cancel', 0 );
	if ( $minimum_time_prior_cancel > 0 ) {
		$allow_cancel += $minimum_time_prior_cancel * HOUR_IN_SECONDS;
	}
	
	// Prepare URL for AJAX requests.
	$ajax_url = admin_url( 'admin-ajax.php' );
	
	// Support WPML.
	if ( $sitepress instanceof \SitePress ) {
		$ajax_url = add_query_arg( array( 'lang' => $sitepress->get_current_language() ) , $ajax_url );
	}
	
	$titles = array();
	if ( @$args['show_column_titles'] ) {
		$titles = array(
				'category' => Lib\Utils\Common::getTranslatedOption( 'bookly_l10n_label_category' ),
				'service'  => Lib\Utils\Common::getTranslatedOption( 'bookly_l10n_label_service' ),
				'staff'    => Lib\Utils\Common::getTranslatedOption( 'bookly_l10n_label_employee' ),
				'date'     => __( 'Date',   'bookly' ),
				'time'     => __( 'Time',   'bookly' ),
				'price'    => __( 'Price',  'bookly' ),
				'cancel'   => __( 'Cancel', 'bookly' ),
				'status'   => __( 'Status', 'bookly' ),
				'customer_name' => __( 'Customer Name', 'bookly' ),
				'reject_or_accept' => __( 'Reject or Accept', 'bookly' ),
				'finalize'	=> __( 'Finalize', 'bookly' ),
		);
		foreach ( Lib\Utils\Common::getTranslatedCustomFields() as $field ) {
			if ( ! in_array( $field->type, array( 'captcha', 'text-content' ) ) ) {
				$titles[ $field->id ] = $field->label;
			}
		}
	}
	
	$url_cancel = add_query_arg( array( 'action' => 'bookly_cancel_appointment', 'csrf_token' => Lib\Utils\Common::getCsrfToken() ) , $ajax_url );
	
	$controller_render = new ReflectionMethod( 'Bookly\Frontend\Modules\CustomerProfile\Controller', 'render' );
	$controller_render->setAccessible( true );
	$output .= $controller_render->invokeArgs( $controller, array( 'short_code',
		compact( 'ajax_url', 'appointments', 'attributes', 'url_cancel', 'titles', 'more', 'allow_cancel', 'role' ), false ) ); 
	
	return $output;
}

function oppcs_manage_appointment_guests_shortcode( $attributes, $content ) {
	global $oppcs_error_messages, $oppcs_wpml_package;
	$output = '';
	if ( !array_key_exists( 'token', $_REQUEST ) || !$token = $_REQUEST[ 'token' ] ) {
		return '<p>' . oppcs_get_translated_error( 'TNF' ) . '</p>';
	}
	$ca = new Bookly\Lib\Entities\CustomerAppointment();
	$ca->loadBy( array( 'token' => $token ) );
	if( !$ca->isLoaded() ) {
		return '<p>' . oppcs_get_translated_error( 'TNF' ) . '</p>';
	}
	$app = new Bookly\Lib\Entities\Appointment();
	$app->loadBy( array( 'id' => $ca->get( 'appointment_id' ) ) );
	if( !$app->isLoaded() ) {
		return '<p>' . oppcs_get_translated_error( 'TNF' ) . '</p>';
	}
	$staff = new Bookly\Lib\Entities\Staff();
	$staff->loadBy( array( 'id' => $app->get( 'staff_id' ) ) );
	if( !$staff->isLoaded() || $staff->get( 'wp_user_id' ) != get_current_user_id() ) {
		return '<p>' . oppcs_get_translated_error( 'TNF' ) . '</p>';
	}
	$customer = new Bookly\Lib\Entities\Customer();
	$customer->loadBy( array( 'id' => $ca->get( 'customer_id' ) ) );
	if( !$customer->isLoaded() ) {
		return '<p>' . oppcs_get_translated_error( 'TNF' ) . '</p>';
	}
	// TODO add or remove guests here from $_REQUEST
	if ( array_key_exists( 'invite', $_REQUEST ) && $_REQUEST[ 'invite' ] ) {
		invite_disinvite_guest_for_appointment( $app, $_REQUEST[ 'invite' ], true );
	}
	if ( array_key_exists( 'disinvite', $_REQUEST ) && $_REQUEST[ 'disinvite' ] ) {
		invite_disinvite_guest_for_appointment( $app, $_REQUEST[ 'disinvite' ], false );
	}
	$guests = get_guests_for_appointment( $app );
// 	$output .= "<pre>Guests:".implode( ",", $guests )."</pre>";
	$res = separate_guests_into_staff_and_customers( $guests );
	$output .= currently_invited_guests_table( $res );
	
	$res_a = get_all_invitable( array_merge( $guests,
		array( $staff->get( 'wp_user_id' ), $customer->get( 'wp_user_id' ) ) ) );
	$output .= invite_more_guests_table( $res_a );
	return $output;
}
function currently_invited_guests_table( $res ) {
	global $oppcs_wpml_package;
	$output = '';
	$output .= "<table>"
		."<caption>"
		.apply_filters( 'wpml_translate_string', 'Currently invited guests', 'guests-caption', $oppcs_wpml_package )
		."<thead><tr><th style=\"width:50%;\">"
		.apply_filters( 'wpml_translate_string', 'Consultants', 'guests-consultants', $oppcs_wpml_package )
		."</th><th>"
		.apply_filters( 'wpml_translate_string', 'Clients', 'guests-clients', $oppcs_wpml_package )
		."</th></thead>";
	$output .= "<tbody><tr><td>" . output_guest_array( $res[ 'staff' ] ) . "</td>";
	$output .= "<td>" . output_guest_array( $res[ 'customers' ] ) . "</td>";
	$output .= "</tr></tbody></table>";
	return $output;
}
function invite_more_guests_table( $res ) {
	global $oppcs_wpml_package;
	$output = '';
	$output .= "<table>"
		."<caption>"
		.apply_filters( 'wpml_translate_string', 'Invite more guests', 'guests-add-caption', $oppcs_wpml_package )
		."<thead><tr><th style=\"width:50%;\">"
		.apply_filters( 'wpml_translate_string', 'Consultants', 'guests-consultants', $oppcs_wpml_package )
		."</th><th>"
		.apply_filters( 'wpml_translate_string', 'Clients', 'guests-clients', $oppcs_wpml_package )
		."</th></thead>";
	$output .= "<tbody><tr><td>" . guest_array_select_form( $res[ 'staff' ] ) . "</td>";
	$output .= "<td>" . guest_array_select_form( $res[ 'customers' ] ) . "</td>";
	$output .= "</tr></tbody></table>";
	return $output;
}
function invite_disinvite_guest_for_appointment( $app, $guest, $invite = true ) {
	global $wpdb, $oppcs_appointments_guests_table;
	$appointment_id = $app->get( 'id' );
	if ( $invite ) {
		$wpdb->insert( $oppcs_appointments_guests_table,
			array( 'appointment_id' => $appointment_id, 'user_id' => $guest ),
			array( '%d', '%d' ) );
	} else {
		$wpdb->delete( $oppcs_appointments_guests_table,
			array( 'appointment_id' => $appointment_id, 'user_id' => $guest ),
			array( '%d', '%d' ) );
	}
}
function get_participants_for_appointment( $token ) {
	$participants = false;
	$ca = new Bookly\Lib\Entities\CustomerAppointment();
	$ca->loadBy( array( 'token' => $token ) );
	if ( $ca->isLoaded() ) {
		$app = new Bookly\Lib\Entities\Appointment();
		$app->loadBy( array( 'id' => $ca->get( 'appointment_id' ) ) );
		if ( $app->isLoaded() ) {
			$staff = new Bookly\Lib\Entities\Staff();
			$staff->loadBy( array( 'id' => $app->get( 'staff_id' ) ) );
			if ( $staff->isLoaded() ) {
				$customer = new Bookly\Lib\Entities\Customer();
				$customer->loadBy( array( 'id' => $ca->get( 'customer_id' ) ) );
				if ( $customer->isLoaded() ) {
					$participants = get_guests_for_appointment( $app );
					array_push( $participants, $staff->get( 'wp_user_id' ), $customer->get( 'wp_user_id' ) );
				}
			}
		}
	}
	return $participants;
}
function get_guests_for_appointment( $app ) {
	global $wpdb, $oppcs_appointments_guests_table;
	$appointment_id = $app->get( 'id' );
	$sql = "SELECT `user_id` FROM `$oppcs_appointments_guests_table`
					WHERE `appointment_id`=$appointment_id";
// 	echo "<pre>";var_dump( $sql ); echo "</pre>";
	$rows = $wpdb->get_results( $sql, ARRAY_A );
// 	echo "<pre>";var_dump( $rows ); echo "</pre>";
	$guests = array();
	foreach ( $rows as $row ) {
		$guests[] = $row[ 'user_id' ];
	}
	return $guests;
}
function get_all_invitable( $already_invited ) {
	global $wpdb;
	$staff = Bookly\Lib\Entities\Staff::query( 's' )
		->select( '*' )
		->whereNotIn( 'wp_user_id', $already_invited )
		->tableJoin( $wpdb->prefix . 'users', 'u', 's.wp_user_id=u.ID' )
		->havingRaw( 'u.ID IS NOT NULL', array() )
		->sortBy( 'full_name')
		->find();
	$customers = Bookly\Lib\Entities\Customer::query( 'c' )
		->select( '*' )
		->whereNotIn( 'wp_user_id', $already_invited )
		->tableJoin( $wpdb->prefix . 'users', 'u', 'c.wp_user_id=u.ID' )
		->havingRaw( 'u.ID IS NOT NULL', array() )
		->sortBy( 'full_name')
		->find();
	return array( 'staff' => $staff, 'customers' => $customers );
}
function separate_guests_into_staff_and_customers( $guests ) {
	$res = array( 'staff' => array(), 'customers' => array() );
	foreach ( $guests as $wp_user_id ) {
		$staff = new Bookly\Lib\Entities\Staff();
		$staff->loadBy( array( 'wp_user_id' => $wp_user_id ) );
		if ( $staff->isLoaded() ) {
			$res[ 'staff' ][] = $staff;
		} else {
			$customer = new Bookly\Lib\Entities\Customer();
			$customer->loadBy( array( 'wp_user_id' => $wp_user_id ) );
			if ( $customer->isLoaded() ) {
				$res[ 'customers' ][] = $customer;
			}
		}
	}
	return $res;
}
function output_guest_array( $a ) {
	global $oppcs_wpml_package;
	$output = '';
	foreach ( $a as $s ) {
		$output .= "<p style=\"margin: 5px 0; padding: 0;\">";
		$output .= $s->get( 'full_name' ) . " ";
		$output .= "<a class=\"bookly-btn\" style=\"padding: 0 10px 2px !important; "
			."width: auto; min-width:0; display:inline-block; background-color: "
			.get_option('oppcs_destructive_button_color') ."\" href=\""
			.esc_attr( $_SERVER[ 'SCRIPT_URL' ] . "?token=" . $_REQUEST[ 'token' ]
					. "&disinvite=" .$s->get( "wp_user_id" ) )."\">"
			."<span style=\"font-size: 12px !important;\">"
			.apply_filters( 'wpml_translate_string', 'Disinvite', 'button-disinvite', $oppcs_wpml_package )
			."</span>"
			."</a>";
		$output .= "</p>";
	}
	return $output;
}
function guest_array_select_form( $a ) {
	global $oppcs_wpml_package;
// 	echo "<pre>"; var_dump( $a ); echo "</pre>";
	$output = '<form>';
	$output .= '<input type="hidden" name="token" value="' . $_REQUEST[ 'token' ] . '"/>';
	$output .= '<select name="invite" style="font-size: 17px; margin: 5px 0;">';
	foreach ( $a as $s ) {
		$name = is_callable( $s, 'getName' ) ? $s->getName() : $s->get( 'full_name' );
		$output .= '<option value="' . $s->get( "wp_user_id" ) . '">' . $name . '</option>';
	}
	$output .= "</select>";
	$output .= "<input type=\"submit\" class=\"bookly-btn\" style=\"padding: 3px 10px 1px !important; "
		."width: auto; min-width:0; display:inline-block; margin-left: 7px; background-color: "
		.get_option('bookly_app_color') ."; font-size: 12px !important;\" value=\""
		.apply_filters( 'wpml_translate_string', 'Invite', 'button-invite', $oppcs_wpml_package )
		."\" />";
	$output .= "</form>";
	return $output;
}

function set_approval_time_for_ca( $ca ) {
	global $wpdb, $oppcs_customer_appointments_addons_table;
	$sql = "UPDATE $oppcs_customer_appointments_addons_table "
					."SET `approval_time` = NOW() "
					."WHERE `id`=" . $ca->get( 'id' );
	return (bool) $wpdb->query( $sql );
}
function purge_old_approved_unpaid_appointments() {
	global $wpdb, $oppcs_customer_appointments_addons_table, $oppcs_wpml_package;
	$payment_window = 24; // hours
	$expired_ca = Bookly\Lib\Entities\CustomerAppointment::query( 'ca' )
			->select( 'ca.*' )
			->tableJoin( $oppcs_customer_appointments_addons_table, 'addon', 'addon.id=ca.id' )
			->where( 'ca.status', Lib\Entities\CustomerAppointment::STATUS_APPROVED )
			->where( 'ca.payment_id', null )
			->whereRaw( '`addon`.`approval_time` < NOW() - INTERVAL ' . $payment_window . ' HOUR', array() ) 
			->find();
// 	echo"<pre>\n\n\n\n\n\n\n\n";var_dump( $expired_ca);echo"</pre>";
	foreach( $expired_ca as $ca ) {
		$ca->cancel( array( 'cancellation_reason' =>
			sprintf( 
				apply_filters( 'wpml_translate_string', "Payment wasn't posted within %s hours after approval",
				'guests-clients', $oppcs_wpml_package ), $payment_window )
			 ) );
	}
}
function any_past_appointments_for_staff( $staff_id ) {
	$q = Bookly\Lib\Entities\Appointment::query( 'a' )
			->select( '1' )
			->where( 'id', $staff_id )
			->whereLt( 'a.start_date', current_time( 'Y-m-d 00:00:00' ) )
			->limit( 1 );
	return (bool) $q->fetchArray();
}
function get_appointments_for_staff( $staff_id, $upcoming = true, $page = 0, $limit = 25, $filters = array() ) {
	// page and limit are only for past appointments, upcoming ones are sent in one page regardless of numbers
	// time zone of client is disregarded for list for staff
	
	$q = Bookly\Lib\Entities\Appointment::query( 'a' )
		->select( 'ca.id AS ca_id,
			c.name AS category,
			s.title AS service,
			st.full_name AS staff,
			customer.full_name AS customer_name,
			a.staff_id,
			a.staff_any,
			a.service_id,
			s.category_id,
			ca.status AS appointment_status,
			ca.extras,
			ca.compound_token,
			ca.number_of_persons,
			ca.custom_fields,
			ca.appointment_id,
			IF( ca.compound_service_id IS NULL, ss.price, s.price ) * ca.number_of_persons AS price,
			a.start_date AS start_date,
			ca.token' )
		->leftJoin( 'Staff', 'st', 'st.id = a.staff_id' )
		->innerJoin( 'CustomerAppointment', 'ca', 'ca.appointment_id = a.id' )
		->leftJoin( 'Customer', 'customer', 'customer.id = ca.customer_id' )
		->leftJoin( 'Service', 's', 's.id = COALESCE(ca.compound_service_id, a.service_id)' )
		->leftJoin( 'Category', 'c', 'c.id = s.category_id' )
		->leftJoin( 'StaffService', 'ss', 'ss.staff_id = a.staff_id AND ss.service_id = a.service_id' )
		->leftJoin( 'Payment', 'p', 'p.id = ca.payment_id' )
		->where( 'a.staff_id', $staff_id )
		->sortBy( 'start_date' )
		->order( 'DESC' );
	foreach( $filters as $field => $values ) {
		if ( is_array( $values ) ) {
			$q->whereIn( $field, $values );
		} else {
			$q->where( $field, $values );
		}
	}
//  	echo "<p style=\"border: 1px solid blue; font-family: verdana;\">".$q->composeQuery()."</p>";
	if ( $upcoming ) {
		$q->whereGte( 'a.start_date', current_time( 'Y-m-d 00:00:00' ) );
		$records = $q->fetchArray();
		$more = any_past_appointments_for_staff( $staff_id );
	} else { // past appointments
		$q->whereLt( 'a.start_date', current_time( 'Y-m-d 00:00:00' ) )
			->limit( $limit + 1 ) // let's get one more, that'll signal if there's more to fetch later
			->offset( ( $page - 1 ) * $limit );
		$records = $q->fetchArray();
		if ( $more = count( $records) > $limit ) {
			array_pop( $records ); // lose the last one, we only used it as a flag
		}
	}
	return array( 'more' => $more, 'appointments' => $records );
}

function dom_from_string( $s ) {
	$dom = new DOMDocument;
	$dom->loadHTML( mb_convert_encoding( $s, 'HTML-ENTITIES', 'UTF-8') );
	return $dom;
}
function string_from_dom( $dom ) {
	return preg_replace(array("/^\<\!DOCTYPE.*?<html><body>/si",
			"!</body></html>$!si"),
			"",$dom->saveHTML());
}
function elements_from_xpath_string( $dom, $xstring ) {
	$xpath = new DOMXPath( $dom );
	return $xpath->query( $xstring );
}
function one_element_from_xpath_string( $dom, $xstring ) {
	$elems = elements_from_xpath_string( $dom, $xstring );
	if ( 0 == $elems->length ) {
		return null;
	}
	return $elems->item( 0 );
}

function get_var_from_session ( $form_id, $var ) {
	if ( 		array_key_exists( 'bookly-oppcs', $_SESSION )
		&&	array_key_exists( $form_id, $_SESSION[ 'bookly-oppcs' ] )
		&&	array_key_exists( $var, $_SESSION[ 'bookly-oppcs' ][ $form_id ] )) {
			return $_SESSION[ 'bookly-oppcs' ][ $form_id ][ $var ];
	} else {
		return false;
	}
}
function set_var_in_session ( $form_id, $var, $val ) {
	if ( !array_key_exists( 'bookly-oppcs', $_SESSION ) ) {
		$_SESSION[ 'bookly-oppcs' ] = array();
	}
	if ( !array_key_exists( $form_id, $_SESSION[ 'bookly-oppcs' ] ) ) {
		$_SESSION[ 'bookly-oppcs' ][ $form_id ] = array();
	}
	$_SESSION[ 'bookly-oppcs' ][ $form_id ][ $var ] = $val; 
}
function get_form_id( $userData ) {
	$λ = function() {
		return $this->form_id;
	};
	$get_form_id = $λ->bindTo( $userData, $userData );
	return $get_form_id();
}
function get_property( $o, $p ) { // also returns private and protected properties
	$λ = function ( $p ) { return $this->$p; };
	$get = $λ->bindTo( $o, $o );
	return $get( $p );
}
function call_as_method( $o, $λ ) { // accepts additional varargs
	$args = func_get_args();
	array_shift( $args ); array_shift( $args );
// 	echo "<pre>".print_r($args,true)."</pre>";
	$m = $λ->bindTo( $o, $o );
	return call_user_func_array( $m, $args );
}