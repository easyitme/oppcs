<?php

/**
 * Plugin Name: OPPCS Helpers
 * Plugin URI: http://easyitme.com
 * Description: Tracks provider-client relationships, updates profile pages, generates video room if there is an active appointment. Depends on: Bookly, Formidable Pro.
 * Version: 2.0.1
 * Author: Peter Szoldan
 * Author URI: http://easyitme.com
 * License: strict
 * Depends on: Appointment-booking, Rating, Timezone-detector
 */

global $oppcs_helpers_db_version;
$oppcs_helpers_db_version= '2.0.1';

global $oppcs_provider_client_link_table_name, $wpdb,
	$oppcs_provider_client_link_table_name, $oppcs_appointments_addons_table,
	$oppcs_appointments_table, $oppcs_appointments_guests_table,
	$oppcs_customer_appointments_table, $oppcs_customer_appointments_addons_table,
	$oppcs_customers_table, $oppcs_staff_table,
	$oppcs_notifications_table;
$oppcs_provider_client_link_table_name = $wpdb->prefix . 'oppcs_provider_client_links';
$oppcs_appointments_addons_table = $wpdb->prefix . 'oppcs_ab_appointments_addons';
$oppcs_appointments_table = $wpdb->prefix . 'ab_appointments';
$oppcs_appointments_guests_table = $wpdb->prefix . 'oppcs_appointments_guests';
$oppcs_customer_appointments_table = $wpdb->prefix . 'ab_customer_appointments';
$oppcs_customer_appointments_addons_table = $wpdb->prefix . 'oppcs_ab_customer_appointments_addons';
$oppcs_customers_table = $wpdb->prefix . 'ab_customers';
$oppcs_staff_table = $wpdb->prefix . 'ab_staff';
$oppcs_notifications_table = $wpdb->prefix . 'oppcs_notifications';

global $oppcs_wpml_package, $oppcs_wpml_notifications_package, $oppcs_wpml_error_messages_package;
$oppcs_wpml_package = array(
	'kind' => 'OPPCS Helpers',
	'name' => 'objects',
	'title' => 'OPPCS Bookly Addons',
	'edit_link' => '',
	'view_link' => ''
);
$oppcs_wpml_notifications_package = array(
		'kind' => 'OPPCS Helpers',
		'name' => 'notifications',
		'title' => 'OPPCS Bookly Notifications',
		'edit_link' => '',
		'view_link' => ''
);
$oppcs_wpml_error_messages_package = array(
		'kind' => 'OPPCS Helpers',
		'name' => 'errors',
		'title' => 'OPPCS Bookly Error Messages',
		'edit_link' => '',
		'view_link' => ''
);
global $oppcs_error_messages;
$oppcs_error_messages = _oppcs_error_messages();
function _oppcs_error_messages() { return array(
	'TNF'	=> 'This token does not refer to an existing appointment.',
	'ARF'	=> 'Appointment rejection failed.',
	'AAF'	=> 'Appointment approval failed.',
	'ACF'	=> 'Appointment cancellation failed.',
	'ARS'	=> 'Appointment was rejected.',
	'AAS'	=> 'Appointment was approved.',
	'ACS'	=> 'Appointment was cancelled.',
	'AAA'	=> 'Appointment was already approved.',
	'AAR'	=> 'Appointment was already rejected.',
	'AAC'	=> 'Appointment was already cancelled.',
	'ADE'	=> 'Appointment doesn\'t exist.',
	'TCC'	=> 'Appointment too close to cancel.',
	'ASP'	=> 'Appointment is waiting for provider to accept or reject.',
	'ASR'	=> 'Appointment was rejected by provider.',
	'ASC'	=> 'Appointment was cancelled by client.',
	'ASA'	=> 'Appointment was approved by provider.',
	'UNP'	=> 'Appointment is unpaid.',
	'ACT'	=> 'Appointment is paid and active.',
	'UOP'	=> 'Unknown operation.',
	'OPF'	=> 'Operation <strong>%s</strong> failed.',
	'UNK'	=> 'An unspecified error occurred.',
); }
function oppcs_get_translated_error( $err ) {
	global $oppcs_error_messages, $oppcs_wpml_error_messages_package;
	if ( !array_key_exists( $err, $oppcs_error_messages) ) {
		$err = 'UNK';
	}
	$msg = apply_filters( 'wpml_translate_string', $oppcs_error_messages[ $err ],
										'error-' . strtolower( $err ), $oppcs_wpml_error_messages_package );
	$args = func_get_args();
	$args[ 0 ] = $msg;
	$translated_msg = call_user_func_array( 'sprintf', $args );
	return $translated_msg;	
}
function oppcs_get_translated_error_paragraph() {
	$args = func_get_args();
	$msg = call_user_func_array( 'oppcs_get_translated_error', $args );
	return '<p style="border: 1px solid red; background-color: pink; color: darkred;">' . $msg . '</p>';
}

foreach(scandir(__DIR__) as $f){
	$spl = new SplFileInfo(__DIR__ . '/' . $f);
	if ($spl->isFile() && 'php' == $spl->getExtension()){
		require_once($spl->getPathname());
	}
}
register_activation_hook( __FILE__, 'oppcs_helpers_install');
register_activation_hook( __FILE__, 'register_wpml_strings');
register_deactivation_hook( __FILE__, 'oppcs_helpers_uninstall');
register_deactivation_hook( __FILE__, 'deregister_wpml_strings');
function printWpdbErrorAsAdminNotice($act){
	global $wpdb;
	$errStr = $wpdb->use_mysqli ? mysqli_error( $wpdb->dbh) : mysql_error( $wpdb->dbh );
	if ($errStr) {
		ob_start();
		$wpdb->show_errors();
		$wpdb->print_error();
		$wpdb->hide_errors();
		$err = ob_get_contents();
		ob_end_clean();
	}else{
		$err = false;
	}

	echo $err ? preg_replace('<div id="error">', "<div class=\"error\"><p>$act</p>", $err)
					:  "<div class=\"updated\"><p>$act</p></div>";
}
function oppcs_admin_notice(){ // class = updated | error | update-nag
	$msg = get_option("oppcs_installation_message");
	if($msg){	echo $msg; }
	delete_option("oppcs_installation_message");
}
add_action('admin_notices', 'oppcs_admin_notice');
function oppcs_helpers_install(){
	global $wpdb, $oppcs_helpers_db_version, $oppcs_provider_client_link_table_name,
		$oppcs_provider_client_link_table_name, $oppcs_appointments_addons_table, 
		$oppcs_appointments_table, $oppcs_appointments_guests_table,
		$oppcs_customer_appointments_table, $oppcs_customer_appointments_addons_table, 
		$oppcs_customers_table, $oppcs_staff_table,
		$oppcs_notifications_table;
	global $oppcs_admin_notices;
	$charset_collate = $wpdb->get_charset_collate();

	ob_start(NULL, 0, PHP_OUTPUT_HANDLER_STDFLAGS);
	
	$wpdb->hide_errors();

	$sql = "CREATE TABLE $oppcs_provider_client_link_table_name (
		provider_id bigint(20) unsigned NOT NULL,
		client_id int(10) unsigned NOT NULL,
		PRIMARY KEY  (provider_id,client_id)
	) ENGINE=InnoDB $charset_collate";
	printWpdbErrorAsAdminNotice("Creating table $oppcs_provider_client_link_table_name.");
	
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	// add trigger to create provider client links
	$sql = "DROP TRIGGER IF EXISTS `OppcsProvCliLink`";
	$wpdb->query($sql);
	printWpdbErrorAsAdminNotice("Dropping trigger if exists `OppcsProvCliLink`.");
	
	$sql = "CREATE TRIGGER `OppcsProvCliLink` AFTER INSERT ON `$oppcs_customer_appointments_table`
					FOR EACH ROW BEGIN
						SET @staff_id = (SELECT `staff_id` FROM `$oppcs_appointments_table` WHERE `id` = NEW.appointment_id);
						SET @staff_wp_id = (SELECT `wp_user_id` FROM `$oppcs_staff_table` WHERE `id` = @staff_id);
						SET @customer_wp_id = (SELECT `wp_user_id` FROM `$oppcs_customers_table` WHERE `id` = NEW.customer_id);
						INSERT IGNORE INTO `$oppcs_provider_client_link_table_name`
						VALUES (@staff_wp_id, @customer_wp_id);
					END";
	$wpdb->query($sql);
	printWpdbErrorAsAdminNotice("Creating trigger `OppcsProvCliLink` to store provider - client links.");
	
	// generate links for existing appointments
	$sql = "INSERT IGNORE INTO `$oppcs_provider_client_link_table_name`
					SELECT `staff`.`wp_user_id` AS `provider_id`, `customer`.`wp_user_id` AS `client_id` FROM `$oppcs_appointments_table` AS `t1`
					JOIN `$oppcs_customer_appointments_table` AS `t2` ON `t1`.`id` = `t2`.`appointment_id`
					JOIN `$oppcs_staff_table` AS `staff` ON `staff`.`id` = `t1`.`staff_id`
					JOIN `$oppcs_customers_table` AS `customer` ON `customer`.`id` = `t2`.`customer_id`
	";
	$wpdb->query($sql);
	printWpdbErrorAsAdminNotice("Initializing provider - client links from existing appointments.");
	
	$sql = "CREATE TABLE $oppcs_appointments_addons_table (
		id INT(10) UNSIGNED NOT NULL,
		status ENUM('pending','accepted','rejected','cancelled','booked') NOT NULL,
		UID BIGINT(20) UNSIGNED NOT NULL,
		PRIMARY KEY  (id),
		KEY status (status),
		UNIQUE KEY UID (UID)
	) ENGINE=InnoDB $charset_collate";
	dbDelta( $sql );
	printWpdbErrorAsAdminNotice("Creating table $oppcs_appointments_addons_table.");

	$foreign_key_constraint_name = "fk_appointment_id";
	
	$sql = "ALTER TABLE $oppcs_appointments_addons_table DROP FOREIGN KEY $foreign_key_constraint_name";
	$wpdb->query($sql);
	printWpdbErrorAsAdminNotice("Dropping foreign key constraint for appointment ids.");
	
	$sql = "ALTER TABLE $oppcs_appointments_addons_table
		ADD CONSTRAINT $foreign_key_constraint_name
		FOREIGN KEY (id)
		REFERENCES $oppcs_appointments_table(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
	";
	$wpdb->query($sql);
	printWpdbErrorAsAdminNotice("Recreating foreign key constraint for appointment ids.");
	
	$uid_generator = "146564756945 + FLOOR( 831646658765 * RAND() )";
	
	// generate UIDs where it doesn't exist
	$sql = "INSERT IGNORE INTO $oppcs_appointments_addons_table
		( `id`, `status`, `UID` )
		SELECT `id`, \"booked\", $uid_generator FROM $oppcs_appointments_table";
	$wpdb->query($sql);
	printWpdbErrorAsAdminNotice("Generating UIDs for existing appointments.");
	
	$sql = "DROP TRIGGER IF EXISTS `OppcsGenerateUID`";
	$wpdb->query($sql);
	printWpdbErrorAsAdminNotice("Dropping trigger `OppcsGenerateUID` if exists.");
		
	$sql= "CREATE TRIGGER `OppcsGenerateUID` AFTER INSERT ON `$oppcs_appointments_table`
					FOR EACH ROW BEGIN
						INSERT INTO $oppcs_appointments_addons_table ( `id`, `status`, `UID` )
						VALUES ( NEW.id, \"pending\", $uid_generator );
					END";
	$wpdb->query($sql);
	printWpdbErrorAsAdminNotice("Creating trigger `OppcsGenerateUID` to generate UIDs for new appointments.");

	$sql = "CREATE TABLE $oppcs_customer_appointments_addons_table (
		id INT(10) UNSIGNED NOT NULL,
		session VARCHAR( 8192 ),
		approval_time DATETIME NULL,
		rate_notification_sent TINYINT(1),
		PRIMARY KEY  (id),
		KEY approval_time ( approval_time ),
		KEY rate_notification_sent ( rate_notification_sent )
	) ENGINE=InnoDB $charset_collate";
	dbDelta( $sql );
	printWpdbErrorAsAdminNotice("Creating table $oppcs_customer_appointments_addons_table.");
	
	$foreign_key_constraint_name = "fk_customer_appointment_id";
	
	//$fk_name, $table, $col, $reft, $refc
	drop_readd_foreign_key_constraint(
		"fk_customer_appointment_id", // foreign key constraint name
		$oppcs_customer_appointments_addons_table, "id",  // constrained column
		$oppcs_customer_appointments_table, "id" ); // references

	// $oppcs_notifications_table
	$sql = "CREATE TABLE $oppcs_notifications_table (
	id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	type VARCHAR(64) NOT NULL,
	active TINYINT(1) NOT NULL,
	subject VARCHAR(255) NOT NULL,
	message TEXT NOT NULL,
	PRIMARY KEY  (id),
	UNIQUE KEY type (type)
	) ENGINE=InnoDB $charset_collate";
	dbDelta( $sql );
	printWpdbErrorAsAdminNotice("Creating table $oppcs_notifications_table.");
	
	require_once oppcs_path( 'default_notifications.php' );
	$inserted = 0;
	add_filter( 'query', 'insert_ignore_filter' ); // add filter to change insert to insert ignore
	foreach( default_oppcs_notifications() as $row ){
		$inserted += $wpdb->insert( $oppcs_notifications_table, $row );		
	}
	remove_filter( 'query', 'insert_ignore_filter' ); // back to normal...
	printWpdbErrorAsAdminNotice( !$inserted ? "No default notifications loaded." : 
			( "Loaded defaults for $inserted notification" . ( 1 == $inserted ? '' : 's' ) . ".") );

	// $oppcs_appointments_guests_table
	$sql = "CREATE TABLE $oppcs_appointments_guests_table (
	appointment_id INT(10) UNSIGNED NOT NULL,
	user_id BIGINT(20) UNSIGNED NOT NULL,
	PRIMARY KEY  ( appointment_id, user_id ),
	KEY appointment_id ( appointment_id ),
	KEY user_id ( user_id )
	) ENGINE=InnoDB $charset_collate";
	dbDelta( $sql );
	printWpdbErrorAsAdminNotice("Creating table $oppcs_appointments_guests_table.");
	
	drop_readd_foreign_key_constraint( 
		"fk_guests_appointment_id", // foreign key constraint name
		$oppcs_appointments_guests_table, "appointment_id",  // constrained column
		$oppcs_appointments_table, "id" ); // references

	drop_readd_foreign_key_constraint(
		"fk_guests_user_id", // foreign key constraint name
			$oppcs_appointments_guests_table, "user_id",  // constrained column
			$wpdb->prefix . "users", "ID" ); // references
		
	add_option( 'oppcs_helpers_db_version', $oppcs_helpers_db_version );

	// set default settings values
	if ( false === get_option ( 'video_available_before_appointment_start_minutes' ) ) {
		add_option('video_available_before_appointment_start_minutes', 15);
	}

	$oppcs_admin_notices = ob_get_contents();
	ob_end_clean();
	add_option('oppcs_installation_message', $oppcs_admin_notices);
}
function drop_readd_foreign_key_constraint( $fk_name, $table, $col, $reft, $refc ) {
	global $wpdb;
	
	$sql = 	"ALTER TABLE $table DROP FOREIGN KEY $fk_name";
	$wpdb->query($sql);
	printWpdbErrorAsAdminNotice("Dropping foreign key constraint for $table $col.");
	
	$sql = "ALTER TABLE $table ADD CONSTRAINT $fk_name
					FOREIGN KEY ( $col )
					REFERENCES $reft ( $refc )
					ON DELETE CASCADE
					ON UPDATE CASCADE
	";
	$wpdb->query($sql);
	printWpdbErrorAsAdminNotice("Recreating foreign key constraint for $table $col.");
}
function register_wpml_strings(){
	global $oppcs_wpml_package, $oppcs_wpml_notifications_package, $sitepress,
				$wpdb, $oppcs_notifications_table, $oppcs_error_messages, $oppcs_wpml_error_messages_package;
	function register_strings_for_package( $strings, $p ) {
		foreach ( $strings as $s ) {
			do_action( 'wpml_register_string', $s[ 'value' ], $s[ 'name' ], $p, $s[ 'title' ], $s[ 'type' ] );
		}
	}
	$objects_strings = array(
			array(	'value' => "Accept", 'name' => 'button-accept',
					'title' => 'Text on button to accept appointment', 'type' => 'LINE' ),
			array(	'value' => "Reject", 'name' => 'button-reject',
					'title' => 'Text on button to reject appointment', 'type' => 'LINE' ),
			array(	'value' => "Cancel", 'name' => 'button-cancel',
					'title' => 'Text on button to cancel appointment', 'type' => 'LINE' ),
			array(	'value' => "Make new appointment", 'name' => 'button-new-booking',
					'title' => 'Text on button to make new appointment', 'type' => 'LINE' ),
			array(	'value' => "Finalize appointment", 'name' => 'button-finalize',
					'title' => 'Text on button to finalize appointment', 'type' => 'LINE' ),
			array(	'value' => "Invite", 'name' => 'button-invite',
					'title' => 'Text on button to invite guest to appointment', 'type' => 'LINE' ),
			array(	'value' => "Disinvite", 'name' => 'button-disinvite',
					'title' => 'Text on button to disinvite guest to appointment', 'type' => 'LINE' ),
			array(	'value' => "Rate video", 'name' => 'button-rate-video',
					'title' => 'Text on button to rate video experience', 'type' => 'LINE' ),
			array(	'value' => "Currently invited guests", 'name' => 'guests-caption',
					'title' => 'Caption of invited guests table', 'type' => 'LINE' ),
			array(	'value' => "Consultants", 'name' => 'guests-consultants',
					'title' => 'Staff members invited as guests to appointment', 'type' => 'LINE' ),
			array(	'value' => "Clients", 'name' => 'guests-clients',
					'title' => 'Additional clients invited as guests to appointment', 'type' => 'LINE' ),
			array(  'value' => "Payment wasn't posted within %s hours after approval", 'name' => 'notification-autocancel',
					'title' => 'Cancellation reason when payment is not posted within given time after approval',
					'type' => 'LINE' ),
			array(  'value' => "We have received your appointment request and have notified the provider. "
										."We have also sent you a confirmation email. As soon as the provider accepts "
										."or rejects the request we will notify you via email.",
					'name' => 'notification-request-received',
					'title' => 'Message for client on the frontend when the reservation is received but not processed yet.',
					'type' => 'AREA' )
	);
	$notification_strings = array();
	$notifications = $wpdb->get_results( "SELECT * FROM $oppcs_notifications_table", ARRAY_A );
	foreach ( $notifications as $n ) {
		$notification_strings[] = array(
				'value' => $n[ 'subject' ], 'name' => $n[ 'type' ]."_subject",
				'title' => $n[ 'type' ]."_subject", 'type' => 'LINE'
		);
		$notification_strings[] = array(
				'value' => $n[ 'message' ], 'name' => $n[ 'type' ]."_message",
				'title' => $n[ 'type' ]."_message", 'type' => 'AREA'
		);
	}
	$error_strings = array();
	foreach( $oppcs_error_messages as $code => $msg ) {
		$error_strings[] = array(	'value' => $msg,
					'name' => 'error-' . strtolower( $code ),
					'title' => $msg, 'type' => 'AREA' );
	}
	$orig_lang = $sitepress->get_default_language();
	$sitepress->set_default_language( 'en' ); // register strings as English
	register_strings_for_package( $objects_strings, $oppcs_wpml_package );
	register_strings_for_package( $notification_strings, $oppcs_wpml_notifications_package );
	register_strings_for_package( $error_strings, $oppcs_wpml_error_messages_package );
	$sitepress->set_default_language( $orig_lang );
}
function oppcs_helpers_uninstall(){
	global $wpdb, $oppcs_helpers_db_version, $oppcs_provider_client_link_table_name, $oppcs_appointments_addons_table;
	$wpdb->show_errors();

	$wpdb->query("DROP TRIGGER IF EXISTS `OppcsProvCliLink`");
	$wpdb->query("DROP TRIGGER IF EXISTS `OppcsGenerateUID`");
	$wpdb->query("DROP TABLE $oppcs_provider_client_link_table_name");
//	$wpdb->query("DROP TABLE $oppcs_appointments_addons_table");
// not deleting this so when the plugin is reenabled, the UIDs are not regenerated
	//	$wpdb->query("DROP TABLE $oppcs_customer_appointments_addons_table");
	//	$wpdb->query("DROP TABLE $oppcs_notifications_table");

}
function deregister_wpml_strings() {
	global $oppcs_wpml_package;
// 	do_action( 'wpml_delete_package_action', $oppcs_wpml_package[ 'name' ],
// 			$oppcs_wpml_package[ 'kind' ] );
// 	do_action( 'wpml_delete_package_action', $oppcs_wpml_notifications_package[ 'name' ],
// 			$oppcs_wpml_notifications_package[ 'kind' ] );
// 	do_action( 'wpml_delete_package_action', $oppcs_wpml_error_messages_package[ 'name' ],
// 			$oppcs_wpml_error_messages_package[ 'kind' ] );
}

add_action('init', 'load_translations');
function load_translations(){
	load_plugin_textdomain( 'OPPCS', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
// 	global $l10n;
// 	echo"<pre>".print_r($l10n, true)."</pre>";exit();
}

// admin settings
define("SETTINGS_PAGE_NAME", "oppcs_plugin");
add_action('admin_menu', 'plugin_admin_add_page');
function plugin_admin_add_page() {
	add_options_page('OPPCS Helper', 'OPPCS Helper Settings', 'manage_options', SETTINGS_PAGE_NAME, 'oppcs_plugin_options_page');
}
function oppcs_plugin_add_settings_link($links){
	$l = '<a href="options-general.php?page='.SETTINGS_PAGE_NAME.'">' . ( 'Settings' ) . '</a>';
	array_push( $links, $l);
	return $links;
}
add_filter( "plugin_action_links_" . plugin_basename(__FILE__), 'oppcs_plugin_add_settings_link');
function oppcs_plugin_options_page(){
	echo "<div>";
	echo "<h2>OPPCS Helper Settings</h2>";
	echo "<form action=\"options.php\" method=\"post\">";
	settings_fields('oppcs_plugin_options');
	do_settings_sections('oppcs_plugin');
	echo "<p class=\"submit\"><input name=\"submit\" type=\"submit\" value=\"Save Changes\" /></p>";
	echo "</form>";
	echo "</div>";
}

add_action('admin_init', 'oppcs_plugin_admin_init');
function oppcs_plugin_admin_init(){
	// soon minutes setting
	register_setting('oppcs_plugin_options', 'video_available_before_appointment_start_minutes', 'oppcs_number_validate');
	add_settings_section('video_options', 'Video Options', 'video_options_text', 'oppcs_plugin');
	add_settings_field('video_available_before_appointment_start_minutes',
		'Video room is available this many minutes before the meeting starts, enter a value between 0 and 1440',
		'soon_minutes_input', 'oppcs_plugin', 'video_options');

	// after minutes setting
	register_setting('oppcs_plugin_options', 'video_available_after_appointment_end_minutes', 'oppcs_number_validate');
	add_settings_field('video_available_after_appointment_end_minutes',
	'Video room is available this many minutes after the meeting ends, enter a value between 0 and 1440',
	'oppcs_after_minutes_input', 'oppcs_plugin', 'video_options');
	
	// shortcode pattern settings
	register_setting('oppcs_plugin_options', 'video_shortcode_pattern');
	add_settings_field('video_shortcode_pattern',
		'Shortcode pattern to use to generate the video shortcode. '
		.'You may include %ID%, %TITLE%, and %UID%, '
		.'which will be filled with the corresponding parameters. '
		.'This pattern helps the OPPCS plugin to cooperate with third-party webrtc plugins '
		.'without releasing the OPPCS plugin again.',
		'oppcs_shortcode_pattern_input', 'oppcs_plugin', 'video_options');

	// formidable pro settings
	register_setting('oppcs_plugin_options', 'oppcs_formidable_pro_form_id', 'oppcs_number_validate');
	add_settings_section('formidable_options', 'Formidable Pro Client Records Options', 'formidable_options_text', 'oppcs_plugin');
	add_settings_field('oppcs_formidable_pro_form_id',
		'Client records are kept using the Formidable Pro form with this id',
		'oppcs_formidable_pro_form_id_input', 'oppcs_plugin', 'formidable_options');
	
	register_setting('oppcs_plugin_options', 'oppcs_note_attach_minutes', 'oppcs_number_validate');
	add_settings_field('oppcs_note_attach_minutes',
		'Client records are shown on the same row with the session if they are created between '
		.'this many minues before the start of the session and this many minues after the end of the session',
		'oppcs_note_attach_minutes_input', 'oppcs_plugin', 'formidable_options');
	
	// bookly addon settings
	add_settings_section('oppcs_bookly_addons', 'Bookly Addons Options', 'oppcs_bookly_addons_text', 'oppcs_plugin');
	register_setting('oppcs_plugin_options', 'oppcs_appointment_status_page' );
	add_settings_field( 'oppcs_appointment_status_page',
		'Select page containing appointment status shortcode. This page will be shown after a change '
		.'in appointment status.',
		'oppcs_appointment_status_page_select', 'oppcs_plugin', 'oppcs_bookly_addons'	);
	
	register_setting('oppcs_plugin_options', 'oppcs_new_booking_page' );
	add_settings_field( 'oppcs_new_booking_page',
			'Select page for booking new appointment. "New Appointment" buttons will '
			.'link to this page.',
			'oppcs_new_booking_page_select', 'oppcs_plugin', 'oppcs_bookly_addons'	);

	register_setting('oppcs_plugin_options', 'oppcs_pending_staff_appointments_page' );
	add_settings_field( 'oppcs_pending_staff_appointments_page',
			'Select page for staff pending appointments page. Staff notification emails will '
			.'link to this page.',
			'oppcs_pending_staff_appointments_page_select', 'oppcs_plugin', 'oppcs_bookly_addons'	);

	register_setting('oppcs_plugin_options', 'oppcs_rate_video_page' );
	add_settings_field( 'oppcs_rate_video_page',
			'Select page for video rating. Video rating notification emails will link to this page.',
			'oppcs_rate_video_page_select', 'oppcs_plugin', 'oppcs_bookly_addons'	);
			
	register_setting('oppcs_plugin_options', 'oppcs_destructive_button_color' );
	add_settings_field( 'oppcs_destructive_button_color',
			'Select color for destructive buttons.',
			'oppcs_destructive_button_color_input', 'oppcs_plugin', 'oppcs_bookly_addons'	);
						
}
function video_options_text(){
	echo "<p>Settings related to video meetings.</p>";
}
function formidable_options_text(){
	echo "<p>Settings related to Formidable Pro client record management.</p>";
}
function oppcs_bookly_addons_text() {
	echo "<p>Settings related to bookly addons.</p>";
}
function soon_minutes_input(){
	$opt = get_option('video_available_before_appointment_start_minutes');
	echo "<input id='video_available_before_appointment_start_minutes' name='video_available_before_appointment_start_minutes' "
			." type='number' min='0' max='1440' value='{$opt}' />";
}
function oppcs_after_minutes_input(){
	$opt = get_option('video_available_after_appointment_end_minutes');
	echo "<input id='video_available_after_appointment_end_minutes' name='video_available_after_appointment_end_minutes' "
			." type='number' min='0' max='1440' value='{$opt}' />";
}
function oppcs_formidable_pro_form_id_input(){
	$opt = get_option('oppcs_formidable_pro_form_id');
	echo "<input id='oppcs_formidable_pro_form_id_input' name='oppcs_formidable_pro_form_id' "
			." type='number' min='1' value='{$opt}' />";
}
function oppcs_note_attach_minutes_input(){
	$opt = get_option('oppcs_note_attach_minutes');
	echo "<input id='oppcs_note_attach_minutes_input' name='oppcs_note_attach_minutes' "
			." type='number' min='0' value='{$opt}' />";
}
function oppcs_shortcode_pattern_input(){
	$opt = get_option('video_shortcode_pattern');
	echo "<textarea id='video_shortcode_pattern' name='video_shortcode_pattern' "
			." rows=\"4\" cols=\"80\" "
			."placeholder='Example: [twilio_video_feed id=\"%ID%\" title=\"%TITLE%\"]'>"
			.$opt."</textarea>";
}
function oppcs_appointment_status_page_select() {
	$opt = esc_attr( get_option( 'oppcs_appointment_status_page' ) );
	echo '<select class="form-control" name="oppcs_appointment_status_page" id="oppcs_appointment_status_page">';
	$pages = get_pages();
	foreach($pages as $page){
		echo "<option value=\"{$page->post_name}\"".($opt == $page->post_name ? " selected" : "").">{$page->post_title}</option>";
	}
	echo "</select>";
}
function oppcs_new_booking_page_select() {
	$opt = esc_attr( get_option( 'oppcs_new_booking_page' ) );
	echo '<select class="form-control" name="oppcs_new_booking_page" id="oppcs_new_booking_page">';
	$pages = get_pages();
	foreach($pages as $page){
		echo "<option value=\"{$page->post_name}\"".($opt == $page->post_name ? " selected" : "").">{$page->post_title}</option>";
	}
	echo "</select>";
}
function oppcs_pending_staff_appointments_page_select() {
	$opt = esc_attr( get_option( 'oppcs_pending_staff_appointments_page' ) );
	echo '<select class="form-control" name="oppcs_pending_staff_appointments_page" id="oppcs_pending_staff_appointments_page">';
	$pages = get_pages();
	foreach($pages as $page){
		echo "<option value=\"{$page->post_name}\"".($opt == $page->post_name ? " selected" : "").">{$page->post_title}</option>";
	}
	echo "</select>";
}
function oppcs_rate_video_page_select() {
	$opt = esc_attr( get_option( 'oppcs_rate_video_page' ) );
	echo '<select class="form-control" name="oppcs_rate_video_page" id="oppcs_rate_video_page">';
	$pages = get_pages();
	foreach($pages as $page){
		echo "<option value=\"{$page->post_name}\"".($opt == $page->post_name ? " selected" : "").">{$page->post_title}</option>";
	}
	echo "</select>";
}
add_action( 'admin_enqueue_scripts', 'mw_enqueue_color_picker' );
function mw_enqueue_color_picker( $hook_suffix ) {
	// first check that $hook_suffix is appropriate for your admin page
	wp_enqueue_style( 'wp-color-picker' );
	wp_enqueue_script( 'destructive-color-picker',
			plugins_url('destructive-color-picker.js', __FILE__ ),
			array( 'wp-color-picker' ), false, true );
}
function oppcs_destructive_button_color_input(){
	$opt = get_option('oppcs_destructive_button_color');
	echo "<input id='oppcs_destructive_button_color_input' name='oppcs_destructive_button_color' "
			." value='{$opt}' />";
	
}
function oppcs_number_validate($input){ return 1 * $input; }

function insert_ignore_filter( $query ) {
	return preg_replace('/^(INSERT INTO)/i', 'INSERT IGNORE INTO', $query, 1 );
}

function join_paths() {
	$path_parts = func_get_args();
	foreach( $path_parts as $k => &$pp ) {
		if ( 0 < $k ) { $pp = ltrim( $pp, "/\\" ); }
		if ( $k < count( $path_parts ) - 1 ) { $pp = rtrim( $pp, "/\\" ); }
	}
	return implode( DIRECTORY_SEPARATOR, $path_parts );
}
function oppcs_path( $path ) {
	if ( !is_array( $path ) ) { $path = array( $path ); }
	array_unshift( $path, plugin_dir_path( __FILE__ ) );
	return call_user_func_array( 'join_paths', $path );
}
