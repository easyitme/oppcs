<?php
add_action( 'wp_ajax_frm_show_entry', 'frm_show_entry');
function frm_show_entry(){
	global $wpdb, $frmdb, $user_ID;
	$req_id = $_POST['entry_id'];
	$FORM_ID = get_option('oppcs_formidable_pro_form_id');
	$output = "Note not found.";

	$sql = "SELECT `t1`.`id` FROM `{$frmdb->entries}` AS `t1` JOIN `{$frmdb->entry_metas}` AS `t2` ON `t1`.`id`=`t2`.`item_id`
	WHERE `t1`.`id` = $req_id AND  `user_id`=" . get_current_user_id();
	$entry_id = $wpdb->get_col( $sql );
	if( $entry_id && is_array($entry_id) && array_key_exists( 0, $entry_id ) ){
		$output = "<div id='form_edit_area_{$entry_id[0]}'>";
		$output .= FrmEntriesController::show_entry_shortcode( array( 'id' => $entry_id[0], 'include_blank' => 1) );
		$output .= '</div>';
		$editLinkOriginal = FrmProEntriesController::entry_edit_link( array( 'id' => $entry_id[0], 'label' => 'Edit',
				'prefix' => 'form_edit_area_' ));
		$editLinkOPPCS = str_replace( "frmEditEntry", "frmOPPCSEditEntry", $editLinkOriginal );
		$output .= $editLinkOPPCS;
	}
	$outputEscaped = htmlspecialchars($output);
	$response = array(
			'what'=>'entry',
			'action'=>'show',
			'id'=>$req_id,
			'data'=>$outputEscaped
	);
	$xmlResponse = new WP_Ajax_Response($response);
	$xmlResponse->send();
}