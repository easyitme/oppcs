<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 4.7.3
 */

/**
 * Database `jhbqjogx_wp615`
 */

function default_oppcs_notifications() { return array(
  array(	'type' => 'client_new_pending_appointment',
  			'active' => '1',
  			'subject' => 'New appointment request sent to {staff_name} for {appointment_time} '
  								.'on {appointment_date}',
  			'message' => '{company_name}  {company_logo}

&nbsp;
<p style="font: 14px Arial; color: #5f6267;">Hello <b>{client_name}</b>,</p>
<p style="font: 14px Arial; color: #5f6267;">This is a confirmation that you have posted new appointment request for <b>{service_name}</b> with <b>{staff_name}</b> at <b>{appointment_time}</b> on <b>{appointment_date}</b> for <b>{service_price}</b>.</p>
<p style="font: 14px Arial; color: #5f6267;">You will be notified immediately once <b>{staff_name}</b> checks the schedule and accepts your request.</p>
&nbsp;'),

///////////////////////////////////////////////////////////////		
		
  array( 'type' => 'client_appointment_rejected',
  			'active' => '1',
  			'subject' => '{staff_name} has rejected your appointment request '
  								.'for {appointment_time} on {appointment_date}',
  			'message' => '{company_name}  {company_logo}

&nbsp;
<p style="font: 14px Arial; color: #5f6267;">Hello <b>{client_name}</b>,</p>
<p style="font: 14px Arial; color: #5f6267;"><b>{staff_name}</b> has rejected your appointment request for an <b>{service_name}</b> at <b>{appointment_time}</b> on <b>{appointment_date}</b> for <b>{service_price}</b>.</p>
<p style="font: 14px Arial; color: #5f6267;">Please click on the button below to make another booking request.</p>
{make_new_appointment_ribbon}

&nbsp;'),
		
///////////////////////////////////////////////////////////////
		
  array( 'type' => 'client_appointment_accepted',
  			'active' => '1',
  			'subject' => '{staff_name} has accepted your appointment request '
  								.'for {appointment_time} on {appointment_date}',
  			'message' => '{company_name}  {company_logo}

&nbsp;
<p style="font: 14px Arial; color: #5f6267;">Hello <b>{client_name}</b>,</p>
<p style="font: 14px Arial; color: #5f6267;"><b>{staff_name}</b> has accepted your appointment request for an <b>{service_name}</b> at <b>{appointment_time}</b> on <b>{appointment_date}</b> for <b>{service_price}</b>.</p>
<p style="font: 14px Arial; color: #5f6267;">Please click on the button below to open the website to finalize this appointment. Your quick action will be much appreciated by <b>{staff_name}</b>.</p>
{finalize_appointment_ribbon}

&nbsp;'),
		
///////////////////////////////////////////////////////////////
		
  array( 'type' => 'staff_new_pending_appointment',
  			'active' => '1',
  			'subject' => 'New appointment request from {client_name} '
  								.'for {appointment_time} on {appointment_date}',
  			'message' => '{company_name}  {company_logo}

&nbsp;
<p style="font: 14px Arial; color: #5f6267;">Hello <b>{staff_name}</b>,</p>
<p style="font: 14px Arial; color: #5f6267;">You have a new appointment request. Please accept or reject this appointment. Your quick action will be much appreciated by <b>{client_name}</b>.</p>
<p style="font: 14px Arial; color: #5f6267;"><b>{client_name}</b> has requested an <b>{service_name}</b> with you at <b>{appointment_time}</b> on <b>{appointment_date}</b> for <b>{service_price}</b>. <b>{client_name}</b>\'s phone number and email are <b>{client_phone}</b> and <b>{client_email}</b> if you want to get in touch.</p>
<p style="font: 14px Arial; color: #5f6267;">You can use the buttons below to directly act on this appointment or alternatively, click <a href="{pending_staff_appointments_url}">here</a> to open a list of all pending appointments in your browser.</p>
{accept_reject_ribbon}

&nbsp;'),
		
///////////////////////////////////////////////////////////////
		
		array( 'type' => 'participant_please_rate_video',
				'active' => '1',
				'subject' => 'Please rate your video conference experience',
				'message' => '{company_name}  {company_logo}
		
&nbsp;
<p style="font: 14px Arial; color: #5f6267;">Hello <b>{user_name}</b>,</p>
<p style="font: 14px Arial; color: #5f6267;">You have just participated in a video conference on our website.</p>
<p style="font: 14px Arial; color: #5f6267;">In order for us to be able to continously improve our services we would kindly ask you to provide feedback on the technology we use.</p>
<p style="font: 14px Arial; color: #5f6267;">Please click on the button below to tell us how the video technology worked for you and if you have any suggestions for improvement.</p>
{rate_video_ribbon}
		
&nbsp;'),
		
);
}