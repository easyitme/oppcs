<?php
add_shortcode( 'sessions_and_notes', 'sessions_and_notes' );
add_filter('widget_text', 'do_shortcode'); // enable shortcodes in widgets

define('OPPCS_RESOURCE_VERSION', '1.0');

add_action( 'wp_enqueue_scripts', 'oppcs_register_formidable_handlers_script');
function oppcs_register_formidable_handlers_script(){
	wp_register_style('oppcs-helpers', plugins_url( 'oppcs-helpers.css', __FILE__), array(), OPPCS_RESOURCE_VERSION);
	wp_register_script("formidableHandlers", plugins_url( 'formidableHandlers.js', __FILE__),
		array( 'jquery', 'wp-ajax-response' ), OPPCS_RESOURCE_VERSION );
}
function oppcs_enqueue_formidable_handlers_script(){
	wp_enqueue_style("oppcs-helpers");
	wp_enqueue_script("formidableHandlers");
}

function sessions_and_notes( $atts ){
	$page = get_page(NULL);
	if("felhasznalok" != $page->post_name){return;} // only works for user profile pages,
	global $ultimatemember;
	if("posts" != $ultimatemember->profile->active_tab){ return ;} // on the posts subtab
	global $user_ID;
	$requested_user = um_get_requested_user();
	if (!$user_ID || !$requested_user) {return;} // when logged in and looking at a profile
	if("on-line-service-providers" != um_user('role')){return;} // and the logged in user is a service provider

	global $oppcs_provider_client_link_table_name, $wpdb, $frmdb,
		$oppcs_provider_client_link_table_name, $oppcs_appointments_table,
		$oppcs_customer_appointments_table, $oppcs_customers_table,
		$oppcs_staff_table;

	$a = shortcode_atts(array('provider' => 1), $atts);
	$FORM_ID = get_option('oppcs_formidable_pro_form_id');
	$NOTES_ATTACH_INTERVAL =  get_option('oppcs_note_attach_minutes');
	$output = "";
	
	$where = " WHERE `provider_id`=$user_ID AND `client_id`=$requested_user";
	$where2 = " WHERE `worker`=$user_ID AND `user`=$requested_user";
	$res = $wpdb->get_results( "SELECT COUNT(*) AS 'count' FROM `$oppcs_provider_client_link_table_name` $where");
	if($res[0]->count){ // existing provider-client relationship

		// get any previous 'completed' appointments
		//         		$sql = "SELECT `start`, `UID` FROM `{$wpdb->prefix}app_appointments` $where2 ORDER BY `start` DESC";
		$sql_t1 = "
			SELECT IF(`t0`.`start` IS NOT NULL, `t0`.`start`, `t3`.`created_at`) AS `start`, `t0`.`UID`, `t3`.`id` FROM
			(SELECT `start_date` AS `start`,`end_date` AS `end`, `UID` FROM `$oppcs_appointments_table` AS `appointment` 
				JOIN $oppcs_staff_table AS `staff` ON `staff`.`id` = `appointment`.`staff_id`
				JOIN $oppcs_customer_appointments_table AS `customer_appointment`
					ON `customer_appointment`.`appointment_id` = `appointment`.`id`
				JOIN $oppcs_customers_table AS `customer` ON `customer`.`id` = `customer_appointment`.`customer_id`
				WHERE `customer`.`wp_user_id` = $requested_user AND `staff`.`wp_user_id` = $user_ID AND
					`appointment`.`start_date` < NOW()
			 ) AS `t0`";
		$sql_t2 = "
			(SELECT `t1`.`id`, `t1`.`created_at` FROM `{$frmdb->entries}` AS `t1`
			JOIN `{$frmdb->entry_metas}` AS `t2` ON `t1`.`id`=`t2`.`item_id`
			WHERE `user_id`=$user_ID AND `form_id`=$FORM_ID AND `field_id`=151"
			." AND `meta_value` = $requested_user) AS `t3` ";
		$sql_join = "
		ON `t3`.`created_at` BETWEEN
		DATE_SUB(`t0`.`start`, INTERVAL $NOTES_ATTACH_INTERVAL MINUTE)
		AND DATE_ADD(`t0`.`end`, INTERVAL $NOTES_ATTACH_INTERVAL MINUTE)";
		$order_by = " ORDER BY `start` DESC"; 
		$sql = "$sql_t1 RIGHT JOIN $sql_t2 $sql_join UNION $sql_t1 LEFT JOIN $sql_t2 $sql_join $order_by";
// 		echo "<pre>$sql</pre>";
		$res = $wpdb->get_results($sql);
// 		echo "<pre>".print_r($res, true)."</pre>";
		$output .= "<h4 class=\"widgettitle sessions_and_notes\">".__("Past Sessions And Notes","OPPCS")."</h4>";
		$output .= "<table class=\"profile sessions oppcs_sessions_and_notes\"><thead><tr>
			<th>".__("Date & time", "OPPCS")."</th><th>".__("Session ID", "OPPCS")."</th><th>".__("Notes", "OPPCS")."</th></tr></thead>\n";
		$output .= "<tbody>\n";
		$docIcon = "<div class='um-field-label'><div class='um-field-label-icon'><i class='um-icon-document'></i></div></div>";
		if(count($res)){
			oppcs_enqueue_formidable_handlers_script();
			foreach($res as $row){
			$uid = $row->UID ? trim(chunk_split($row->UID, 4, "-"),"-") : "";
					$dt = $row->start ? $row->start : $row->created_at;
					$note = $row->id ?
					"<a href=\"javascript:frmShowEntry({$row->id}, 'form_edit_area_', " . get_the_ID() . ", $FORM_ID, "
					. "'Cancel', '')\" class=\"frm_edit_link\" id=\"frm_edit_{$row->id}\" title=\"View note\""
					. ">$docIcon</a>"
			: "";
				
			//"<a href=\"\"><div class='um-field-label'><div class='um-field-label-icon'><i class='um-icon-document'></i></div></div></a>" : "&nbsp;";
			$output .= "<tr><td>$dt</td><td>$uid</td><td>$note</td></tr>";
			}
		}
		$output .= "</tbody></table>\n";
	}
	return $output ? $output : NULL;
}
