��    #     4  �  L"      �-  %   �-  &   �-     .     .     .     1.  
   H.     S.     o.     |.     �.     �.     �.  d   �.     /     ,/  
   I/     T/     `/     m/  	   /     �/     �/     �/     �/     �/     �/     �/     �/     �/  	   �/     �/     0  
   0     0     "0     .0     ?0     ^0     k0     y0     �0     �0     �0  C   �0     �0     �0     1     1     )1     01     L1     U1     ]1     l1     u1     �1     �1  %   �1  	   �1     �1     �1     �1     �1  	   �1     �1      2     2     2  	   "2     ,2  '   12     Y2     a2     x2     �2     �2     �2     �2     �2     �2  	   �2     �2     �2     �2  �   �2    �3    �4    �5  	   �6  &   �6     7  !   07  !   R7      t7     �7     �7     �7     �7     �7  	   �7  	   �7     �7     8     8  %   8     @8     H8  	   M8     W8     `8     e8     v8     �8     �8     �8     �8     �8  (   �8     9     9     +9  t   89     �9     �9     �9     �9     �9     :  S   (:  M   |:     �:     �:     �:     �:     �:     ;  I   ;     [;     k;  	   p;     z;     �;     �;     �;  �   �;  �   ><  �   =  :   �=  �   #>  �   �>     �?     �?     �?  {   �?  9   .@     h@     o@     ~@     �@     �@     �@     �@     �@     �@     �@     A     A     *A  
   6A     AA     RA  
   qA     |A     �A     �A     �A     �A     �A     �A     �A     �A     �A     �A  ]   �A  _   CB     �B     �B     �B     �B  
   �B  
   �B     �B  $   �B     C     .C     ?C  -   MC  '   {C     �C  +   �C     �C      �C     D     !D     'D  0   8D  2   iD  A   �D  ;   �D  6   E     QE     _E     tE     xE     �E     �E     �E     �E     �E     �E     �E     �E     F     F     F     F     0F     LF     cF  #   iF     �F  �   �F  e   FG     �G  �   �G     �H     �H  '   �H  "   �H  $   I  #   9I     ]I     wI     �I     �I     �I  
   �I     �I     �I  	   �I     �I     �I  	   J     J     J     *J     7J     JJ     aJ     nJ     wJ     �J     �J     �J     �J     �J     �J     �J     �J     �J     K     #K     2K     8K     DK     VK     vK     �K     �K     �K  
   �K     �K  &   �K     �K     �K     �K     L  .   L     ML     [L     wL     �L     �L     �L     �L     �L     �L     �L     �L     �L     �L     M     M     )M     7M     SM  	   nM     xM     ~M     �M  
   �M     �M     �M     �M  G   �M     N     1N     DN  	   IN  
   SN  %   ^N  2   �N  0   �N  '   �N  6   O  M   GO  ,   �O  -   �O  +   �O  
   P  ,   'P     TP     qP  
   vP     �P     �P     �P     �P     �P     �P     �P     �P  7   �P     �P     Q     Q     !Q     8Q     DQ     IQ  '   YQ     �Q     �Q     �Q  	   �Q     �Q     �Q  	   �Q  �   �Q  !   mR  $   �R     �R     �R  .   �R  "   S     0S  T  OS  �  �U  %  �W  �  �Y  e   �]  e   �]  2   N^  N   �^  L   �^  9   _  �   W_  �   `  �   �`    Ca  *   Eb  *   pb  6   �b  5   �b  8   c  &   Ac  $   hc  *   �c  ,   �c  "   �c  <   d  B   Ed  7   �d  >   �d  $   �d  >   $e  (   ce  .   �e  2   �e  &   �e  0   f     Ff  �   �f  �   Lg  y   �g  [   eh  o   �h  )   1i  �   [i  �   �i  �   �j  '   xk  !   �k  '   �k  '   �k  !   l      4l  	   Ul     _l     wl     l     �l     �l     �l     �l     �l     m     m     m     7m     Pm     om     �m     �m     �m     �m  $   �m  =   n     [n     sn     �n     �n     �n     �n  
   �n     �n     �n  v  �n  �   uq     r  �   &t  �   �t    �u  =   �v  )  �v  A   #x  J  ex  �   �z  �   ={  -   �{    |    )}  B   H~  M   �~  S   �~  <   -     j  >   k�  �   ��  �   ��  ;   ~�  �  ��  �   ��     !�     6�  "   K�  2   n�  
   ��  "   ��     χ     ��     �     ��     �     "�     4�     D�     S�     b�     s�  &   ��  '   ��  (   ��  )   	�  !   3�  "   U�     x�  &   ��  '   ��  !   �  "   �  "   *�  -   M�  -   {�  -   ��  -   ׊     �     �      <�     ]�     t�      ��  9   ��  C   �  C   3�  &   w�  '   ��  9   ƌ  B    �  C   C�  9   ��  B   ��  C   �  5   H�  5   ~�     ��     ��     ێ     �     ��     �     ,�     A�     Z�     r�     ��     ��     ��     я     �     ��     �     �     %�     7�  �   J�  )   <�  (   f�     ��     ��     ��      ��     ؑ     �     
�     *�     6�     E�     \�  t   t�     �  "   ��     �     ,�     H�     f�     ��     ��     ��     ��  
   ��     Ɠ  
   ܓ     �     ��     �     (�     ;�     C�     O�     [�  	   d�     n�  :   ��     ��     ɔ     ה     ߔ     �     ��  J   
�     U�     c�     k�  "   ��     ��  $   ��  
   ѕ     ܕ      �  
   �     �     &�     8�  ;   S�     ��     ��     ��     ��     ��     Ȗ     Ԗ     ٖ     �     �     ��  	   �  4   �     J�     R�     j�     r�     ��     ��     ��     ��     ��  	   ʗ     ԗ     ۗ     ߗ    �  C  ��  <  C�  @  ��     ��  ,   ʜ     ��  4   �  4   J�  8   �     ��     ��     ӝ     �      �     �     "�  
   0�  	   ;�     E�  0   U�     ��     ��     ��  
   ��     ��     ��  !   ؞     ��  +   �     <�     K�      Z�  :   {�     ��     ʟ     ޟ  i   �     \�     t�     ��  #   ��  '   ��  $   �  e   
�  b   p�     ӡ     ۡ  
   ��     ��     �     '�  r   >�     ��     Ţ     ͢     ٢     �     ��     �  �   �  �   �  �   �  ;   ɥ  �   �  �   Ϧ     ̧     ߧ     �  �   ��  R   ��     �     ��  #   �     %�     7�     V�  
   c�     n�     ��     ��     ��     թ     �     ��     �  -   �     H�     V�     \�     k�     z�     ��     ��     ��     ��     ��     ��     Ȫ  �   ֪  �   Z�     �     
�     �     #�     +�     >�     Q�  +   U�     ��     ��     ��  ^   ��  H   �     h�  :   ��  "   ��  /   �     �      �     ,�  6   F�  1   }�  =   ��  7   ��  3   %�     Y�     h�     �     ��     ��     ��     ��  &   ¯     �     ��     �  %   �     :�     C�     P�     Y�  &   r�     ��     ��  2   ��  $   �  �   �  �   ñ  $   U�  	  z�  %   ��  $   ��  3   ϳ  #   �  0   '�  6   X�  !   ��     ��  $   ʹ     �     �     �     �     �     #�     2�     G�     Y�     g�     ��     ��     ��     ��     ڵ     �     ��     �     ,�     E�     [�     s�     ��     ��     ��  )   Ͷ     ��     �     �     4�     C�     V�     o�     ~�     ��  
   ��     ��     ��  6   ��     ��  "   �     *�     A�  C   X�     ��  '   ��     ָ     ��     �      �     5�     B�  	   K�     U�     d�     ��     ��  '   ��     ̹     �      �  %   #�     I�     X�  
   f�     q�     ~�     ��     ��     ��  D   ��     ��     �     /�     7�     G�  1   W�  3   ��  *   ��  ,   �  :   �  O   P�  <   ��  =   ݼ  ;   �     W�  8   c�  +   ��     Ƚ     ͽ     ݽ     �     ��     ��     �     �     �     $�  O   +�     {�     ��  	   ��  ,   ��     Ծ     ��     ��  /   �     J�     e�     l�     q�     �     ��     ��  �   ��  2   i�  '   ��     ��     ��  #   ��     �  '   $�  �  L�  '  ��  2  �    5�  }   F�  �   ��  5   G�  (   }�  5   ��  %   ��  �   �  �   ��  L   6�     ��     ��  
   ��     ��     ��     ��     ��     ��     ��     ��     ��     ��     �     &�     =�     U�     ]�     y�     ��     ��     ��     ��  q   ��  z   <�  �   ��  M   F�  :   ��  M   ��  
   �  �   (�  �   ��  �   >�  
   �     )�     /�     8�     G�     P�     l�     ��  
   ��     ��     ��     ��     ��     ��     ��     �  	   �     �     5�     G�     \�     q�     ��     ��     ��  %   ��  <   ��     �     $�     9�     X�     h�     ��     ��     ��     ��  �  ��  ~   [�  '  ��  �   �    ��  �   ��  '   ��    ��     ��  �  ��  |   ��  �   `�     *�  <  7�  8  t�  '   ��  ;   ��  A   �  2   S�  �   ��     ��  �   ��  �   ��     k�  
  ��  �   ��     �     0�      K�  I   l�     ��  )   ��     ��     �     �     1�     A�     P�     b�     u�     ��     ��     ��     ��     ��      �      %�     F�     `�     z�     ��     ��     ��     ��     �  &   �  &   B�  &   i�  &   ��     ��     ��     ��     ��     �     �  >   /�  A   n�  H   ��     ��     �  >   7�  A   v�  H   ��  >   �  A   @�  H   ��  *   ��  *   ��     !�  )   .�     X�     _�     m�     {�     ��     ��     ��     ��     ��     ��     ��     �     )�     >�     U�     X�     p�     ��             Y   �  �   $   �      R      {            ;       0      �      �     U   �  �  O   �  �     �   �      	  Y  �   �   �                  o    r      4   /   d  �      �   t      �  �  C  �   �   �  �       }  �  �      �  �   �                 �   8     M   �   /  �  y       �   t   G      �   &      
                �   �       �         q   I      '  6   �          s       �  �           �    �                     �  &  �     �  �   	   �  �  N   �        *       �       �  �   �  m                  �                     $  �  c  �   �     �      �  B           �  x      �   �   �     �   �        �  
   G   !  7      �  �         �   �   �  �   b   �   e  f   �  �      W  Z   5  k  *    �  <      �       �  ;  �     m       �        2  :     j             E   6      V  �                �       �     \      (  r   �   �  �         w  >   |  �   �   �                  �      S  B                          �  �    �  �          �       �  !        ,                   v   ~          �       '   �   C          �       d   �   �  ,   �       �       �  l   �   �  �  �  z      �               �   �   �      @  1      j        2     �  a   3       K     �     )    8  7   [  4      V     �       �   �   ?     �  �   �           K          R   �       �  _  �       J       |       X  s  h   �    O         �  W     S   �  T   �                    �  �           �     L   �   �  �  �         x   �   �  �   �      �     �     w   (   �   e   f      #   �   �       �   �   �  }          �       b  �       �     �  �  �   �   �  �                  �  `   0   �   �  �  �   z                      �   �  +  D      �        �  �  �   �  �  �   D           �          1   �   �  9      �   _       A    #          �  l  �  �   �  �  �   "   �  �       �  P  >  M          Q  n   "    =  �       %                   %  �  ]       -   N  �          g       ]  �       �           �   L  �            5      �   �  �  �       .             �   �    �  p              \   k   �   �   �   �  #  �       u   �  �      �   n  �       v  �           �         �  �   -           �             �         E  H   �   q       �      �  X   �        9           =   �   �   �  �       �   ~  y  A   P   .          3  �   �   �   �     �  �          @   `  �  �       �  �   ^       �       �   �  J  i  �   h  �   �   �   �  U  �     !        H  
  �       ?  �  u  c   �             �  �  ^  �  :      p   Z  �  �  �   �   a      �  �   F       �  {   �  Q   "  F  )   	  I   [         o   g  �      T      i   <       +   �   �   �       �   "%s" is too long (%d characters max). "%s" is too short (%d characters min). %d h %d min -- Search customers -- -- Select a service -- 2 way sync 2Checkout Standard Checkout API Login ID API Password API Signature API Transaction Key API Username Accept <a href="javascript:void(0)" data-toggle="modal" data-target="#ab-tos">Terms & Conditions</a> Account Number Add Bookly appointments list Add Coupon Add Service Add Services Add Staff Members Add money Address Administrator phone All All Day All Services All day All payment types All providers All services All staff Amount Another code Appearance Apply Appointment Appointment Date Appointment accept/reject page Appointments Are you sure? Avatar Back Booking Time Booking cancellation Bookly: You do not have sufficient permissions to access this page. Business hours Calendar Calendar ID Calendar ID is not valid. Cancel Cancel appointment page URL Capacity Captcha Cart item data Category Change password Checkbox Checkbox Group Click on the underlined text to edit. Client ID Client secret Code Codes Columns Comma (,) Company Company logo Company name Connect Connected Cost Could not save appointment in database. Country Country out of service Coupons Create customer Currency Custom Fields Custom Range Customer Customer Name Customers Date Day Days off Dear [[CLIENT_NAME]].

Thank you for choosing [[COMPANY_NAME]]. We hope you were satisfied with your [[SERVICE_NAME]].

Thank you and we look forward to seeing you again soon.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]]
 Dear [[CLIENT_NAME]].

This is confirmation that you have booked [[SERVICE_NAME]].

We are waiting you at [[COMPANY_ADDRESS]] on [[APPOINTMENT_DATE]] at [[APPOINTMENT_TIME]].

Thank you for choosing our company.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]]
 Dear [[CLIENT_NAME]].

We would like to remind you that you have booked [[SERVICE_NAME]] tomorrow on [[APPOINTMENT_TIME]]. We are waiting you at [[COMPANY_ADDRESS]].

Thank you for choosing our company.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]]
 Dear [[CLIENT_NAME]].
This is confirmation that you have booked [[SERVICE_NAME]].
We are waiting you at [[COMPANY_ADDRESS]] on [[APPOINTMENT_DATE]] at [[APPOINTMENT_TIME]].
Thank you for choosing our company.
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]]
 Deduction Deduction should be a positive number. Default country code Default value for category select Default value for employee select Default value for service select Delete Delete break Delete current photo Delete customers Delete this staff member Delimiter Delivered Details Disabled Discount (%) Discount should be between 0 and 100. Dismiss Done Drop Down Duration Edit Edit appointment Edit booking details Email Notifications Email already in use. Empty password. Enabled End time must not be empty End time must not be equal to start time Enter a URL Enter a label Enter a name Enter a phone number in international format. E.g. for the United States a valid phone number would be +17327572923. Enter a value Enter code from email Error Error adding the break interval Error connecting to server. Error sending email. Evening notification with the next day agenda to staff member (requires cron setup) Evening reminder to customer about next day appointment (requires cron setup) Expired Export to CSV Failed Failed to send SMS. Filter Final step URL Follow-up message in the same day after appointment (requires cron setup) Forgot password From Full name General Google Calendar Google Calendar integration HTML Hello.

An account was created for you at [[SITE_ADDRESS]]

Your user details:
user: [[NEW_USERNAME]]
password: [[NEW_PASSWORD]]

Thanks.
 Hello.

The following booking has been cancelled.

Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]]
 Hello.

You have new booking.

Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]]
 Hello.

Your agenda for tomorrow is:

[[NEXT_DAY_AGENDA]]
 Hello.
An account was created for you at [[SITE_ADDRESS]]
Your user details:
user: [[NEW_USERNAME]]
password: [[NEW_PASSWORD]]

Thanks.
 Hello.
The following booking has been cancelled.
Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]]
 Hide this block Hide this field Holidays If you do not see your country in the list please contact us at <a href="mailto:support@ladela.com">support@ladela.com</a>. If you need to change your name, please contact an admin. Import Incorrect code Incorrect email or password. Incorrect password. Incorrect recovery code. Insert Instructions Invalid date or time Invalid email Invalid email. Invalid number Last 30 Days Last 7 Days Last Month Last appointment Limit number of fetched events Loading... Local Log In Log out Login Message Month Name New Category New Customer New Staff Member New appointment New appointment request from [[CLIENT_NAME]] for [[APPOINTMENT_TIME]] on [[APPOINTMENT_DATE]] New appointment request sent to [[STAFF_NAME]] for [[APPOINTMENT_TIME]] on [[APPOINTMENT_DATE]] New booking information New customer New password Next Next Month Next month No No appointments for selected period. No appointments found No coupons found No customers. No payments for selected period and criteria. No services found. Please add services. No staff selected No time is available for selected criteria. No, delete just customers No, update just here in services Note Notes Notes (optional) Notification settings were updated successfully. Notification to customer about appointment details Notification to customer about their WordPress user login details Notification to staff member about appointment cancellation Notification to staff member about appointment details Notifications Number of times used OFF Old password Option Order Out of credit Padding time (before and after) Page Redirection Participants Password Passwords must be the same. Payment Payments Period Personal Information Phone field default country Phone number is empty. Photo Please accept terms and conditions. Please add your staff members. Please be aware that a value in this field is required in the frontend. If you choose to hide this field, please be sure to select a default value for it Please configure Google Calendar <a href="?page=ab-settings&type=_google_calendar">settings</a> first Please enter old password. Please note, this field has to contain the English version of the name. Also, if you change the name here, you have to change it in the translations file as well, otherwise it will not be properly translated. Please select a customer Please select a service Please select at least one appointment. Please select at least one coupon. Please select at least one customer. Please select at least one service. Please tell us your email Please tell us your name Please tell us your phone Previous month Price Price list Profile Provider Providers Publishable Key Purchase Code Purchases Queued Quick search customer Radio Button Radio Button Group Recovery code expired. Redirect URI Register Registration Remember my choice Remove customer Remove field Remove item Reorder Repeat every year Repeat new password Repeat password Reply directly to customers Required Required field Reset SMS Details SMS Notifications SMS has been sent successfully. Sandbox Mode Save Save Member Schedule Secret Key Secret Word Select a project, or create a new one. Select file Select from WP users Select product Selected / maximum Selected period doesn't match service duration Semicolon (;) Send copy to administrators Send email notifications Send emails as Send test SMS Sender email Sender name Sending Sent Service Service paid locally Services Settings Settings saved. Show blocked timeslots Show calendar Show each day in one column Show form progress tracker Signed up Staff Staff Member Staff Members Start Over Start time must not be empty Status Subject Synchronize the data of the staff member bookings with Google Calendar. Template for event title Terms & Conditions Text Text Area Text Field Thank you for purchasing our product. The client ID obtained from the Developers Console The number of customers should be not more than  The requested interval is not available The selected period is occupied by another appointment The selected time is not available anymore. Please, choose another time slot. The start time must be less than the end one The start time must be less than the end time The value is taken from client’s browser. This Month This coupon code is invalid or has been used This email is already in use Time Time range Time slot length Title Titles To Today Total Total appointments Type URL for cancel appointment link (to use inside <a> tag) Uncategorized Undelivered Untitled Update service setting Usage limit User User not found. Visible to non-logged in customers only We are not working on this day Website Week Week days Welcome to Bookly! Yes Yesterday You are about to change a service setting which is also configured separately for each staff member. Do you want to update it in staff settings too? Your agenda for [[TOMORROW_DATE]] Your appointment at [[COMPANY_NAME]] Your appointment information Your balance Your payment has been accepted for processing. Your payment has been interrupted. Your visit to [[COMPANY_NAME]] [[COMPANY_NAME]]  [[COMPANY_LOGO]]

&nbsp;
<p style="font: 14px Arial; color: #5f6267;">Hello <b>[[CLIENT_NAME]]</b>,</p>
<p style="font: 14px Arial; color: #5f6267;"><b>[[STAFF_NAME]]</b> has accepted your appointment request for an <b>[[SERVICE_NAME]]</b> at <b>[[APPOINTMENT_TIME]]</b> on <b>[[APPOINTMENT_DATE]]</b> for <b>[[SERVICE_PRICE]]</b>.</p>
<p style="font: 14px Arial; color: #5f6267;">Please click on the button below to open the website to finalize this appointment. Your quick action will be much appreciated by <b>[[STAFF_NAME]]</b>.</p>
[[FINALIZE_APPOINTMENT_RIBBON]]

&nbsp;
 [[COMPANY_NAME]]  [[COMPANY_LOGO]]

&nbsp;
<p style="font: 14px Arial; color: #5f6267;">Hello <b>[[CLIENT_NAME]]</b>,</p>
<p style="font: 14px Arial; color: #5f6267;"><b>[[STAFF_NAME]]</b> has rejected your appointment request for an <b>[[SERVICE_NAME]]</b> at <b>[[APPOINTMENT_TIME]]</b> on <b>[[APPOINTMENT_DATE]]</b> for <b>[[SERVICE_PRICE]]</b>.</p>
<p style="font: 14px Arial; color: #5f6267;">Please click on the button below to make another booking request.</p>
[[MAKE_NEW_APPOINTMENT_RIBBON]]

&nbsp;
 [[COMPANY_NAME]]  [[COMPANY_LOGO]]

&nbsp;
<p style="font: 14px Arial; color: #5f6267;">Hello <b>[[CLIENT_NAME]]</b>,</p>
<p style="font: 14px Arial; color: #5f6267;">This is a confirmation that you have posted new appointment request for <b>[[SERVICE_NAME]]</b> with <b>[[STAFF_NAME]]</b> at <b>[[APPOINTMENT_TIME]]</b> on <b>[[APPOINTMENT_DATE]]</b> for <b>[[SERVICE_PRICE]]</b>.</p>
<p style="font: 14px Arial; color: #5f6267;">You will be notified immediately once <b>[[STAFF_NAME]]</b> checks the schedule and accepts your request.</p>
&nbsp;
 [[COMPANY_NAME]]  [[COMPANY_LOGO]]

&nbsp;
<p style="font: 14px Arial; color: #5f6267;">Hello <b>[[STAFF_NAME]]</b>,</p>
<p style="font: 14px Arial; color: #5f6267;">You have a new appointment request. Please accept or reject this appointment. Your quick action will be much appreciated by <b>[[CLIENT_NAME]]</b>.</p>
<p style="font: 14px Arial; color: #5f6267;"><b>[[CLIENT_NAME]]</b> has requested an <b>[[SERVICE_NAME]]</b> with you at <b>[[APPOINTMENT_TIME]]</b> on <b>[[APPOINTMENT_DATE]]</b> for <b>[[SERVICE_PRICE]]</b>. <b>[[CLIENT_NAME]]</b>'s phone number and email are <b>[[CLIENT_PHONE]]</b> and <b>[[CLIENT_EMAIL]]</b> if you want to get in touch.</p>
<p style="font: 14px Arial; color: #5f6267;">You can use the buttons below to directly act on this appointment or alternatively, click <a href="[[PENDING_STAFF_APPOINTMENTS_URL]]">here</a> to open a list of all pending appointments in your browser.</p>
[[ACCEPT_REJECT_RIBBON]]

&nbsp;
 [[STAFF_NAME]] has accepted your appointment request for [[APPOINTMENT_TIME]] on [[APPOINTMENT_DATE]] [[STAFF_NAME]] has rejected your appointment request for [[APPOINTMENT_TIME]] on [[APPOINTMENT_DATE]] a reject and accept button organized into a ribbon ab_appearance_text_info_couponThe price for the service is [[SERVICE_PRICE]]. ab_appearance_text_info_fifth_stepPlease tell us how you would like to pay: ab_appearance_text_info_first_stepPlease select service: ab_appearance_text_info_fourth_stepYou selected a booking for [[SERVICE_NAME]] by [[STAFF_NAME]] at [[SERVICE_TIME]] on [[SERVICE_DATE]]. The price for the service is [[SERVICE_PRICE]]. ab_appearance_text_info_second_stepBelow you can find a list of available time slots for [[SERVICE_NAME]] by [[STAFF_NAME]].
Click on a time slot to proceed with booking.
 ab_appearance_text_info_sixth_stepThank you! Your booking is complete. An email with details of your booking has been sent to you. ab_appearance_text_info_third_stepYou selected a booking for [[SERVICE_NAME]] by [[STAFF_NAME]] at [[SERVICE_TIME]] on [[SERVICE_DATE]]. The price for the service is [[SERVICE_PRICE]].
Please provide your details in the form below to proceed with booking.
 ab_appearance_text_info_third_step_guest  ab_appearance_text_label_categoryCategory ab_appearance_text_label_ccard_codeCard Security Code ab_appearance_text_label_ccard_expireExpiration Date ab_appearance_text_label_ccard_numberCredit Card Number ab_appearance_text_label_couponCoupon ab_appearance_text_label_emailEmail ab_appearance_text_label_employeeEmployee ab_appearance_text_label_finish_byFinish by ab_appearance_text_label_nameName ab_appearance_text_label_number_of_personsNumber of persons ab_appearance_text_label_pay_ccardI will pay now with Credit Card ab_appearance_text_label_pay_locallyI will pay on site ab_appearance_text_label_pay_paypalI will pay now with PayPal ab_appearance_text_label_phonePhone ab_appearance_text_label_select_dateI'm available on or after ab_appearance_text_label_serviceService ab_appearance_text_label_start_fromStart from ab_appearance_text_option_categorySelect category ab_appearance_text_option_employeeAny ab_appearance_text_option_serviceSelect service ab_appearance_text_staff_accept_acceptedYou have accepted this appointment. [[CLIENT_NAME]] has been notified to post payment. ab_appearance_text_staff_accept_bookedYou had accepted this appointment and [[CLIENT_NAME]] has paid for it. This is a live booking. ab_appearance_text_staff_accept_info[[CLIENT_NAME]] has requested an [[SERVICE_NAME]] with you at [[SERVICE_TIME]] on [[SERVICE_DATE]] for [[SERVICE_PRICE]]. ab_appearance_text_staff_accept_not_foundThis appointment is not found. The client might have cancelled in the meantime. ab_appearance_text_staff_accept_pendingPlease approve or decline the pending request here. ab_appearance_text_staff_accept_rejectedYou have rejected this appointment. [[CLIENT_NAME]] has been notified. ab_appearance_text_step_acceptAcceptance ab_appearance_text_step_accept_accepted[[STAFF_NAME]] has accepted your appointment request. Please click on the "Next" button to finalize the appointment. ab_appearance_text_step_accept_pendingThanks for choosing the appointment. We sent your request to [[STAFF_NAME]] for approval. You will get notified of the approval in an email. ab_appearance_text_step_accept_rejected[[STAFF_NAME]] has rejected your appointment request for [[SERVICE_TIME]] on [[SERVICE_DATE]]. Please click on the "Start Over" button to book another appointment. ab_appearance_text_step_detailsDetails ab_appearance_text_step_doneDone ab_appearance_text_step_paymentPayment ab_appearance_text_step_serviceService ab_appearance_text_step_timeTime accept appointment link (button) add break address of your company breaks: buttonAccept buttonBack buttonCancel Appointment buttonFinalize Appointment buttonMake New Appointment buttonManage Appointment buttonNext buttonReject cancel appointment link category_0Uncategorized category_1Online Consultation category_1Online consultation category_2Diagnostic interview category_3Therapy category_4Supervision category_5Liaison consultation combined values of all custom fields combined values of all custom fields (formatted in 2 columns) custom_field_nameNotes customer new password customer new username date of appointment date of next day date of service disconnect email of client email of staff email_client_appointment_accepted[[COMPANY_NAME]]  [[COMPANY_LOGO]]

&nbsp;
<p style="font: 14px Arial; color: #5f6267;">Hello <b>[[CLIENT_NAME]]</b>,</p>
<p style="font: 14px Arial; color: #5f6267;"><b>[[STAFF_NAME]]</b> has accepted your appointment request for an <b>[[SERVICE_NAME]]</b> at <b>[[APPOINTMENT_TIME]]</b> on <b>[[APPOINTMENT_DATE]]</b> for <b>[[SERVICE_PRICE]]</b>.</p>
<p style="font: 14px Arial; color: #5f6267;">Please click on the button below to open the website to finalize this appointment. Your quick action will be much appreciated by <b>[[STAFF_NAME]]</b>.</p>
[[FINALIZE_APPOINTMENT_RIBBON]]

&nbsp;
 email_client_appointment_accepted_subject[[STAFF_NAME]] has accepted your appointment request for [[APPOINTMENT_TIME]] on [[APPOINTMENT_DATE]] email_client_appointment_rejected[[COMPANY_NAME]]  [[COMPANY_LOGO]]

&nbsp;
<p style="font: 14px Arial; color: #5f6267;">Hello <b>[[CLIENT_NAME]]</b>,</p>
<p style="font: 14px Arial; color: #5f6267;"><b>[[STAFF_NAME]]</b> has rejected your appointment request for an <b>[[SERVICE_NAME]]</b> at <b>[[APPOINTMENT_TIME]]</b> on <b>[[APPOINTMENT_DATE]]</b> for <b>[[SERVICE_PRICE]]</b>.</p>
<p style="font: 14px Arial; color: #5f6267;">Please click on the button below to make another booking request.</p>
[[MAKE_NEW_APPOINTMENT_RIBBON]]

&nbsp;
 email_client_appointment_rejected_subject[[STAFF_NAME]] has rejected your appointment request for [[APPOINTMENT_TIME]] on [[APPOINTMENT_DATE]] email_client_follow_upDear [[CLIENT_NAME]].

Thank you for choosing [[COMPANY_NAME]]. We hope you were satisfied with your [[SERVICE_NAME]].

Thank you and we look forward to seeing you again soon.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]]
 email_client_follow_up_subjectDear [[CLIENT_NAME]].
Thank you for choosing [[COMPANY_NAME]]. We hope you were satisfied with your [[SERVICE_NAME]].
Thank you and we look forward to seeing you again soon.
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]]
 email_client_follow_up_subjectYour visit to [[COMPANY_NAME]] email_client_new_appointmentDear [[CLIENT_NAME]].

This is confirmation that you have booked [[SERVICE_NAME]].

We are waiting you at [[COMPANY_ADDRESS]] on [[APPOINTMENT_DATE]] at [[APPOINTMENT_TIME]].

Thank you for choosing our company.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]]
 email_client_new_appointment_subjectYour appointment information email_client_new_pending_appointment[[COMPANY_NAME]]  [[COMPANY_LOGO]]

&nbsp;
<p style="font: 14px Arial; color: #5f6267;">Hello <b>[[CLIENT_NAME]]</b>,</p>
<p style="font: 14px Arial; color: #5f6267;">This is a confirmation that you have posted new appointment request for <b>[[SERVICE_NAME]]</b> with <b>[[STAFF_NAME]]</b> at <b>[[APPOINTMENT_TIME]]</b> on <b>[[APPOINTMENT_DATE]]</b> for <b>[[SERVICE_PRICE]]</b>.</p>
<p style="font: 14px Arial; color: #5f6267;">You will be notified immediately once <b>[[STAFF_NAME]]</b> checks the schedule and accepts your request.</p>
&nbsp;
 email_client_new_pending_appointment_subjectNew appointment request sent to [[STAFF_NAME]] for [[APPOINTMENT_TIME]] on [[APPOINTMENT_DATE]] email_client_new_wp_userHello.

An account was created for you at [[SITE_ADDRESS]]

Your user details:
user: [[NEW_USERNAME]]
password: [[NEW_PASSWORD]]

Thanks.
 email_client_new_wp_user_subjectNew customer email_client_reminderDear [[CLIENT_NAME]].

We would like to remind you that you have booked [[SERVICE_NAME]] tomorrow on [[APPOINTMENT_TIME]]. We are waiting you at [[COMPANY_ADDRESS]].

Thank you for choosing our company.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]]
 email_client_reminder_subjectDear [[CLIENT_NAME]].
We would like to remind you that you have booked [[SERVICE_NAME]] tomorrow on [[APPOINTMENT_TIME]]. We are waiting you at [[COMPANY_ADDRESS]].
Thank you for choosing our company.
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]]
 email_client_reminder_subjectYour appointment at [[COMPANY_NAME]] email_staff_agendaHello.

Your agenda for tomorrow is:

[[NEXT_DAY_AGENDA]]
 email_staff_agenda_subjectHello.
Your agenda for tomorrow is:
[[NEXT_DAY_AGENDA]]
 email_staff_agenda_subjectYour agenda for [[TOMORROW_DATE]] email_staff_cancelled_appointmentHello.

The following booking has been cancelled.

Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]]
 email_staff_cancelled_appointment_subjectBooking cancellation email_staff_new_appointmentHello.

You have new booking.

Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]]
 email_staff_new_appointment_subjectHello.
You have new booking.
Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]]
 email_staff_new_appointment_subjectNew booking information email_staff_new_pending_appointment[[COMPANY_NAME]]  [[COMPANY_LOGO]]

&nbsp;
<p style="font: 14px Arial; color: #5f6267;">Hello <b>[[STAFF_NAME]]</b>,</p>
<p style="font: 14px Arial; color: #5f6267;">You have a new appointment request. Please accept or reject this appointment. Your quick action will be much appreciated by <b>[[CLIENT_NAME]]</b>.</p>
<p style="font: 14px Arial; color: #5f6267;"><b>[[CLIENT_NAME]]</b> has requested an <b>[[SERVICE_NAME]]</b> with you at <b>[[APPOINTMENT_TIME]]</b> on <b>[[APPOINTMENT_DATE]]</b> for <b>[[SERVICE_PRICE]]</b>. <b>[[CLIENT_NAME]]</b>'s phone number and email are <b>[[CLIENT_PHONE]]</b> and <b>[[CLIENT_EMAIL]]</b> if you want to get in touch.</p>
<p style="font: 14px Arial; color: #5f6267;">You can use the buttons below to directly act on this appointment or alternatively, click <a href="[[PENDING_STAFF_APPOINTMENTS_URL]]">here</a> to open a list of all pending appointments in your browser.</p>
[[ACCEPT_REJECT_RIBBON]]

&nbsp;
 email_staff_new_pending_appointment_subjectNew appointment request from [[CLIENT_NAME]] for [[APPOINTMENT_TIME]] on [[APPOINTMENT_DATE]] errorForm ID error. errorSession error. finalize appointment link (button) finalize appointment link in a ribbon for the lazy login form make new appointment link (button) name of category name of client name of service name of staff name of your company number of persons phone of client phone of staff photo of staff price of service service_1Online consultation service_1Online consultation (15 min) service_1Online consultation (15 mins) service_18Diagnostic interview (60 min) service_18Diagnostic interview (60 mins) service_19Psychotherapy (30 min) service_19Psychotherapy (30 mins) service_2Online consultation service_2Online consultation (60 min) service_2Online consultation (60 mins) service_20Psychotherapy (60 min) service_20Psychotherapy (60 mins) service_21Psychotherapy (45 mins) service_23Psychiatric consultation (15 mins) service_24Psychiatric consultation (30 mins) service_25Psychiatric consultation (45 mins) service_26Psychiatric consultation (60 mins) service_27Supervision service_27Supervision (30 min) service_27Supervision (30 mins) service_28Supervision service_28Supervision (60 min) service_28Supervision (60 mins) service_29Liaison consultation for health care providers service_29Liaison consultation for health care providers  (30 min) service_29Liaison consultation for health care providers (30 mins) service_3Online consultation (30 min) service_3Online consultation (30 mins) service_30Liaison consultation for health care providers service_30Liaison consultation for health care providers (15 min) service_30Liaison consultation for health care providers (15 mins) service_31Liaison consultation for health care providers service_31Liaison consultation for health care providers (60 min) service_31Liaison consultation for health care providers (60 mins) service_33First interview for psychotherapy (60 min) service_34First interview for psychotherapy (90 min) site address staff agenda for next day staff_0Any staff_1Ben Dr. Sobel staff_10Csukly Gábor staff_11Anna Angyalosi staff_2John Butcher staff_3Patricia Polgár staff_3patricia polgar staff_5viktoria simon staff_6Viktória Dr.Simon staff_8Mark Dr. Szoldan staff_9Lajos Simon this web-site address time of appointment time of service to total price of booking your company logo your company phone Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Project-Id-Version: WPML_EXPORT_bookly
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
Language: hu
MIME-Version: 1.0
X-Generator: Poedit 1.8.6
 "%s" túl hosszú (% d karakter maximum). "%s" túl rövid (% d karakter minimum). %d ó %d perc -- Ügyfelek keresése -- -- Válasszon szolgáltatást -- Kétirányú szinkron 2Checkout Standard Checkout API bejelentkezési azonosító API jelszó API aláírás API tranzakciós kulcs API felhasználói név Elfogadom a <a href="javascript:void(0)" data-toggle="modal" data-target="#ab-tos">Felhasználási Feltételeket</a> Számlaszám Bookly időpont lista hozzáadása Kupon hozzáadása Szolgáltatás hozzáadása Szolgáltatások hozzáadása Munkatársak hozzáadása Pénz hozzáadása Cím Adminisztrátor telefonja Mindenki Egész nap Minden szolgáltatás Egész nap Minden fizetési mód Minden szolgáltató Minden szolgáltatás Összes munkatárs Összeg Másik kód Megjelenés Alkalmaz Foglalás A foglalás dátuma Időpont elfogadásra vagy elutasításra szolgáló oldal Foglalások Biztos benne? Avatár Vissza Foglalási idő Foglalás törlése Bookly: Nem rendelkezik megfelelő jogosultsággal az oldal eléréséhez. Nyitvatartás Naptár Naptár azonosítója Naptár azonosító érvénytelen. Mégsem Foglalás törlés oldalának URL-je Kapacitás Captcha Bevásárlókosár tétel adatai Kategória Jelszó módosítása Jelölő négyzet Jelölőnégyzetet csoport Kattintson az aláhúzott szövegre annak szerkesztéséhez Ügyfél azonosító Ügyfél titok Kód Kódok Oszlopok Vessző (,) Cég Cég logója Cégnév Csatlakozás Csatlakoztatva Költség Foglalását nem sikerült menteni az adatbázisban. Ország Ország üzemen kívül Kuponok Ügyfél létrehozása Pénznem Egyedi mezők Egyéni tartomány Ügyfél Ügyfél neve Ügyfelek Dátum Nap Szabadnapok Kedves [[CLIENT_NAME]]!

Köszönjük, hogy minket választott. Reméljük, hogy elégedett volt a [[SERVICE_NAME]] szolgáltatással.

Köszönjük, és reméljük hamarosan újra igénybe veszi a szolgáltatásunkat!

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]]
 Kedves [[CLIENT_NAME]]!

Ez az email azt igazolja, hogy van egy foglalása erre a szolgáltatásra:[[SERVICE_NAME]].

Várjuk Önt nálunk: [[COMPANY_ADDRESS]] a [[APPOINTMENT_DATE]] napon, [[APPOINTMENT_TIME]] időben.

Köszönjük, hogy cégünket választotta. 
[[COMPANY_NAME]]
 [[COMPANY_PHONE]]
 [[COMPANY_WEBSITE]]
 Kedves [[CLIENT_NAME]].

Szeretnénk emlékeztetni a foglalására, mely a következő szolgáltatást tartalmazza: [[SERVICE_NAME]] a holnapi napon ekkor: [[APPOINTMENT_TIME]]. Várjuk Önt [[COMPANY_ADDRESS]].

Köszönjük, hogy cégünket választotta. 

[[COMPANY_NAME]] 
[[COMPANY_PHONE]] 
[[COMPANY_WEBSITE]]
 Kedves [[CLIENT_NAME]]!
Ez az email azt igazolja, hogy van egy foglalása erre a szolgáltatásra:[[SERVICE_NAME]].
Várjuk Önt nálunk: [[COMPANY_ADDRESS]] a [[APPOINTMENT_DATE]] napon, [[APPOINTMENT_TIME]] időben.
Köszönjük, hogy cégünket választotta. 
[[COMPANY_NAME]]
 [[COMPANY_PHONE]]
 [[COMPANY_WEBSITE]]
 Levonás A levonásnak pozitív számnak kell lennie! Alapértelmezett ország kód Alapértelmezett érték a kategória választáshoz Alapértelmezett érték a munkatárs választáshoz Alapértelmezett érték a szolgáltatás választáshoz Törlés Szünet törlése Jelenlegi fotó törlése Ügyfél törlése Munkatárs törlése Elválasztó Kézbesített Részletek Letiltott Kedvezmény (%) A kedvezménynek 0 és 100 között kell lennie. Elvetés Kész Legördülő menü Időtartam Szerkesztés Foglalás szerkesztése Foglalás adatainak szerkesztése E-mail értesítések Ezzel az e-mail címmel már regisztráltak Üres jelszó. Engedélyezett Befejezés ideje nem lehet üres Befejezés ideje nem lehet egyenlő a kezdési időponttal Adjon meg egy URL-t Adja meg a címkét Adjon meg egy nevet Írja be a telefonszámot nemzetközi formátumban. Pl. Magyarországhoz a helyes formátum: +36205557777 Adjon meg egy értéket Írja be a kódot az e-mailből Hiba Hiba a szünet hozzáadása közben Nem sikerült csatlakozni a szerverhez. Az e-mail elküldése nem sikerült. Esti emlékeztető a munkatársnak a következő napi foglalásokról (cron beállítás szükséges) Esti emlékeztető az ügyfélnek a következő napi foglalásról (cron beállítás szükséges) Lejárt Exportálás CSV-fájlba Sikertelen Nem sikerült az SMS küldése. Szűrő Utolsó lépés URL-je Azonos napi nyomonkövető üzenet az ügyfélnek, a létrejött találkozó után (cron beállítás szükséges) Elfelejtett jelszó Ettől: Teljes név Általános Google Naptár Google Naptár integrációja HTML Üdvözöljük!
A [[SITE_ADDRESS]] honlapon egy felhasználói fiókot hoztunk létre önnek.
Az ön felhasználói adatai:
Felhasználó név: [[NEW_USERNAME]]
Jelszó: [[NEW_PASSWORD]]
Köszönjük.
 Üdvözöljük!

A következő foglalás törölve lett.

Szolgáltatás: [[SERVICE_NAME]] 
Dátum: [[APPOINTMENT_DATE]]
Idő: [[APPOINTMENT_TIME]] 
Ügyfél neve: [[CLIENT_NAME]]
Ügyfél telefonja: [[CLIENT_PHONE]] 
Ügyfél e-mailje: [[CLIENT_EMAIL]]
 Üdvözöljük!
Új foglalása van.
Szolgáltatás: [[SERVICE_NAME]] 
Dátum: [[APPOINTMENT_DATE]]
Idő: [[APPOINTMENT_TIME]] 
Ügyfél neve: [[CLIENT_NAME]]
Ügyfél telefonja: [[CLIENT_PHONE]] 
Ügyfél e-mailje: [[CLIENT_EMAIL]]
 Üdvözlöm.

A holnapi menetrendje: 

[[NEXT_DAY_AGENDA]]
 Üdvözöljük!
A [[SITE_ADDRESS]] honlapon egy felhasználói fiókot hoztunk létre önnek.
Az ön felhasználói adatai:
Felhasználó név: [[NEW_USERNAME]]
Jelszó: [[NEW_PASSWORD]]
Köszönjük.
 Üdvözöljük!
A következő foglalás törölve lett.
Szolgáltatás: [[SERVICE_NAME]] 
Dátum: [[APPOINTMENT_DATE]]
Idő: [[APPOINTMENT_TIME]] 
Ügyfél neve: [[CLIENT_NAME]]
Ügyfél telefonja: [[CLIENT_PHONE]] 
Ügyfél e-mailje: [[CLIENT_EMAIL]]
 E blokk elrejtése Mező elrejtése Ünnepnapok Ha nem látja az országát a listában, kérjük lépjen kapcsolatba velünk ezen a címen: <a href="mailto:support@ladela.com">support@ladela.com</a>. Kérjük, ha szeretné megváltoztatni a nevét, kérjen meg egy adminisztrátort. Importálás Hibás kód Helytelen e-mail cím vagy jelszó. A jelszó hibás. Hibás helyreállítási kód. Beillesztés Útmutató Érvénytelen dátum vagy idő Érvénytelen e-mail cím Érvénytelen e-mail cím Érvénytelen szám Legutóbbi 30 nap Legutóbbi 7 nap Múlt hónap Legutóbbi foglalás Lehívott események számának korlátozása Betöltés... Helyi Bejelentkezés Kijelentkezés Bejelentkezés Üzenet Hónap Név Új kategória Új Ügyfél Új munkatárs Új foglalás Új találkozót kérés [[CLIENT_NAME]] ügyféltől, erre az időpontra: [[APPOINTMENT_TIME]], ezen a napon: [[APPOINTMENT_DATE]] Új találkozó kérés elküldve [[STAFF_NAME]] munkatárs részére, erre az időpontra: [[APPOINTMENT_TIME]], ezen a napon: [[APPOINTMENT_DATE]] Új foglalás információk Új ügyfél Új jelszó Tovább Következő hónap Következő hónap Nem Nincs foglalás a választott periódusban. Foglalás nem található Nem található kupon Nincs ügyfél Nincs a kiválasztott időintervallumnak és kirtériumoknak megfelelő fizetési tranzakció. Nincs szolgáltatás még felvéve. Kérjük vegyen fel szolgáltatást. Nincs munkatárs kiválasztva Nincs a kiválasztott kritériumoknak megfelelő időpont. Nem, csak az ügyfeleket törölje Nem, csak itt a szolgáltatásoknál frissítse Megjegyzés Megjegyzés Megjegyzés (opcionális) Az értesítési beállítások sikeresen módosítva. Ügyfél értesítése a foglalás részleteiről Ügyfél értesítése a WordPress bejelentkezési adatairól Munkatárs értesítése a foglalás visszavonásáról Munkatárs értesítése a foglalás részleteiről Értesítések Előfordulások száma KI Régi jelszó Opció Megrendelés Nem áll rendelkezésre keret Biztosíték idő (előtte és utána) Oldal átirányítás Résztvevők Jelszó Jelszavaknak azonosnak kell lenniük. Fizetés Befizetések Időszak Személyes információk Telefon mező alapértelmezett ország Telefonszám üres. Fotó Kérjük, fogadja el a szerződési feltételeket. Kérjük, adja meg a munkatársakat. Kérjük, vegye figyelembe, hogy a felhasználói felületen e mezőbe érték kell kerüljön. Ha úgy dönt, hogy elrejti a mezőt, kérjük adjon meg egy alapértelmezett értéket Kérjük először a következő Google Naptár konfigurációt álltisa be: <a href="?page=ab-settings&type=_google_calendar">Beállítások</a> Kérjük, írja be a régi jelszót. Kérjük, figyeljen oda, hogy ennek a mezőnek a név angol változatát kell tartalmaznia. Továbbá, ha megváltoztatja a nevet, akkor ezt a változtatást a fordításokat tartalmazó fájlban is át kell vezetnie, különben a név nem lesz többé lefordítva. Kérjük, válasszon ki egy ügyfelet Kérjük, válasszon szolgáltatást Kérjük, válasszon ki legalább egy találkozót. Jelöljön ki legalább egy kupont! Kérjük, válasszon ki legalább egy ügyfelet. Kérjük, válasszon ki legalább egy szolgáltatást! Kérjük, adja meg e-mail címét Kérjük, adja meg a nevét Kérjük, adja meg a telefonszámát Előző hónap Ár Árlista Profil Szolgáltató Szolgáltatók Közzétehető kulcs Vásárlási kód Vásárlások Várólistára állítva Ügyfél gyorskeresés Rádiógomb Választógomb csoport Helyreállítási kód lejárt. Átirányítási URI Regisztrálás Regisztráció Jegyezze meg a választásomat Ügyfél eltávolítása Mező eltávolítása Az elem eltávolítása Újrasorrendezés Ismétlés minden évben Új jelszó megismétlése Jelszó ismétlése Közvetlenül az ügyfélnek válaszoljon Szükséges Kötelező mező Alapértékek visszaállítása SMS részletek SMS értesítések SMS sikeresen elküldve. Homokozó Mód Mentés Munkatárs mentése Ütemezés Titkos kulcs Titkos szó Válasszon ki egy projektet, vagy hozzon létre újat! Válasszon fájlt Válasszon a WP felhasználókból Termék kiválasztása Kijelölt / maximális A kiválasztott időszak nem egyezik szolgáltatás időtartamával Pontosvessző (;) Másolat küldése a rendszergazdáknak E-mail értesítések küldése E-mail küldése mint Teszt SMS küldése Küldő e-mail címe Küldő neve Küldés Elküldve Szolgáltatás Szolgáltatást helyben fizetni Szolgáltatások Beállítások A beállítások elmentésre kerültek. Blokkolt időpontok mutatása Naptár megjelenítése Teljes nap egy oszlopban mutatva Űrlap folyamatjelző megjelenítése Iratkozott fel Szolgáltató Munkatárs Munkatársak Újrakezdés Kezdés ideje nem lehet üres Állapot Tárgy Munkatársa foglalás adatainak szinkronizálása Google Naptárral. Esemény neve sablon Felhasználási feltételek Szöveg Szövegterület Szöveges mező Köszönjük, hogy termékünket vásárolta meg! A Fejlesztői Konzolról nyert ügyfél azonosító Az ügyfelek száma nem lehet több, mint  A választott időintervallum nem elérhető A kiválasztott időszakban már van egy másik foglalás. A kiválaszott időpont már nem elérhető. Kérjük, válasszon egy másikat! A kezdési időpontnak hamarabb kell lenni a befejezésinél A kezdési időnek előbb kell lennie a befejezési időnél! Ezt az érdéket a felhasználó böngészőjéből vettük Ez a hónap Ez a kupon kód érvénytelen, vagy már felhasználták Ezzel az e-mail címmel már regisztráltak Idő Idő tartomány Időegység hossza Cím Címek Eddig: Ma Végösszeg Összes foglalás Típus URL a foglalás törlésének linkjéhez (az <a> tag-en belüli használathoz). Nem kategorizált Kézbesítetlen Névtelen Szolgáltatás beállításának frissitése Használat korlátozása Felhasználó Felhasználó nem található. Csak bejelentkezett felhasználóknak látható Nem dolgozunk ezen a napon Honlap Hét A hét napjai Üdvözli a Bookly! Igen Tegnap Olyan szolgáltatási beállítást készül módosítani, mely minden munkatársnál külön is állítható. Szeretné, hogy a munkatársak beállításainál is ez a módosítás legyen érvényben? Az Ön menetrendje ezen a napon: [[TOMORROW_DATE]] Az Ön foglalása itt: [[COMPANY_NAME]] Foglalásának adatai Az Ön egyenlege Fizetése feldolgozásra elfogadva. Fizetés megszakadt. Az Ön [[COMPANY_NAME]] konzultációja <p style="font: 14px Arial; color: #5f6267;">Kedves <b>[[CLIENT_NAME]]</b>,</p>
<p style="font: 14px Arial; color: #5f6267;"><b>[[STAFF_NAME]]</b> elfogadta a foglalás kérését, aminek a részletei ezek: Szolgáltatás: <b>[[SERVICE_NAME]]</b>, ekkor: <b>[[APPOINTMENT_TIME]]</b>, ezen a napon: <b>[[APPOINTMENT_DATE]]</b>, ezen az áron: <b>[[SERVICE_PRICE]]</b>.</p>
<p style="font: 14px Arial; color: #5f6267;">Kérjük kattintson a lenti gombra amely elviszi Önt honlapunkra, ahol fizetéssel véglegesítheti a foglalását. Mihamarabbi véglegesítését <b>[[STAFF_NAME]]</b> előre is köszöni.
</p>
[[FINALIZE_APPOINTMENT_RIBBON]]

&nbsp;
 <p style="font: 14px Arial; color: #5f6267;">Kedves <b>[[CLIENT_NAME]]</b>,</p>
<p style="font: 14px Arial; color: #5f6267;"><b>[[STAFF_NAME]]</b> elutasította a foglalás kérését, aminek a részletei ezek voltak: Szolgáltatás: <b>[[SERVICE_NAME]]</b>, ekkor: <b>[[APPOINTMENT_TIME]]</b>, ezen a napon: <b>[[APPOINTMENT_DATE]]</b>, ezen az áron: <b>[[SERVICE_PRICE]]</b>.</p>
<p style="font: 14px Arial; color: #5f6267;">Kérjük kattintson a lenti gombra egy új foglalás kérés elindításához.</p>
[[MAKE_NEW_APPOINTMENT_RIBBON]]

&nbsp;
 [[COMPANY_NAME]]  [[COMPANY_LOGO]]

&nbsp;
<p style="font: 14px Arial; color: #5f6267;">Ez egy visszaigazolás, hogy foglalás kérést küldött a következő szolgáltatásra: <b>[[SERVICE_NAME]]</b> a következő munkatárssal: <b>[[STAFF_NAME]]</b>, a következő időpontra: <b>[[APPOINTMENT_TIME]]</b> ezen a napon: <b>[[APPOINTMENT_DATE]]</b> amely ennyibe kerül <b>[[SERVICE_PRICE]]</b>.</p>
<p style="font: 14px Arial; color: #5f6267;">Azonnal értesítjük, ahogy <b>[[STAFF_NAME]]</b> megnézi beosztását és jóváhagyja foglalását.</p>
&nbsp;
 <p style="font: 14px Arial; color: #5f6267;">Kedves <b>[[STAFF_NAME]]</b>,</p>
<p style="font: 14px Arial; color: #5f6267;">Önnek új foglalási kérése érkezett.  Kérjük mihamarabb fogadja el, vagy utasítsa el a kérést. Gyors válaszát előre is köszöni <b>[[CLIENT_NAME]]</b>.</p>
<p style="font: 14px Arial; color: #5f6267;"><b>[[CLIENT_NAME]]</b> a következő szolgáltatást kéri: <b>[[SERVICE_NAME]]</b> Önnel, ebben az időpontban: <b>[[APPOINTMENT_TIME]]</b>, ezen a napon: <b>[[APPOINTMENT_DATE]]</b>, ezen az áron: <b>[[SERVICE_PRICE]]</b>. <b>[[CLIENT_NAME]]</b> telefonszáma és e-mailje:<b>[[CLIENT_PHONE]]</b> és <b>[[CLIENT_EMAIL]]</b> ha kapcsolatba kívánna lépni vele.</p>
<p style="font: 14px Arial; color: #5f6267;">Használhatja a lenti gombokat, hogy közvetlenül válaszoljon a megkeresésre, vagy kattintson <a href="[[PENDING_STAFF_APPOINTMENTS_URL]]">ide</a> hogy böngészőjében megjelenítsük Önnek a függőben lévő foglalás kérések listáját.</p>
[[ACCEPT_REJECT_RIBBON]]

&nbsp;
 [[STAFF_NAME]] elfogadta a találkozó kérését erre az időpontra: [[APPOINTMENT_TIME]] ezen a napon: [[APPOINTMENT_DATE]] [[STAFF_NAME]] elutasította a találkozó kérését, erre az időpontra: [[APPOINTMENT_TIME]] ezen a napon: [[APPOINTMENT_DATE]] "Elutasít" és "Elfogad" gombok szalagba szerkesztve A szolgáltatás ára [[SERVICE_PRICE]]. Kérjük, válassza ki, hogy hogyan szeretne fizetni: Kérjük, válasszon szolgáltatást: Ön {{a}} [[SERVICE_NAME]] szolgáltatást válaszotta ki, a szolgáltató [[STAFF_NAME]], dátum [[SERVICE_DATE]], [[SERVICE_TIME]]-kor. A szolgáltatás ára [[SERVICE_PRICE]]. [[STAFF_NAME]] az alábbi időpontokban szabad még [[SERVICE_NAME]]-re.
Klikkeljen a kiválasztott időpontra a folytatáshoz!
 Köszönjük! A foglalás sikeres. Részleteiről e-mailt küldtünk Önnek. Ön {{a}} [[SERVICE_NAME]] szolgáltatást válaszotta ki, a szolgáltató [[STAFF_NAME]], dátum [[SERVICE_DATE]], [[SERVICE_TIME]]-kor. A szolgáltatás ára [[SERVICE_PRICE]].
Kérjük, adja meg (vagy ellenőrizze) az elérhetőségeit a folytatáshoz.
   Kategória Kártya biztonsági kód Lejárati dátum Hitelkártya száma Kupon E-mail Szolgáltató Eddig: Név Személyek száma Hitelkártyával fizetek most Helyben fogok fizetni  PayPal-lal fizetek most Telefon E naptól kezdve keressünk Szolgáltatás Ettől: Válasszon kategóriát Bárki Válasszon szolgáltatást Ön elfogadta ezt az időpont kérelmet. [[CLIENT_NAME]]t értesítettük erről, és megkértük, hogy fizessen, Ön már korábban elfogadta ezt az időpont kérelmet, és [[CLIENT_NAME]] már fizetett is érte. Ez egy élő időpont. [[CLIENT_NAME]] időpontot kért Öntől, [[SERVICE_NAME]] céljából, [[SERVICE_DATE]] napon, [[SERVICE_TIME]]-kor, [[SERVICE_PRICE]] áron. Ez a foglalás nem található. Lehet, hogy az ügyfél időközben lemondta. Kérjük, fogadja vagy utasítsa el a az alábbi kérést. Ön elutasította ezt az időpont kérést. [[CLIENT_NAME]]t értesítettük. Elfogadás [[STAFF_NAME]] elfogadta az Ön időpont kérését. Kérjük, klikkeljen a "Tovább" gombra, hogy véglegesítse az időpontot. Időpont kérését köszönettel megkaptuk. [[STAFF_NAME]] részére elküldük, hogy fogadja el. Emailt küldünk majd Önnek, ha ez megtörtént. Sajnos, [[STAFF_NAME]] számára nem megfelelő az Ön által válaszott időpont, [[SERVICE_DATE]] napon, [[SERVICE_TIME]]-kor. Kérjük, klikkeljen az "Új időpont keresése" gombra, és válasszon egy másik időpontot! Részletek Kész Fizetés Szolgáltatás Időpont időpont elfogadása (gomb) szünet hozzáadása cégének címe szünetek: Elfogad Vissza Időpont lemondása Időpont véglegesítése Új időpont foglalása Időpont kezelése Tovább Elutasít Foglalás törlésének linkje Nem kategorizált Online konzultáció Online konzultáció Diagnosztikai interjú Terápia Szupervízió Liaison konzultáció egyéni mezők összesített értéke egyéni mezők összesített értéke (2 oszlopba tördelve) Megjegyzés ügyfél új jelszó ügyfél új felhasználónév Foglalás napja következő nap dátuma szolgáltatás dátuma szétkapcsolás ügyfél e-mailje munkatárs e-mailje <p style="font: 14px Arial; color: #5f6267;">Kedves <b>[[CLIENT_NAME]]</b>,</p>
<p style="font: 14px Arial; color: #5f6267;"><b>[[STAFF_NAME]]</b> elfogadta a foglalás kérését, aminek a részletei ezek: Szolgáltatás: <b>[[SERVICE_NAME]]</b>, ekkor: <b>[[APPOINTMENT_TIME]]</b>, ezen a napon: <b>[[APPOINTMENT_DATE]]</b>, ezen az áron: <b>[[SERVICE_PRICE]]</b>.</p>
<p style="font: 14px Arial; color: #5f6267;">Kérjük kattintson a lenti gombra amely elviszi Önt honlapunkra, ahol fizetéssel véglegesítheti a foglalását. Mihamarabbi véglegesítését <b>[[STAFF_NAME]]</b> előre is köszöni.
</p>
[[FINALIZE_APPOINTMENT_RIBBON]]

&nbsp;
 [[STAFF_NAME]] elfogadta a találkozó kérését, erre az időpontra: [[APPOINTMENT_TIME]] ezen a napon: [[APPOINTMENT_DATE]] <p style="font: 14px Arial; color: #5f6267;">Kedves <b>[[CLIENT_NAME]]</b>,</p>
<p style="font: 14px Arial; color: #5f6267;"><b>[[STAFF_NAME]]</b> elutasította a foglalás kérését, aminek a részletei ezek voltak: Szolgáltatás: <b>[[SERVICE_NAME]]</b>, ekkor: <b>[[APPOINTMENT_TIME]]</b>, ezen a napon: <b>[[APPOINTMENT_DATE]]</b>, ezen az áron: <b>[[SERVICE_PRICE]]</b>.</p>
<p style="font: 14px Arial; color: #5f6267;">Kérjük kattintson a lenti gombra egy új foglalás kérés elindításához.</p>
[[MAKE_NEW_APPOINTMENT_RIBBON]]

&nbsp;
 [[STAFF_NAME]] elutasította a foglalás kérését, amely erre az időpontra vonatkozott: [[APPOINTMENT_TIME]] ezen a napon: [[APPOINTMENT_DATE]] Kedves [[CLIENT_NAME]]!

Köszönjük, hogy minket választott. Reméljük, hogy elégedett volt a [[SERVICE_NAME]] szolgáltatással.

Köszönjük, és reméljük hamarosan újra igénybe veszi a szolgáltatásunkat!

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]]
 Kedves [[CLIENT_NAME]]!
Köszönjük, hogy minket választott. Reméljük, hogy elégedett volt a [[SERVICE_NAME]] szolgáltatással.
Reméljük hamarosan újra igénybe veszi a szolgáltatásunkat!
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]]
 Az Ön [[COMPANY_NAME]] konzultációja Kedves [[CLIENT_NAME]]!

Visszaigazoljuk, hogy foglalása megtörtént {{a}} [[SERVICE_NAME]]-re.

Várjuk Önt [[APPOINTMENT_DATE]] napon, [[APPOINTMENT_TIME]] időben.

Köszönjük, hogy cégünket választotta. 
[[COMPANY_NAME]]
 [[COMPANY_PHONE]]
 [[COMPANY_WEBSITE]]
 Foglalásának adatai [[COMPANY_NAME]]  [[COMPANY_LOGO]]

&nbsp;
<p style="font: 14px Arial; color: #5f6267;">Kérését {{a}} <b>[[SERVICE_NAME]]</b>-re <b>[[STAFF_NAME]]</b> szolgáltatóval, ezen a napon: <b>[[APPOINTMENT_DATE]]</b> <b>[[APPOINTMENT_TIME]]</b>-kor, </b> mely <b>[[SERVICE_PRICE]]</b>-ba kerül, megkaptuk, és továbbítottuk.</p>
<p style="font: 14px Arial; color: #5f6267;">Azonnal értesítjük, ahogy <b>[[STAFF_NAME]]</b> megnézi beosztását és jóváhagyja foglalását.</p>
&nbsp;
 Új találkozó kérés elküldve [[STAFF_NAME]] részére, erre a napra: [[APPOINTMENT_DATE]] ekkorra: [[APPOINTMENT_TIME]] Üdvözöljük!
A [[SITE_ADDRESS]] honlapon egy felhasználói fiókot hoztunk létre önnek.
Az ön felhasználói adatai:
Felhasználó név: [[NEW_USERNAME]]
Jelszó: [[NEW_PASSWORD]]
Köszönjük.
 Új ügyfél Kedves [[CLIENT_NAME]].

Szeretnénk emlékeztetni a foglalására, mely a következő szolgáltatást tartalmazza: [[SERVICE_NAME]] a holnapi napon ekkor: [[APPOINTMENT_TIME]]. Várjuk Önt [[COMPANY_ADDRESS]].

Köszönjük, hogy cégünket választotta. 

[[COMPANY_NAME]] 
[[COMPANY_PHONE]] 
[[COMPANY_WEBSITE]]
 Kedves [[CLIENT_NAME]]!
Szeretnénk emlékeztetni a foglalására, mely a következő szolgáltatást tartalmazza: [[SERVICE_NAME]] a holnapi napon ekkor: [[APPOINTMENT_TIME]]. Várjuk Önt [[COMPANY_ADDRESS]].
Köszönjük, hogy cégünket választotta.
[[COMPANY_NAME]] 
[[COMPANY_PHONE]] 
[[COMPANY_WEBSITE]]
 Az Ön foglalása itt: [[COMPANY_NAME]] Üdvözlöm.

A holnapi menetrendje: 

[[NEXT_DAY_AGENDA]]
 Üdvözöljük!
Az ön holnapi menetrendje
:
[[NEXT_DAY_AGENDA]]
 Az Ön menetrendje ezen a napon: [[TOMORROW_DATE]] Üdvözöljük!

A következő foglalás törölve lett.

Szolgáltatás: [[SERVICE_NAME]] 
Dátum: [[APPOINTMENT_DATE]]
Idő: [[APPOINTMENT_TIME]] 
Ügyfél neve: [[CLIENT_NAME]]
Ügyfél telefonja: [[CLIENT_PHONE]] 
Ügyfél e-mailje: [[CLIENT_EMAIL]]
 Foglalás törlése Üdvözöljük!
Új foglalása van.
Szolgáltatás: [[SERVICE_NAME]] 
Dátum: [[APPOINTMENT_DATE]]
Idő: [[APPOINTMENT_TIME]] 
Ügyfél neve: [[CLIENT_NAME]]
Ügyfél telefonja: [[CLIENT_PHONE]] 
Ügyfél e-mailje: [[CLIENT_EMAIL]]
 Üdvözöljük!
Új foglalása van.
Szolgáltatás: [[SERVICE_NAME]] 
Dátum: [[APPOINTMENT_DATE]]
Idő: [[APPOINTMENT_TIME]] 
Ügyfél neve: [[CLIENT_NAME]]
Ügyfél telefonja: [[CLIENT_PHONE]] 
Ügyfél e-mailje: [[CLIENT_EMAIL]]
 Új foglalás információk [[COMPANY_NAME]]  [[COMPANY_LOGO]]

&nbsp;
<p style="font: 14px Arial; color: #5f6267;">Tisztelt <b>[[STAFF_NAME]]!</b></p>
<p style="font: 14px Arial; color: #5f6267;">Önnek új találkozó kérése érkezett. Kérjük, fogadja el, vagy utasítsa el a kérést. Gyors döntését <b>[[CLIENT_NAME]]</b> előre is köszöni!</p>
<p style="font: 14px Arial; color: #5f6267;"><b>[[CLIENT_NAME]]</b> szeretné {{a}} <b>[[SERVICE_NAME]]</b> szolgáltatást igénybe venni Önnél,  dátum: <b>[[APPOINTMENT_DATE]]</b>, időpont: <b>[[APPOINTMENT_TIME]]</b>, <b>[[SERVICE_PRICE]]</b> áron. <b>[[CLIENT_NAME]]</b> telefonszáma és email címe <b>[[CLIENT_PHONE]]</b> és <b>[[CLIENT_EMAIL]]</b> ha szeretne vele kapcsolatba lépni.</p>
<p style="font: 14px Arial; color: #5f6267;">Az alábbi gombok segítségével rögtön léphet, vagy klikkeljen <a href="[[PENDING_STAFF_APPOINTMENTS_URL]]">ide</a>, ha az összes függőben lévő találkozóját szeretné egyszerre látni a böngészőjében.</p>
[[ACCEPT_REJECT_RIBBON]]

&nbsp;
 Új találkozót kérés [[CLIENT_NAME]] ügyféltől, erre az időpontra: [[APPOINTMENT_TIME]], ezen a napon: [[APPOINTMENT_DATE]] Hibás űrlap azonosító Kapcsolatazonosító hiba. időpont véglegesítése (gomb) időpont véglegesítése link szalagba szerkesztve, a lusták kedvéért bejelentkezési űrlap új találkozó link létrehozása (gomb) kategória neve ügyfél neve szolgáltatás megnevezése munkatárs neve Cégének neve személyek száma ügyfél telefonja munkatárs telefonja munkatárs fotója szolgáltatás ára Online konzultáció Online konzultáció (15 perc) Online konzultáció (15 perc) Diagnosztikus interjú (60 perc) Diagnosztikai interjú (60 perc) Pszichoterápia (30 perc) Pszichoterápia (30 perc) Online konzultáció Online konzultáció (60 perc) Online konzultáció (60 perc) Pszichoterápia (60 perc) Pszichoterápia (60 perc) Pszichoterápia (45 perc) Pszichiátriai konzultáció (15 perc) Pszichiátriai konzultáció (30 perc) Pszichiátriai konzultáció (45 perc) Pszichiátriai konzultáció (60 perc) Szupervízió Szupervízió (30 perc) Felügyelet (30 perc) Szupervízió Szupervízió (60 perc) Felügyelet (60 perc) Liaison konzultáció egészségügyi szolgáltatók számára Liaison konzultáció Egészségügyi Szolgáltatóknak (30 perc) Liaison konzultáció egészségügyi szolgáltatók számára (30 perc) Online konzultáció (30 perc) Online konzultáció (30 perc) Liaison konzultáció egészségügyi szolgáltatók számára Liaison konzultáció Egészségügyi Szolgáltatóknak (15 perc) Liaison konzultáció egészségügyi szolgáltatók számára (15 perc) Liaison konzultáció egészségügyi szolgáltatók számára Liaison konzultáció Egészségügyi Szolgáltatóknak (60 perc) Liaison konzultáció egészségügyi szolgáltatók számára (60 perc) Pszichoterápiás első interjú (60 perc) Pszichoterápiás első interjú (90 perc) honlap címe munkatárs napirendje a következő napon Bárki Dr. Sobel Ben Csukly Gábor Angyalosi Anna Hentes János dr. Polgár Patrícia dr. Polgár Patrícia dr. Simon Viktória Dr. Simon Viktória  Dr. Szoldan Mark  dr. Simon Lajos ennek a honlapnak a címe Foglalás időpontja szolgáltatás dátuma ig a foglalás teljes ára cégének logója cégének telefonszáma 