<?php

/**
 * Plugin Name: Debug logger
 * Plugin URI: http://easyitme.com
 * Description: Creates an API which lets other plugins to store custom data for troubleshooting purposes. Also can set email to send error reports to.
 * Version: 2.0.0
 * Changelog: 2.0.0 added email option for errors
 * Author: Akos Adam Medgyes, Peter Szoldan
 * Author URI: http://easyitme.com
 * License: strict
 */

register_activation_hook( __FILE__, 'debug_logger_install');
register_deactivation_hook( __FILE__, 'debug_logger_uninstall');

global $debug_logger_table_name, $wpdb;
$debug_logger_table_name = $wpdb->prefix . 'dl_entries';
$email_setting_name = 'debug_logger_email_address_to_send_error_reports_to';
$verbosity_setting_name = 'debug_logger_verbosity_level';

function append_to_option( $o, $msg ) {
	$v = get_option( $o ) || '';
	if ( '' == $v ) {
		add_option( $msg );
	} else {
		update_option( $msg );
	}
}
// set this plugin to load first, before all other plugins to be able to handle errors occurring in those plugins
if ( is_admin() ) {
	set_debug_logger_to_load_first();
}
function set_debug_logger_to_load_first() {
	$path = str_replace( WP_PLUGIN_DIR . '/', '', __FILE__ );
	if ( $plugins = get_option( 'active_plugins' ) ) {
		if ( $key = array_search( $path, $plugins ) ) { // exists in list AND not the first yet
			array_splice( $plugins, $key, 1 );
			array_unshift( $plugins, $path );
			update_option( 'active_plugins', $plugins );
			debug_logger_new_entry( array(
					'error_level' => 10, // info
					'plugin' => 'Debug Logger Admin',
					'comment' => 'Debug logger plugin made first in loading order.',
			));
			append_to_option( 'debug_logger_admin_notice',
				"<div class='notice notice-info is-dismissible'>"
				."<p>Debug logger plugin made first in loading order.</p>"
				."</div>" );
		}
	}
}
function debug_logger_admin_notice(){
	$msg = get_option( "debug_logger_admin_notice" );
	if ( $msg ){ echo $msg; }
	delete_option( "debug_logger_admin_notice" );
}
add_action( 'admin_notices', 'debug_logger_admin_notice' );
function dlprintWpdbErrorAsAdminNotice($act){
    global $wpdb;
    if ( '' !== $wpdb->last_error ) {
        ob_start();
        $wpdb->show_errors();
        $wpdb->print_error();
        $wpdb->hide_errors();
        $err = ob_get_contents();
        ob_end_clean();
    }else{
        $err = false;
    }

    echo $err ? preg_replace('<div id="error">', "<div class=\"error\"><p>$act</p>", $err)
                    :  "<div class=\"updated\"><p>$act</p></div>";
}
function debug_logger_install(){
	global $wpdb, $debug_logger_table_name;
    $charset_collate = $wpdb->get_charset_collate();
    ob_start(NULL, 0, PHP_OUTPUT_HANDLER_STDFLAGS);
    
    // please note key definitions MUST come after all column definitions because of the
    // picky nature of the dbDelta() funcion
    // see more here: https://codex.wordpress.org/Creating_Tables_with_Plugins
    $sql = "CREATE TABLE $debug_logger_table_name (
        id INT NOT NULL AUTO_INCREMENT,
        error_level INT(1) UNSIGNED NOT NULL DEFAULT 10,
        timestamp TIMESTAMP DEFAULT NOW(),
        plugin VARCHAR(255),
        user_agent TEXT,
        client_ip VARCHAR(16),
        user_id int(10) UNSIGNED NOT NULL,
        comment VARCHAR(2048),
        backtrace VARCHAR(2048),
        PRIMARY KEY  ( id ),
        KEY error_level ( error_level ),
        KEY plugin ( plugin )
    ) $charset_collate";
    //dlprintWpdbErrorAsAdminNotice("Creating '$debug_logger_table_name' table if it not exists.");
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    echo "<div class='notice notice-info is-dismissible'><p>"
    	.implode( '<br />', dbDelta( $sql ) )."</p></div>";

    debug_logger_new_entry( array(
    		'error_level' => 10, // info
    		'plugin' => 'Debug Logger Admin',
    		'comment' => 'Debug logger plugin enabled',
    ));
    append_to_option( 'debug_logger_admin_notice', ob_get_clean() );
}
function debug_logger_uninstall(){
	debug_logger_new_entry( array(
			'error_level' => 10, // info
			'plugin' => 'Debug Logger Admin',
			'comment' => 'Debug logger plugin disabled',
	));
	
}
function generateCallTrace() {
	$e = new Exception();
	$trace = explode("\n", $e->getTraceAsString());
	// reverse array to make steps line up chronologically
	// $trace = array_reverse($trace);
	array_pop($trace); // remove {main}
	array_shift($trace); // remove call to this method
	$shift_until = -1;
	foreach( $trace as $key => $t ) {
		if ( false !== strpos( $t, 'debug_logger_new_entry' )
				|| false !== strpos( $t, 'debug_logger_error_handler' ) ) {
			$shift_until = max( $key, $shift_until );
		}
	}
	$trace = array_slice( $trace, $shift_until + 1 );
	
	$url_prefix = implode( '/', array_slice( explode( '/', __FILE__ ), 0, 4 ) );
	$length = count($trace);
	$result = array();
	for ($i = 0; $i < $length; $i++) {
		$result[] = str_replace( $url_prefix, '',
				str_pad( ($i + 1), 4, ' ', STR_PAD_LEFT )  . ')' . substr($trace[$i], strpos($trace[$i], ' ') )
				// replace '#someNum' with '$i)', set the right ordering
		);
	}
	return implode( "\n", $result );
}
function debug_logger_new_entry( $entry ){
    global $wpdb, $debug_logger_table_name, $email_setting_name, $verbosity_setting_name;
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    $client_ip = $_SERVER['REMOTE_ADDR'];
    $user_id = get_current_user_id();
    $backtrace = generateCallTrace();
    if ( !array_key_exists( 'plugin', $entry ) || !$entry[ 'plugin' ] ) {
    	// try to determine plugin from backtrace
    	$matches = array();
    	if( preg_match( '_/wp-content/plugins/([^/]*)/_', $backtrace, $matches ) ) {
    		$entry[ 'plugin' ] = $matches[ 1 ]; // use first plugin encountered
    	}
    }
    $sql = $wpdb->prepare("INSERT INTO $debug_logger_table_name
        (error_level, plugin, user_agent, client_ip, user_id, comment, backtrace)
        VALUES (%d, %s, %s, %s, %d, %s, %s)",
    		$entry[ 'error_level' ], $entry['plugin'], $user_agent, $client_ip, $user_id,
    		$entry[ 'comment' ], $backtrace );
    $wpdb->query($sql);
    
    $email_to = get_option( $email_setting_name );
    if ( "" != $email_to ) {
    	$verbosity = get_option( $verbosity_setting_name );
    	if ( $verbosity >= $entry[ 'error_level' ] ) {
    		// send email
    		$b =  get_browser( null, true );
    		$nature = '';
    		switch ( $entry[ 'error_level' ] ) {
    			case 1: case 2: case 3: case 4: case 5: $nature = "error"; $t = "An error has occurred"; break;
    			case 6: case 7: $nature = "warning"; $t = "A warning was issued"; break;
    			case 8: case 9: $nature = "notice"; $t = "A notice was posted"; break;
    			case 10: $nature = "info"; $t = "Information was provided"; break;
    			default: $nature = "unknown"; $t = "Log entry with no type was posted";
    		}
    		$message = "$t on ".get_site_url().".\n"
    			. "Error level:" . $entry[ 'error_level' ] . "\n"
    			. "Timestamp: " . date( "Y-M-D H:i:s" ) . "\n"
    			. "Plugin: ". $entry[ 'plugin' ] . "\n"
    			. "User agent: ". $b[ 'platform' ] . " " . $b['browser'] . " v" . $b[ 'version' ]  . "\n"
				. "Client IP: " . $client_ip . "\n"
				. "User id: " . $user_id . "\n"
				. "Comment: " . $entry[ 'comment' ] . "\n"
				. "Backtrace:\n" . $backtrace
    		;
    		$c = $entry[ 'comment' ];
    		if ( strlen( $c ) > 70 ) {
    			$c = substr( $c, 0, 65 ). " ...";
    		}
    		$subject = ucfirst( $nature ) . ": $c";
    		switch ( $_SERVER['HTTP_HOST'] ) {
    			case 'dev2.pszichonline.hu' : $site = "DEV2"; break;
    			case 'www.pszichonline.hu' : case 'pszichonline.hu' : $site = "PRD"; break;
    			default: $site = "UNK debug logger L".__LINE__; break;
    		}
    		$from = "OPPCS $site DEBUG <wordpress@".$_SERVER['HTTP_HOST'].">";
    		$headers = "From: $from\r\n"
    				."X-Mailer: PHP/" . phpversion();
    		mail( $email_to, $subject, $message, $headers );
    	}
    }
}
function most_recent_entries_in_log(){
	global $wpdb, $debug_logger_table_name;
	echo "<h2>Most recent entries in debug log</h2>";
	$res = $wpdb->get_results( "SELECT * FROM $debug_logger_table_name
			ORDER BY `id` DESC LIMIT 50" );
	if ( 0 < sizeof( $res ) ) {
		echo "<table style='border-collapse: collapse;'><thead><tr>";
		foreach ( $res[ 0 ] as $k => $devnull ) {
			$k2 = str_replace( "_", "&nbsp;", $k );
			$k3 = ucwords( $k2 );
			$k4 = str_replace( "Ip", "IP", $k3 );
			echo "<th style='border:1px solid grey;'>$k4</th>";
		}
		echo "</tr></thead></tbody>";
		$prev_date = "";
		foreach ( $res as $line ) {
			$curr_date = date( "Y-M-D", strtotime( $line->timestamp ) );
			$new_day = $prev_date != $curr_date;
			$prev_date = $curr_date;
			$b =  get_browser( $line->user_agent, true );
			$line->user_agent = $b[ 'platform' ] . " " . $b['browser'] . " v" . $b[ 'version' ];
			echo "<tr>";
			foreach ( $line as $k => $cell ) {
				if ( 'timestamp' == $k ) {
					$cell = str_replace( array( " ", "-" ), array( "&nbsp;", "&#8209;" ), $cell );
				}
				$cell = str_replace( "\n\t", "<br />", $cell );
				echo "<td style='border:1px solid grey; padding: 2px; font-size: 10px;"
						.( $new_day ? "border-top: 5px double black;" : "" )."'>$cell</td>";
			}
			echo "</tr>";
		}
		echo "</tbody>";
		echo "</table>";
	} else {
		echo "<p><i>Log empty.</i></p>";
	}
}

$verbosity = get_option( $verbosity_setting_name ) * 1;
$email = get_option( $email_setting_name );
$error_masks =  array(
			0 => 0,
			1 => E_ERROR,
			2 => E_ERROR,
			3 => E_ERROR,
			4 => E_ERROR,
			5 => E_ERROR,
			6 => E_ERROR | E_WARNING,
			7 => E_ERROR | E_WARNING,
			8 => E_ERROR | E_WARNING | E_NOTICE,
			9 => E_ERROR | E_WARNING | E_NOTICE,
			10 => E_ALL|E_STRICT|E_DEPRECATED,
); 
if ( $verbosity > 0 && '' != $email ) {
	$error_mask = $error_masks[ $verbosity ];
 	set_error_handler( 'debug_logger_error_handler', $error_mask );
}
function debug_logger_error_handler( $errno, $errstr, $errfile, $errline ){
	global $error_masks;
	foreach( $error_masks as $level => $e) {
		if ( ( $errno & $e ) > 0 ) {
			$error_level = $level;
			break;
		}
	}
	debug_logger_new_entry( array(
			'error_level' => $error_level,
			'plugin' => false, // determine plugin from backtrace
			'comment' => $errstr,
			'skip_backtrace_entries' => 2,
	) );
	return false;
}

// admin settings
define("DEBUG_LOGGER_SETTINGS_PAGE_NAME", "debug_logger_plugin");
add_action('admin_menu', 'debug_logger_plugin_admin_add_page');
function debug_logger_plugin_admin_add_page() {
	add_options_page('Debug Logger', 'Debug Logger Settings', 'manage_options',
			DEBUG_LOGGER_SETTINGS_PAGE_NAME, 'debug_logger_plugin_options_page');
}
function debug_logger_plugin_add_settings_link($links){
	$l = '<a href="options-general.php?page='.DEBUG_LOGGER_SETTINGS_PAGE_NAME
			.'">' . ( 'Settings' ) . '</a>';
	array_push( $links, $l);
	return $links;
}
add_filter( "plugin_action_links_" . plugin_basename(__FILE__), 'debug_logger_plugin_add_settings_link');
$option_group = 'debug_logger_plugin_options';
function debug_logger_plugin_options_page(){
	global $option_group;
	echo "<div>";
	echo "<h2>Debug Logger Settings</h2>";
	echo "<form action=\"options.php\" method=\"post\">";
	settings_fields( $option_group );
	do_settings_sections('debug_logger_plugin');
	echo "<p class=\"submit\"><input name=\"submit\" type=\"submit\" value=\"Save Changes\" /></p>";
	echo "</form>";
	echo "</div>";
	echo "<hr>";
	most_recent_entries_in_log();
}
add_action('admin_init', 'debug_logger_plugin_admin_init');
function debug_logger_plugin_admin_init(){
	global $option_group, $email_setting_name, $verbosity_setting_name;

	register_setting( $option_group, $email_setting_name,
			array( 'sanitize_callback' => 'debug_logger_plugin_email_validator' ) );
	add_settings_section('email_options', 'Email Report Options', 'email_options_text', 'debug_logger_plugin');
	add_settings_field( $email_setting_name,
			'Debug report emails will be sent to this address(es). Separate multiple addresses with commas.',
			'email_address_input', 'debug_logger_plugin', 'email_options');

	register_setting( $option_group, $verbosity_setting_name );
	add_settings_field( $verbosity_setting_name,
			'Set to zero to turn off email reporting; set to max to get emails about every error, ' 
			.'or finetune to somewhere in between.',
			'verbosity_input', 'debug_logger_plugin', 'email_options');
}
function email_options_text(){
	echo '<p><i>Set options related to email error reports.</i></p>';
}
function email_address_input(){
	global $email_setting_name;
	$opt = get_option( $email_setting_name );
	echo "<textarea id='$email_setting_name' name='$email_setting_name' placeholder='Enter email addresses here...' "
			." rows='4' cols='60'>{$opt}</textarea>";
}
function verbosity_input(){
	global $verbosity_setting_name;
	$opt = get_option( $verbosity_setting_name );
	$options = array(
			0 => 'Turn off reporting',
			1 => 'Fatal errors only',
			2 => '',
			3 => '',
			4 => 'All runtime errors',
			5 => '',
			6 => 'All errors and warnings',
			7 => '',
			8 => 'All errors, warnings, and notices',
			9 => '',
			10 => 'All errors, warnings, notices, and info',
	);
	echo "<select id='$verbosity_setting_name' name='$verbosity_setting_name'>";
	foreach ( $options as $k => $o ) {
		echo "<option ".( $opt == $k ? 'selected ' : '')." value='$k' ".( "" === $o ? "disabled" : "").">"
				."$k - ".( "" === $o ? '(reserved for future use)' : $o )."</option>";
	}
	echo "</select>";
}
require_once( __DIR__.'/email_validator.php' );
function in_place_trim( &$value ) { $value = trim( $value ); }
function filter_empty_string( $value ) { return "" != $value; }
function debug_logger_plugin_email_validator( $new_value ){
	global $email_setting_name;
	$old_value = get_option( $email_setting_name );
	$emails = explode( ',', $new_value );
	array_walk( $emails, 'in_place_trim' );
	$emails_filtered = array_filter( $emails, 'filter_empty_string' );
	$any_errors = false;
	foreach ( $emails_filtered as $email ) {
		if ( !is_email_valid( $email ) ) {
				if ( !$any_errors ) {
					$any_errors = true;
					add_settings_error( $setting_name, 'validation-error',
							"Error(s) prevented saving the new email setting value <i>$new_value</i>.", 'error' );
				}
				add_settings_error( $setting_name, 'validation-error',
						"The email address <i>$email</i> is not valid.", 'error' );
		}
	}
	if ( !$any_errors ) {
		debug_logger_new_entry( array(
			'error_level' => 10, // info
			'plugin' => 'Debug Logger Admin',
			'comment' => 'Updating email settings to '.implode( ',', $emails_filtered )
		));
	}
	return $any_errors ? $old_value : implode( ',', $emails_filtered );
}