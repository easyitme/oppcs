<?php
/**
 *Validate an email address.
 *Provide email address (raw input)
 *Returns true if the email address has the email
 *address format and the domain exists.
 *http://www.linuxjournal.com/article/9585?page=0,0
 *
 * Email addresses must conform to the following specification:
 *
 * 1. An e-mail address consists of local part and domain separated by an at sign (@) character (RFC 2822 3.4.1).
 * 2. The local part may consist of alphabetic and numeric characters, and the following characters:
 * 		!, #, $, %, &, ', *, +, -, /, =, ?, ^, _, `, {, |, } and ~, possibly with dot separators (.), inside,
 * 		but not at the start, end or next to another dot separator (RFC 2822 3.2.4).
 * 3. The local part may consist of a quoted string—that is, anything within quotes ("), including spaces (RFC 2822 3.2.5).
 * 4. Quoted pairs (such as \@) are valid components of a local part, though an obsolete form from RFC 822 (RFC 2822 4.4).
 * 5. The maximum length of a local part is 64 characters (RFC 2821 4.5.3.1).
 * 6. A domain consists of labels separated by dot separators (RFC1035 2.3.1).
 * 7. Domain labels start with an alphabetic character followed by zero or more alphabetic characters, numeric characters or the hyphen (-),
 *		ending with an alphabetic or numeric character (RFC 1035 2.3.1).
 * 8. The maximum length of a label is 63 characters (RFC 1035 2.3.1).
 * 9. The maximum length of a domain is 255 characters (RFC 2821 4.5.3.1).
 * 10. The domain must be fully qualified and resolvable to a type A or type MX DNS address record (RFC 2821 3.6).
 */
function is_email_valid($email) {
	$isValid = true;
	$atIndex = strrpos($email, "@"); // find rightmost @
	if (is_bool($atIndex) && !$atIndex) { $isValid = false; } // if there's no @, it's over
	else {
		$domain = substr($email, $atIndex+1); // get domain part
		$local = substr($email, 0, $atIndex); // get local part
		$localLen = strlen($local); // length of local part
		$domainLen = strlen($domain); // length of domain part
		if ($localLen < 1 || $localLen > 64) { $isValid = false; } // local part length exceeded
		else if ($domainLen < 1 || $domainLen > 255) { $isValid = false; } // domain part length exceeded
		else if ($local[0] == '.' || $local[$localLen-1] == '.') { $isValid = false; } // local part starts or ends with '.'
		else if (preg_match('/\\.\\./', $local)) { $isValid = false; } // local part has two consecutive dots
		else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) { $isValid = false; } // character not valid in domain part
		else if (preg_match('/\\.\\./', $domain)) { $isValid = false; } // domain part has two consecutive dots
		else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/',str_replace("\\\\","",$local))){
			// character not valid in local part unless local part is quoted
			if (!preg_match('/^"(\\\\"|[^"])+"$/',str_replace("\\\\","",$local))) { $isValid = false; } }
			if ($isValid && !(dns_get_record($domain,DNS_MX)||dns_get_record($domain,DNS_A))) { $isValid = false; } //||
			// domain not found in DNS (checkdnsrr doesn't work for some reasons, always returns TRUE)
	}
	return $isValid;
}