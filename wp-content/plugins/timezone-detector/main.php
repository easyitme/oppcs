<?php

/**
 * Plugin Name: Timezone Detector
 * Plugin URI: http://easyitme.com
 * Description: Detect user's timeoffset and stores it in usermeta if logged in
 * Version: 1.0.0
 * Author: Peter Szoldan
 * Author URI: http://easyitme.com
 * License: strict
 */

@session_start();
$timeoffset = array_key_exists( 'timeoffset', $_SESSION ) ? $_SESSION[ 'timeoffset' ] : false;

if ( !$timeoffset ) {
	add_action( 'wp_enqueue_scripts', 'enqueue_detect_timeoffset' );
}
function enqueue_detect_timeoffset(){
	wp_register_script('detect_timeoffset', plugins_url( 'detect-timeoffset.js', __FILE__), 'jquery');
	wp_enqueue_script("detect_timeoffset");
	wp_localize_script( 'detect_timeoffset', 'ajax_object',
			array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_ajax_register_timeoffset', 'register_timeoffset' );
function register_timeoffset() {
	$_SESSION[ 'timeoffset' ] = $_POST[ 'timeoffset' ];
	set_timeoffset_usermeta();
	wp_die();
}
function set_timeoffset_usermeta() {
	if( is_user_logged_in() ) {
		$uid = get_current_user_id();
		$meta = get_user_meta( $uid );
		$to = $_SESSION[ 'timeoffset' ];
		if ( array_key_exists( 'timeoffset', $meta ) ) {
			update_user_meta( $uid, 'timeoffset', $to );
		} else {
			add_user_meta( $uid, 'timeoffset', $to );
		}
	}
}