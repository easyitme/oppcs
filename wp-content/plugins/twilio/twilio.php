<?php

/**
 * Plugin Name: Twilio WebRTC plugin
 * Plugin URI: http://easyitme.com
 * Description: Integrating Twilio WebRTC into via shortcode
 * Version: 1.6.1
 * Author: Akos Adam Medgyes, Peter Szoldan
 * Author URI: http://easyitme.com
 * License: strict
 *
 */

define('TWILIO_RESOURCE_VERSION', '1.6');

foreach(scandir(__DIR__) as $f)
{
    $spl = new SplFileInfo(__DIR__ . '/' . $f);
    if ($spl->isFile() && 'php' == $spl->getExtension())
    {
        require_once($spl->getPathname());
    }
}

global $twilio_wpml_package, $twilio_wpml_messages, $twilio_message_data;
$twilio_wpml_package = array(
		'kind' => 'Twilio Video Chat',
		'name' => 'messages',
		'title' => 'Twilio Video Chat messages',
		'edit_link' => '',
		'view_link' => ''
);
$twilio_wpml_messages = array(
	'WEBRTCNA'		=> 'WebRTC is not available in your browser',
	'MOREINFLNK' 	=> 'Click {here} for more info on browser compatibility',
	'REQTOK'				=> 'Requesting video chat token from server',
	'STARTPLAYER'	=> 'Starting video player',
	'NOCAMTRYMIC'=> 'Camera not found, trying microphone only',
	'NOCAMNOMIC'	=> 'There is no camera or microphone attached or you didn\'t allow access to them',
	'RELOADWSOL'	=> 'Reload the page when you solved the problems',
	'COULDNTCON'	=> 'Couldn\'t connect to the conference',
	'ERRSTART'			=> 'Error while starting the player',
	'ERRTOKEN'		=> 'Error while requesting token',
	'SUCCCONN'		=> 'Successfully connected to room',
	'YOU'						=> 'You',
	'WAITINGREMP'	=> 'Waiting for a remote party to connect',
	'AUDIOONLY'		=> 'Audio only',
	'SHAREFILEWB'	=> 'Share file on whiteboard',
	'CLEARWB'			=> 'Clear whiteboard',
);
$twilio_message_data = array(
	'MOREINFLNK' => array( 'https://github.com/twilio/twilio-video.js/blob/master/COMMON_ISSUES.md' )
);
function twilio_get_translated_message( $name ) {
	global $twilio_wpml_package, $twilio_wpml_messages, $twilio_message_data;
	if ( array_key_exists( strtoupper( $name ), $twilio_wpml_messages ) ) {
		$msg = apply_filters( 'wpml_translate_string', $twilio_wpml_messages[ strtoupper( $name ) ],
				'msg-' . strtolower( $name ), $twilio_wpml_package );
		if ( array_key_exists( strtoupper( $name ), $twilio_message_data ) ) {
			foreach ( $twilio_message_data[ strtoupper( $name ) ]  as $data ) {
				$msg = preg_replace( '/\{(\w+)\}/', '<a target="_blank" href="' . $data . '">$1</a>',
					$msg, 1);
			}
		}
		return $msg;
	} else {
		return '';
	}
}

register_activation_hook( __FILE__, 'twilio_install');
function twilio_install() {
	global $twilio_wpml_package, $twilio_wpml_messages, $sitepress;
	$title = "Twilio message";
	$type = "LINE";
	$orig_lang = $sitepress->get_default_language();
	$sitepress->set_default_language( 'en' ); // register strings as English
	foreach ( $twilio_wpml_messages as $name => $value ) {
		do_action( 'wpml_register_string', $value, 'msg-' . strtolower( $name ), $twilio_wpml_package, $title, $type );
	}
	$sitepress->set_default_language( $orig_lang );
}

add_action('wp_enqueue_scripts', 'twilio_register_script');
function twilio_register_script(){
	global $twilio_wpml_package, $twilio_wpml_messages, $twilio_message_data;
    wp_register_script("twilio-video", plugins_url('twilio-video.min.js', __FILE__), array(), TWILIO_RESOURCE_VERSION);
    wp_register_script("twilio", plugins_url('twilio.js', __FILE__),
        array( 'jquery', 'wp-ajax-response', 'twilio-video' ),
        TWILIO_RESOURCE_VERSION
    );
    
    $twilio_translated_messages = array();
    foreach( $twilio_wpml_messages as $name => $value ) {
    	$twilio_translated_messages[ strtolower( $name ) ] = 
    		apply_filters( 'wpml_translate_string', $value, 'msg-' . strtolower( $name ), $twilio_wpml_package );
    }
    foreach ( $twilio_message_data as $name => $data_arr ) {
    	$name = strtolower( $name );
    	foreach ( $data_arr as $data ){
    		$twilio_translated_messages[ $name ] = preg_replace( '/\{(\w+)\}/', 
    			'<a target="_blank" href="' . $data . '">$1</a>', $twilio_translated_messages[ $name ], 1);
    	}
//     trigger_error( print_r( $twilio_translated_messages, true ), E_USER_NOTICE );
    }
    wp_localize_script('twilio', 'twilio_backend', array(
        'ajax_url' => admin_url('admin-ajax.php'),
    	'twilio_messages' => $twilio_translated_messages
    ));
}

add_action('wp_enqueue_scripts', 'twilio_register_style');
function twilio_register_style()
{
    wp_register_style("twilio", plugins_url('twilio.css', __FILE__), array(), TWILIO_RESOURCE_VERSION);
}

add_shortcode('twilio_video_feed', 'twilio_video_feed');
/*
 * Expected case sensitive arguments of the shortcode
 *
 * @param id    string The ID of the room, required by Twilio
 * @param title string The name of the room, used only for informing the user
 */
function twilio_video_feed($raw_args = [])
{
    $args = shortcode_Atts(array(
        'id' => 'Test id',
        'title' => 'Test title'
    ), $raw_args);

    wp_enqueue_script("twilio");
    wp_enqueue_style("twilio");

    debug_logger_new_entry( array( 
    		'error_level' => 10,
    		'plugin' => 'Twilio',
    		'comment' => $args['id']) );

    $share_button_text = twilio_get_translated_message( 'SHAREFILEWB' );
    $clear_button_text = twilio_get_translated_message( 'CLEARWB' );
    echo <<<EOF
    <h5 class="twilio-title">${args['title']}</h5>
    <input id="twilio-room-id" type="hidden" value="${args['id']}"/>
    <div id="main-video" class="oppcs_video_your_next_appointment oppcs_no_video_msg">
    	<p id="data-track"></p>
        <p id="video-log"></p>
    </div>
    <div id="participants"></div>
    <p class="share-file">
    	<input id="share-file" type="file" accept=".txt,.jpg,.png,.pdf"/>
    	<button id="whiteboard" disabled>$share_button_text</button>
    	<button id="clear_whiteboard" disabled>$clear_button_text</button>
    </p>
EOF;
}
