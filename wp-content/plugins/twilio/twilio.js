// https://github.com/twilio/twilio-video.js/blob/master/COMMON_ISSUES.md
var dataTrack;
const twilioDebug = false;
const prod = false;
const twilioLogLevel = twilioDebug ? 'debug' : ( prod ? 'error' : 'warn' );
const twilio_data_chunk_max_length = 15000;
const twilio_header_length = 16;
const twilio_type_codes = {
		'txt' : 0,
		'jpg' : 1,
		'png' : 2,
		'pdf' : 3,
		'clr' : 4, // clear whiteboard msg
}
var twilio_type_codes_rev = new Array( twilio_type_codes.length );
for ( var type in twilio_type_codes ) {
	if( !twilio_type_codes.hasOwnProperty( type ) ) continue;
	twilio_type_codes_rev[ twilio_type_codes[ type ] ] = type;
}
const twilio_data_receive_buffers = {};
const twilio_data_receive_metadata = {};
jQuery(document).ready( function( $ ) {
	// Check for WebRTC
	if ( !Twilio.Video.isSupported ) {
		video_log( 'webrtcna' ); // web rtc not available
		video_log( 'moreinflnk' ); // more info on browser compatibility
		return;
	}
	
    video_log( 'reqtok' ); // requesting token
    jQuery.ajax({
        url : twilio_backend.ajax_url,
        type : 'post',
        data : {
            action : 'twilio_generate_new_token'
        },
        success : function( raw_response ) {
            video_log( 'startplayer' );
            try {
                response =  JSON.parse(raw_response);
                if (response.success) {
                    identity = response.identity;
                    roomName = jQuery('#twilio-room-id').attr('value');
//                    collectStreams(response.token);
                    Twilio.Video.connect( response.token, { name: roomName, logLevel : twilioLogLevel }).then( connectedToRoom,
                    	function (e) {
	                    	if ( "DevicesNotFoundError" == e.name || true ) { // presumably no camera, let's try with audio only
	                    	    video_log( 'nocamtrymic' );
	                    		Twilio.Video.connect( response.token, { name : roomName, audio : true, logLevel : twilioLogLevel }).then( connectedToRoom,
	                    			function() {
	                    				video_log( 'nocamnomic' );
	                    				video_log( 'reloadwsol' );
	                    				video_log( 'couldntcon' );
	                    			} );
	                    	} else { // rethrow if something else
	                    		throw (e);
	                    	}
	                    } );
                }
                else {
                    video_log( 'errstart', response );
                    video_log(response.error);
                }
            }
            catch (error) {
                video_log( 'errstart', response );
                video_log( 'errstart', error.message );
            }
        },
        error : function( response ) {
            video_log( 'errtoken', response);
        }
    });
});

function connectedToRoom( room ) {
	video_log( 'succconn' );
	console.log( 'Connected to Room "%s"', room.name );
	console.log( room );

	// When we are about to transition away from this page, disconnect from the room, if joined.
	window.addEventListener( 'beforeunload', room.disconnect );

	// create and add data track
    dataTrack = new Twilio.Video.LocalDataTrack();
    room.localParticipant.publishTrack( dataTrack );
    jQuery( '#whiteboard' ).on( 'click', sendShareFileOnDataTrack );
    jQuery( '#clear_whiteboard' ).on( 'click', clearWhiteBoard );
    jQuery( '#whiteboard,#clear_whiteboard' ).prop( 'disabled', false );

	const div = jQuery( '<div id="localparticipant" />' )[ 0 ];
	jQuery( '<p class="audio-only">' + twilio_backend[ 'twilio_messages' ][ 'audioonly' ]
		+ '</p>' ).appendTo( jQuery( div ) );
	jQuery( '<p class="participant_name">' + twilio_backend[ 'twilio_messages' ][ 'you' ]
		+ '</p>' ).appendTo( jQuery( div ) );
	try {
		div.appendChild( room.localParticipant.videoTracks.values().next().value.attach() );
		jQuery( div ).addClass( 'twilio-track-added-video' );
	} catch (e) {}
	jQuery( '#participants' ).append(div);
	
	if ( 0 == room.participants.size ) {
		video_log( 'waitingremp' );
	} else {
		room.participants.forEach( participantConnected );		
	}

	room.on( 'participantConnected', participantConnected );
	room.on( 'participantDisconnected', participantDisconnected );
	room.once( 'disconnected', error => room.participants.forEach( participantDisconnected ) );
}

function participantConnected(participant) {
	console.log('Participant "%s" connected', participant.identity);
	
	const div = document.createElement('div');
	div.id = participant.sid;
	jQuery( '<p class="audio-only">' + twilio_backend[ 'twilio_messages' ][ 'audioonly' ]
		+ '</p>' ).appendTo( jQuery( div ) );
	jQuery( '<p class="participant_name">' + participant.identity + '</p>' ).appendTo( jQuery( div ) );
	
	participant.on('trackAdded', track => trackAdded(div, track));
	participant.tracks.forEach(track => trackAdded(div, track));
	participant.on('trackRemoved', trackRemoved);
	jQuery( '#participants' ).append(div);

	if ( jQuery( '#main-video' ).hasClass( 'twilio_no_video_msg' ) ) {
		participant.on('trackAdded', track => trackAdded(jQuery( '#main-video' )[0], track));
		participant.tracks.forEach(track => trackAdded(jQuery( '#main-video' )[0], track));
	}
}
function participantDisconnected(participant) {
	console.log('Participant "%s" disconnected', participant.identity);
	participant.tracks.forEach(trackRemoved);
	document.getElementById(participant.sid).remove();
}
function trackAdded(div, track) {
//	console.log( track );
	if ( div != jQuery( '#main-video' )[ 0 ] || 'video' == track.kind ) { // don't add audio to main video screen
		if ( 'video' == track.kind ){
			// only attach one video to main video
			if ( div != jQuery( '#main-video' )[ 0 ] || 0 == jQuery( '#main-video').has( 'video' ).length ) {
				div.appendChild(track.attach());
			}
		} else if ( 'audio' == track.kind ) {
//			console.log( 'appending track' );
			div.appendChild(track.attach());
		} else if ( 'data' == track.kind ) {
//			console.log( 'attaching message event handler' );
			track.on( 'message', dataMessageReceived );
		}
		jQuery( div ).removeClass( 'twilio_no_video_msg' ).addClass( 'twilio-track-added-' + track.kind );
	}
}
function sendShareFileOnDataTrack() {
   	console.log( 'sending message' );
   	var file = jQuery( '#share-file' )[ 0 ].files[ 0 ];
   	if ( !file ) {
//   		console.log( 'No file selected to share' );
   	} else {
   		console.log( file );
   		// determine type
   		var type = -1;
   		for( var ext in twilio_type_codes ) {
   			if ( !twilio_type_codes.hasOwnProperty( ext ) || 'clr' == ext ) continue;
   			if ( '.' + ext ==  file.name.substr( file.name.length - ext.length - 1 ) ){
   				type = twilio_type_codes[ ext ];
   			}
   		}
   		if ( -1 == type ) {
   			video_log( 'FILETYPENA' );
   		} else {
		   	var reader = new FileReader();
		   	reader.onload = function(e) {
		   		var contentsAB = e.target.result;
		   		var chunks = twilio_chunk_up_data( contentsAB, type );
		   		chunks.forEach( element => { dataTrack.send( element ); console.log( element ); });
		   		twilio_display_message( new Uint8Array( contentsAB ), type );
		   	}
		   	reader.readAsArrayBuffer( file );
   		}
   	}
}
function clearWhiteBoard() {
//	console.log( 'clearing whiteboard' );
	var contentsAB = new ArrayBuffer(1);
	var chunks = twilio_chunk_up_data( contentsAB, twilio_type_codes[ 'clr' ] );
	chunks.forEach( element => { dataTrack.send( element ); console.log( element ); });
	twilio_display_message( new Uint8Array( contentsAB ), twilio_type_codes[ 'clr' ] );
}
function b64_encode (input) {
    var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var output = "";
    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    var i = 0;

    while (i < input.length) {
        chr1 = input[i++];
        chr2 = i < input.length ? input[i++] : Number.NaN; // Not sure if the index 
        chr3 = i < input.length ? input[i++] : Number.NaN; // checks are needed here

        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;

        if (isNaN(chr2)) {
            enc3 = enc4 = 64;
        } else if (isNaN(chr3)) {
            enc4 = 64;
        }
        output += keyStr.charAt(enc1) + keyStr.charAt(enc2) +
                  keyStr.charAt(enc3) + keyStr.charAt(enc4);
    }
    return output;
}
function dataMessageReceived( dataAB ) {
//	console.log( 'data msg received', dataAB );
	var data = new Uint8Array( dataAB );
	var current_chunk_number = data[ 0 ] + 256 * data[ 1 ];
	var num_chunks = data[ 2 ] + 256 * data[ 3 ];
	var uid = data[ 4 ] + 256 * data[ 5 ] + 65536 * data[ 6 ] + 16777216 * data[ 7 ];
	var type = data[ 8 ];
	if ( undefined == twilio_data_receive_buffers[ uid ] ) {
		twilio_data_receive_metadata[ uid ] = {
			'num_chunks' : num_chunks,
			'type' : type,
		}
		twilio_data_receive_buffers[ uid ] = new Array( num_chunks);
	}
	twilio_data_receive_buffers[ uid ][ current_chunk_number ] = data.subarray( twilio_header_length );
//	console.log( twilio_data_receive_metadata );
//	console.log( twilio_data_receive_buffers );
	if ( twilio_data_receive_buffers[ uid ].length ==twilio_data_receive_metadata[ uid ][ 'num_chunks' ] ) {
		// all here, recreate original data
		var total_length = 0;
		twilio_data_receive_buffers[ uid ].forEach( chunk => { total_length += chunk.byteLength; })
//		console.log( 'Total length:', total_length );
		var total_data = new Uint8Array( total_length );
		var idx = 0;
		twilio_data_receive_buffers[ uid ].forEach( chunk => { total_data.set( chunk, idx ); idx += chunk.byteLength; })
//		console.log( 'Total data:', total_data );
//		console.log( 'String data:', new TextDecoder( "utf-8").decode( total_data ) );
		twilio_display_message( total_data, twilio_data_receive_metadata[ uid ][ 'type' ] );
	}
}
function get_html_to_display( message_data, type ) {
	var result = '';
	switch ( type ) {
	case twilio_type_codes[ 'txt' ] :
		var string_data = new TextDecoder( "utf-8").decode( message_data );
		result = jQuery( '#data-track' ).html() + '<br />' + string_data;
		break;
	case twilio_type_codes[ 'jpg' ]:
		result = '<img src="data:image/jpeg;base64,' + b64_encode( message_data ) + '"/>';
		break;
	case twilio_type_codes[ 'png' ]:
		result = '<img src="data:image/png;base64,' + b64_encode( message_data ) + '"/>';
		break;
	case twilio_type_codes[ 'pdf' ]:
		result = '<object data="data:application/pdf;base64,' + b64_encode( message_data )
				+ '" type="application/pdf">'
				+ '<embed src="data:application/pdf;base64,' + b64_encode( message_data )
				+ '" type="application/pdf" /></object>';
		break;
	}
	return result;
}
function twilio_remove_data_type_classes() {
	var mv = jQuery( '#main-video' );
	for( var type in twilio_type_codes ){
		if ( !twilio_type_codes.hasOwnProperty( type ) ) continue;
		mv.removeClass( 'data-type-' + type );
	}
}
function twilio_display_message( message_data, type ) {
	if ( twilio_type_codes[ 'clr' ] == type ) {
		jQuery( '#data-track' ).html('');
		jQuery( '#main-video' ).removeClass( 'whiteboard' );
		twilio_remove_data_type_classes();
	} else {
		var html_to_display = get_html_to_display( message_data, type );
		jQuery( '#data-track' ).html( html_to_display );
		twilio_remove_data_type_classes();
		jQuery( '#main-video' ).addClass( 'whiteboard' ).addClass( 'data-type-' + twilio_type_codes_rev[ type ] );
	}
}
function twilio_chunk_up_data( dataAB, type ) {

	var length = dataAB.byteLength;
	var num_chunks = Math.ceil( length / twilio_data_chunk_max_length );
	var chunks = [];
	var last_chunk_length = length - ( num_chunks - 1 ) * twilio_data_chunk_max_length;

	var unique_id = Math.floor( Math.random() * 9007199254740991 ); // 2^53 - 1
	for( var i = 0; i < num_chunks; i ++ ) {
		var chunk_length = i == num_chunks-1 ? last_chunk_length : twilio_data_chunk_max_length;
		var data = new Uint8Array( twilio_header_length + chunk_length );
		var current_chunk = new Uint8Array( dataAB, i * twilio_data_chunk_max_length, chunk_length );
		data.set( current_chunk, twilio_header_length );
		data[ 0 ] = i % 256; data[ 1 ] = Math.floor( i / 256 ); // serial of current chunk
		data[ 2 ] = num_chunks % 256; data[ 3 ] = Math.floor( num_chunks / 256 ); // num chunks
		var uid = unique_id;
		for ( var b = 0; b < 4 ; b++ ) {
			data[ 4 + b ] = uid % 256;
			uid = Math.floor( uid / 256 );
		}
		data[ 8 ] = type;
		chunks.push( data );
	}
	return chunks;
}
function trackRemoved(track) {
	if ( 'function' === typeof track.detach ) {
		track.detach().forEach(element => {
			if ( jQuery.contains( jQuery( '#main-video' )[ 0 ], element ) ) {
				jQuery( '#main-video' ).addClass( 'twilio_no_video_msg' );
			}
			element.remove();
		});
	}
}
function video_log( code, more_data ){
	message = twilio_backend[ 'twilio_messages' ][ code ];
	if ( undefined != more_data ) {
		message += more_data;
	}
    jQuery('#main-video').addClass('twilio_no_video_msg');
    jQuery('#video-log').html( jQuery('#video-log').html() + "<br />" + message);
}
