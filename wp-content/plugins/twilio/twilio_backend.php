<?php

add_action('wp_ajax_twilio_generate_new_token', 'twilio_generate_new_token');
add_action('wp_ajax_nopriv_twilio_generate_new_token', 'twilio_not_authorized_for_token');

function twilio_not_authorized_for_token()
{
    echo json_encode(array(
        'success' => false,
        'error' => "Csak bejelentkezett felhasználóknak érhető el a videó konzultáció"
    ));
    die();
}

function twilio_generate_new_token()
{
    if ( defined( 'DOING_AJAX' ) && DOING_AJAX )
    {
        try
        {
            include_once("Twilio/autoload.php");

            $current_user = wp_get_current_user();
            $identity = $current_user->display_name;

            // Create access token, which we will serialize and send to the client
            $token = new Twilio\Jwt\AccessToken(
                get_option("twilio_account_id"), // Account id
                get_option("twilio_api_key"), // api key
                get_option("twilio_api_secret"),   // api secret
                3600,
                $identity
            );

            // Grant access to Conversations
            $grant = new Twilio\Jwt\Grants\VideoGrant();
            $grant->setConfigurationProfileSid(get_option("twilio_config_sid")); // config sid
            $token->addGrant($grant);

            echo json_encode(array(
                'success' => true,
                'token' => $token->toJWT(),
                'identity' => $identity
            ));
        }
        catch (\Exception $error)
        {
            debug_logger_new_entry( array(
            		'error_level' => 1,
            		'plugin' => 'Twilio',
            		'comment' => "Token generation failed: " . $error->getMessage() ) );
            echo json_encode(array(
                'success' => false,
                'error' => $error->getMessage()
            ));
        }
    }
    die();
}
