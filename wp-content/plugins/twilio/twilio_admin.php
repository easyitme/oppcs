<?php
define("TWILIO_SETTINGS_PAGE_NAME", "twilio_plugin");
add_action('admin_menu', 'twilio_admin_add_page');
function twilio_admin_add_page()
{
    add_options_page('Twilio WebRTC', 'Twilio WebRTC Settings', 'manage_options', TWILIO_SETTINGS_PAGE_NAME, 'twilio_options_page');
}

add_filter("plugin_action_links_" . plugin_basename(__FILE__), 'twilio_add_settings_link');
function twilio_plugin_add_settings_link($links)
{
    $l = '<a href="options-general.php?page='.TWILIO_SETTINGS_PAGE_NAME.'">' . ('Settings') . '</a>';
    array_push( $links, $l);
    return $links;
}

function twilio_options_page()
{
    echo "<div>";
    echo "<h2>Twilio WebRTC Settings</h2>";
    echo "<form action=\"options.php\" method=\"post\">";
    settings_fields('twilio_options');
    do_settings_sections('twilio_options');
    echo "<p class=\"submit\"><input name=\"submit\" type=\"submit\" value=\"Save Changes\" /></p>";
    echo "</form>";
    echo "</div>";
}

add_action('admin_init', 'twilio_admin_init');
function twilio_admin_init()
{
    register_setting('twilio_options', 'twilio_account_id');
    register_setting('twilio_options', 'twilio_api_key');
    register_setting('twilio_options', 'twilio_api_secret');
    register_setting('twilio_options', 'twilio_config_sid');

    add_settings_section('twilio_options_section', 'Twilio account configuration', 'twilio_settings_description', 'twilio_options');
    add_settings_field('twilio_account_id',
        'Twilio account ID',
        'twilio_string_input', 'twilio_options', 'twilio_options_section', array('twilio_account_id'));
    add_settings_field('twilio_api_key',
        'Twilio API key',
        'twilio_string_input', 'twilio_options', 'twilio_options_section', array('twilio_api_key'));
    add_settings_field('twilio_api_secret',
        'Twilio API secret',
        'twilio_string_input', 'twilio_options', 'twilio_options_section', array('twilio_api_secret'));
    add_settings_field('twilio_config_sid',
        'Twilio config SID',
        'twilio_string_input', 'twilio_options', 'twilio_options_section', array('twilio_config_sid'));
}

function twilio_string_input($args)
{
    $input_name = $args[0];
    $opt = get_option($input_name);
    echo "<input id='${input_name}_input' name='${input_name}' value='{$opt}' />";
}

function twilio_settings_description()
{
    echo "The following keys are available in you Twilio profile, just copy over the keys caring about the whitespaces.";
}
