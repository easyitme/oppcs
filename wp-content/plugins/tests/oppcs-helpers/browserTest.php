<?php

use PHPUnit\Framework\TestCase;

function add_shortcode()
{
}
require_once(dirname(__FILE__) . "/../../oppcs-helpers/shortcode_my_video_feed.php");

class BrowserTest extends TestCase
{
	/**
	 * @dataProvider provideUserAgentStringsWithVersions
	 */
	public function testSimple($user_agent, $browser_name, $version)
	{
		$_SERVER['HTTP_USER_AGENT'] = $user_agent;
		$detected_browser = detect_browser(true);
		$this->assertSame($detected_browser['browser'], $browser_name);
		$this->assertSame(intval($detected_browser['version']), $version);
	}

	public static function provideUserAgentStringsWithVersions()
	{
		return [
			[
				'Mozilla/5.0 (X11; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0',
				'Firefox',
				49
			],
			// EDGE is not Chrome
			[
				'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246',
				null,
				0
			],
			[
				'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
				'Chrome',
				41
			],
			[
				'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
				'Chrome',
				41
			]
		];
	}
}
